/*
 *  This sketch demonstrates how to set up a simple HTTP-like server.
 *  The server will set a GPIO pin depending on the request
 *    http://server_ip/gpio/0 will set the GPIO2 low,
 *    http://server_ip/gpio/1 will set the GPIO2 high
 *  server_ip is the IP address of the ESP8266 module, will be 
 *  printed to Serial when the module is connected.
 */

#include <ESP8266WiFi.h>
#include <Wire.h>
#include <SFE_MicroOLED.h>  // Include the SFE_MicroOLED library
#include <WEMOS_DHT12.h>

#define PIN_RESET 255  //
#define DC_JUMPER 0  // I2C Addres: 0 - 0x3C, 1 - 0x3D
MicroOLED oled(PIN_RESET, DC_JUMPER);  // I2C Example

DHT12 dht12;

const int goaltemp= 22;
const int histeresis=1;

const char* ssid = "Edge";
const char* password = "stem2017";

// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(D3, OUTPUT);
  digitalWrite(D3, LOW);
  delay(2000);
  oled.begin();     // Initialize the OLED
  oled.clear(PAGE); // Clear the display's internal memory
  oled.clear(ALL);  // Clear the library's display buffer
  oled.display(); // display the memory buffer drawn
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  oled.setCursor(3, 12);
    oled.print("Conecting Edge");
    oled.display();
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    
  }
  Serial.println("");
  Serial.println("WiFi connected");
  
  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {

  int state =  digitalRead(D3);
  
  if(dht12.get()==0){
    oled.clear(PAGE);
    oled.setFontType(0); // set font type 0, please see declaration in SFE_MicroOLED.cpp
    
    oled.setCursor(3, 12);
    oled.print("H: ");
    oled.print(dht12.humidity);

    oled.setCursor(3, 0); 
    oled.print("T: ");
    oled.print(dht12.cTemp);
    if (dht12.cTemp>goaltemp+histeresis) digitalWrite(D3, HIGH); 
    else if (dht12.cTemp<=goaltemp) digitalWrite(D3, LOW);

    oled.setCursor(3, 24); 
    oled.print("I/O:");
    oled.print(goaltemp+histeresis);
    oled.print("|");
    oled.print(goaltemp);

    
    oled.setCursor(3, 36); 
    oled.print("Relay: ");
    oled.print((state) ? "ON":"OFF");
  }
  else
  {
    oled.print("error! ");
  }
  oled.display();
  //delay(1000);
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  
  // Wait until the client sends some data
  Serial.println("new client");
  while(!client.available()){
    delay(1);
  }
  
  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();
  
  // Match the request 
  /*
  int val;
  if (req.indexOf("/gpio/0") != -1)
    val = 0;
  else if (req.indexOf("/gpio/1") != -1)
    val = 1;
  else {
    Serial.println("invalid request");
    client.stop();
    return;
  }*/

  // Set GPIO2 according to the request
  //digitalWrite(2, val);
  
  client.flush();

  // Prepare the response+
  String myString1 = String(dht12.cTemp);
  String myString2 = String(dht12.humidity);
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML><meta http-equiv=refresh content=1 >\r\n<html>\r\nRelay is now ";
  s += (state)?"ON":"OFF";
  s += "<br>Temp:"+myString1;
  s += "<br>Hum:"+myString2;
  s += " </html>\n";

  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println("Client disonnected");

  // The client will actually be disconnected 
  // when the function returns and 'client' object is detroyed
}

