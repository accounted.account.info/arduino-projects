#include <Controllino.h>

const byte digital_output = CONTROLLINO_D0;
const byte interruptPin = CONTROLLINO_IN0;
volatile byte state = LOW;
unsigned long last_interrupt_time = 0;
unsigned long interrupt_time = millis();

void setup() {
 pinMode(CONTROLLINO_R9, OUTPUT); 
}

void loop() {
 
   digitalWrite(CONTROLLINO_R9,HIGH); 
   delay(3000);
   digitalWrite(CONTROLLINO_R9,LOW); 
   delay(3000);
}
