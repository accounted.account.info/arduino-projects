/*

  Firmware: Modbus SE PM130EH
  Author:: Felipe Bravo

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

*/

#include <ModbusMaster.h>



/*===========================================================
/////////////////////// + ADDRESS   /////////////////////////
===========================================================*/

/*/-- address--/*/
int address= 2 ; //direccion de este equipo
const int master_ad= 0xFF;
int zone=0;

/*/-- System --/*/
int doCommand=0;
int bufferToWrite=0;
int app_id=0x10;


/*/-- packet data --/*/
int command=0;
int code=0;
int r_zone;
int r_master_ad=0;
int r_ad=0;
int r_app_id=0;
int r_chck=0;
byte  data[200];

/*/-- packet bytes --/*/
  int o_address = 0x00;
  int packet[255];
  int packet_o[255];
  int packet_counter=0;
  int packet_ini=0;
  int packet_end=0;
  int packet_size=0;
  byte dataBuffer[255];
  int sys_dataBuffer[255];
  int dataBufferLength=0;
  int setpointActivated=0;

/*/-- system time handle --/*/
  unsigned long start_t=0 ;  //SERIAL
  unsigned long work_last=0;
  unsigned long buzzer_start=0 ;  //SERIAL
  unsigned long buzzer_current=0;  
  unsigned long work_current=0;
  const unsigned long work_frame=600;
  const unsigned long answer_time=600;
  const unsigned long line_time=5000;
  unsigned long line_time_start=0;
  unsigned long line_time_current=0;

/*!
  We're using a MAX485-compatible RS485 Transceiver.
  Rx/Tx is hooked up to the hardware serial port at 'Serial'.
  The Data Enable and Receiver Enable pins are hooked up as follows:
*/

#define MAX485_DE      3
#define MAX485_RE_NEG  2

// instantiate ModbusMaster object
ModbusMaster node;

void setup()
{
  pinMode(MAX485_RE_NEG, OUTPUT);
  pinMode(MAX485_DE, OUTPUT);
  // Init in receive mode
  digitalWrite(MAX485_RE_NEG, 0);
  digitalWrite(MAX485_DE, 0);

  // Modbus communication runs at 115200 baud
  Serial.begin(2400);  //DEBUG
  Serial2.begin(9600); //RS485
  Serial1.begin(2400); //MODEM
  // Modbus slave ID 1
  node.begin(1, Serial2);
  // Callbacks allow us to configure the RS485 transceiver correctly
  node.preTransmission(preTransmission);
  node.postTransmission(postTransmission);
  startDelta();
}


void loop()
{
  packet_check(); //
  if(packet_end==1 && r_master_ad==address){
    getData(packet_size);
    COM2323TO485();
    //RS485COM_READ(11616,2);
    clear_serial();
    startDelta();
  } else if (packet_end==1){
        Serial.print("\naddr:");
        Serial.print(r_master_ad);
        Serial.print("-\n");
        clear_serial();
    }
  
  /*
  long time_delta=getDelta();
  if(time_delta>line_time && setpointActivated==1){
      readSetpointRegisters();
      startDelta();
    }
    
  
  uint16_t result;
  uint16_t data[6];
  
  // Toggle the coil at address 0x0002 (Manual Load Control)
  //result = node.writeSingleCoil(0x0002, state);
  //state = !state;

  // Read 16 registers starting at 0x3100)
  result = node.readInputRegisters(11616, 1);
  if (result == node.ku8MBSuccess)
  {
    Serial.print("Setpoints: ");
    Serial.println(node.getResponseBuffer(0x00),BIN);
  }

  delay(1000);
  //RS485COM_READ(11616,2);
  //delay(1000);
  */
}



/*===========================================================
///////////////////// RS485 FUNCTIONS   /////////////////////
===========================================================*/

void preTransmission()
{
  digitalWrite(MAX485_RE_NEG, 1);
  digitalWrite(MAX485_DE, 1);
}

void postTransmission()
{
  digitalWrite(MAX485_RE_NEG, 0);
  digitalWrite(MAX485_DE, 0);
}

int startDelta(){
    line_time_current=millis();
    line_time_start=millis();
  }

  long getDelta(){
    line_time_current=millis();
    return line_time_current-line_time_start;
  }
void RS485COM_READ(int mbaddress, int quantity){
      uint16_t result;
      uint16_t data_[6];
      int dat_len = 0;
      
      // Toggle the coil at address 0x0002 (Manual Load Control)
      //result = node.writeSingleCoil(0x0002, state);
      //state = !state;
    
      result = node.readInputRegisters(mbaddress, quantity);
      if (result == node.ku8MBSuccess)
      {
        Serial.println("bytes: ");
        for (int i=3; i<quantity; i++){
            uint16_t response =  node.getResponseBuffer(i);
            data_[2*i]=(byte)(response>>8);
            data_[(2*i)+1]=(byte)response;
            Serial.println(data_[(2*i)],BIN);
            Serial.println(data_[(2*i)+1],BIN);
            dat_len = (2*i)+1;
        }
      }
          
      //delay(500);
  }

/*===========================================================
///////////////////// SYSTEM FUNCTIONS   ////////////////////
===========================================================*/


int clearDataBuffer(){

  for(int j=0;j<255;j++){
      dataBuffer[j]= 0x20;    
  }
  
}

int COM2323TO485(){
  
    /*!
    * This Structure (Databuffer of sigmation's protocol package structure)
    * Follows the same structure as Modbus: (here is how it unpacks)
    * 
    * data[0] = Slave Address       (1 byte)
    * data[1] = Function            (1 byte)
    * data[2] = Starting Address Hi (1 byte)
    * data[3] = Starting Address Lo (1 byte)
    * data[4] = No. of Registers Hi (1 byte)
    * data[5] = No. of Registers Lo (1 byte)
    * data[6] = Error Check LRC     (2 bytes)  //this won't be needed but it is still included in the packet
    *
    * Due to the high noise signals on the Leaky Feeder, it is not recomended to use the
    * RTU Modbus protocol as it is due to silence requeriments to start/end packets
    * Neither ASCII due to larger data packets
    * Modbus was designed for low-noise cable enviroments like the RS485, not RF, keep this in mind.
    * 
    * -Felipe Bravo
    * 
    */

    uint8_t  slave_address= dataBuffer[0];                                       // modbus address
    uint8_t  function = dataBuffer[1];                                           // function
    uint16_t registry_address= ((unsigned int)dataBuffer[2]<<8) + dataBuffer[3]; // Starting Address
    uint16_t registry_quantity= ((unsigned int)dataBuffer[4]<<8) +dataBuffer[5]; // No. of Registers

    /*save variables for answer packet*/
    data[0] = dataBuffer[0];   
    data[1] = dataBuffer[1];   
    data[2] = dataBuffer[2];   
    data[3] = dataBuffer[3];   
    data[4] = dataBuffer[4];
    data[5] = dataBuffer[5];

   /*for certain functions, store data in modbus buffer*///8d00-0000-0000-0000-val1h-val1l-val2h-val2l
   /*For extended ModbusLF functions, it will not store data*/
    if( function >= 15  && registry_quantity<60 && function<0xA0) {
      Serial.println("writing registers in buffer ");
      for (int i =0; i < registry_quantity ; i++){
          Serial.print("*");
          uint16_t toint16= ((unsigned int)dataBuffer[6+(2*i)]<<8) + dataBuffer[7+(2*i)];
          node.setTransmitBuffer(i,toint16);
        }
      Serial.print("  done \n");
    }

    
    

   /*  Now the function is assigned to the corresponding library call 
    *  
    *  For the PM130EH
    *  
    *  03 Read holding registers Read multiple registers
    *  04 Read input registers Read multiple registers
    *  06 Preset single register Write single register
    *  16 Preset multiple registers Write multiple registers
    *   
    *   For the whole Modbus protocol
    *   
    *   01 Read Coil Status Y Y Y Y Y Y
    *   02 Read Input Status Y Y Y Y Y Y
    *   03 Read Holding Registers Y Y Y Y Y Y
    *   04 Read Input Registers Y Y Y Y Y Y
    *   05 Force Single Coil Y Y Y Y Y Y
    *   06 Preset Single Register Y Y Y Y Y Y
    *   07 Read Exception Status Y Y Y Y Y Y
    *   08 Diagnostics (see Chapter 3)
    *   09 Program 484 N Y N N N N
    *   10 Poll 484 N Y N N N N
    *   11 Fetch Comm. Event Ctr. Y N Y N N Y
    *   12 Fetch Comm. Event Log Y N Y N N Y
    *   13 Program Controller Y N Y N N Y
    *   14 Poll Controller Y N Y N N Y
    *   15 Force Multiple Coils Y Y Y Y Y Y
    *   16 Preset Multiple Registers Y Y Y Y Y Y
    *   17 Report Slave ID Y Y Y Y Y Y
    *   18 Program 884/M84 N N N Y Y N
    *   19 Reset Comm. Link N N N Y Y N
    *   20 Read General Reference N N Y N N Y
    *   21 Write General Reference N N Y N N Y
    *   
    *   Supported by the library
    *   
    *       static const uint8_t ku8MBReadCoils                  = 0x01; ///< Modbus function 0x01 Read Coils
    *       static const uint8_t ku8MBReadDiscreteInputs         = 0x02; ///< Modbus function 0x02 Read Discrete Inputs
    *       static const uint8_t ku8MBWriteSingleCoil            = 0x05; ///< Modbus function 0x05 Write Single Coil
    *       static const uint8_t ku8MBWriteMultipleCoils         = 0x0F; ///< Modbus function 0x0F Write Multiple Coils
    *   
    *       // Modbus function codes for 16 bit access
    *       static const uint8_t ku8MBReadHoldingRegisters       = 0x03; ///< Modbus function 0x03 Read Holding Registers
    *       static const uint8_t ku8MBReadInputRegisters         = 0x04; ///< Modbus function 0x04 Read Input Registers
    *       static const uint8_t ku8MBWriteSingleRegister        = 0x06; ///< Modbus function 0x06 Write Single Register
    *       static const uint8_t ku8MBWriteMultipleRegisters     = 0x10; ///< Modbus function 0x10 Write Multiple Registers
    *       static const uint8_t ku8MBMaskWriteRegister          = 0x16; ///< Modbus function 0x16 Mask Write Register
    *       static const uint8_t ku8MBReadWriteMultipleRegisters = 0x17; ///< Modbus function 0x17 Read Write Multiple Registers
    * 
    * 
    *   Asociated functions from the library
    *  
    *     uint8_t  readCoils(uint16_t, uint16_t);
    *     uint8_t  readDiscreteInputs(uint16_t, uint16_t);
    *     uint8_t  readHoldingRegisters(uint16_t, uint16_t);
    *     uint8_t  readInputRegisters(uint16_t, uint8_t);
    *     uint8_t  writeSingleCoil(uint16_t, uint8_t);
    *     uint8_t  writeSingleRegister(uint16_t, uint16_t);
    *     uint8_t  writeMultipleCoils(uint16_t, uint16_t);
    *     uint8_t  writeMultipleCoils();
    *     uint8_t  writeMultipleRegisters(uint16_t, uint16_t);
    *     uint8_t  writeMultipleRegisters();
    *     uint8_t  maskWriteRegister(uint16_t, uint16_t, uint16_t);
    *     uint8_t  readWriteMultipleRegisters(uint16_t, uint16_t, uint16_t, uint16_t);
    *     uint8_t  readWriteMultipleRegisters(uint16_t, uint16_t);
    *     
    *  Acording to this info, functions are switched accordingly for the PM130EH
    */
      uint16_t result;
      Serial.println("."); Serial.print(function,HEX); Serial.print(" - "); Serial.print(slave_address,HEX); Serial.print(" - "); Serial.print(registry_address,HEX); Serial.print(" - "); Serial.print(registry_quantity,HEX);
      switch(function) {
        
          case 0x03   :   result = node.readHoldingRegisters(registry_address,registry_quantity);
                          debugdata(result, registry_quantity);
                          break;
        
          case 0x04   :   result = node.readInputRegisters(registry_address,registry_quantity);
                          debugdata(result, registry_quantity);
                          break;       

          case 0x05   :   result = node.writeSingleCoil(registry_address,registry_quantity);
                          debugdata(result, 8);
                          break;

          case 0x06   :   result = node.writeSingleRegister(registry_address,registry_quantity);
                          debugdata(result, 8);
                          break;

          case 0x0F   :   result = node.writeMultipleCoils(registry_address,registry_quantity);
                          debugdata(result, 8);
                          break;   

          case 0x10   :   result = node.writeMultipleRegisters(registry_address,registry_quantity);
                          debugdata(result, 8);
                          break;
          case 0xA1   :   readDifferentRegisters();  
                          break;                                                             

          default     :   Serial.println(" unsupported PM130EH function received");
                          break;     
        }

        //Serial.println(".");
  
  }

  int readDifferentRegisters(){
        uint16_t result;
        int framsize = 4; //4 bytes: address high/low, quantity high/low
        int frames = dataBuffer[2];
        int bytecounter=3;
        int nocom=0;
        data[0]=dataBuffer[0];
        data[1]=dataBuffer[1];
        data[2]=dataBuffer[2];
        Serial.print("\ngetting different variables: ");
        Serial.print(data[0],HEX);
        Serial.print(" -  ");
        Serial.print(data[1],HEX);
        Serial.print(" -  ");
        Serial.println(data[2],HEX);
        for(int i=0;i<frames && nocom==0 ;i++){
          uint16_t registry_address= ((unsigned int)dataBuffer[3+(4*i)]<<8) + dataBuffer[4+(4*i)]; // Starting Address
          uint16_t registry_quantity= ((unsigned int)dataBuffer[5+(4*i)]<<8) +dataBuffer[6+(4*i)]; // No. of Registers 
          if(registry_address==11616) setpointActivated=1;
          Serial.print("\nadd/qty: ");
          Serial.print(registry_address);
          Serial.print(" - ");
          Serial.println(registry_quantity);
          result = node.readHoldingRegisters(registry_address,registry_quantity); 

          data[bytecounter]=dataBuffer[3+(4*i)];
          data[bytecounter+1]=dataBuffer[4+(4*i)];
          data[bytecounter+2]=dataBuffer[5+(4*i)];
          data[bytecounter+3]=dataBuffer[6+(4*i)];
          data[bytecounter+4]=0xFF;
          data[bytecounter+5]=0xFF;
          int r_count=1;
          if(registry_quantity!=1){
            r_count=2;
            data[bytecounter+6]=0xFF;
            data[bytecounter+7]=0xFF;
           }
           if (result == node.ku8MBSuccess)
            {
              for (int i=0; i<r_count; i++){
                  uint16_t response =  node.getResponseBuffer(i);
                  data[bytecounter+4+(i*2)]=(byte)(response>>8);
                  data[bytecounter+5+(i*2)]=(byte)response;
              }
            } else {
                nocom=1;  
                Serial.println("<NO COM>");
            }
            if(registry_quantity==1) bytecounter=bytecounter+6;
            else bytecounter=bytecounter+8;
        }
        if(nocom==0) send_packet(0xFF, bytecounter);
        else {
            data[1]=0xB1;
            data[2]=0x01;
            send_packet(0xFF, 2);
          }
    }

int readSetpointRegisters(){
        uint16_t result;
        int transmit =0;
        int framsize = 4; //4 bytes: address high/low, quantity high/low
        int frames = 1;
        int bytecounter=3;
        data[0]=1;
        data[1]=0xA1;
        data[2]=frames;
        Serial.print("\ngetting setpoints: ");
        Serial.print(data[0],HEX);
        Serial.print(" -  ");
        Serial.print(data[1],HEX);
        Serial.print(" -  ");
        Serial.println(data[2],HEX);
        for(int i=0;i<frames;i++){
          uint16_t registry_address= 11616; // Starting Address
          uint16_t registry_quantity= 1; // No. of Registers 
          Serial.print("\nadd/qty: ");
          Serial.print(registry_address);
          Serial.print(" - ");
          Serial.println(registry_quantity);
          result = node.readHoldingRegisters(registry_address,registry_quantity); 

          data[bytecounter]=dataBuffer[3+(4*i)];
          data[bytecounter+1]=dataBuffer[4+(4*i)];
          data[bytecounter+2]=dataBuffer[5+(4*i)];
          data[bytecounter+3]=dataBuffer[6+(4*i)];
          data[bytecounter+4]=0xFF;
          data[bytecounter+5]=0xFF;
          int r_count=1;

           if (result == node.ku8MBSuccess)
            {
              for (int i=0; i<r_count; i++){
                  uint16_t response =  node.getResponseBuffer(i);
                  if(response>0) transmit=1;
                  data[bytecounter+4+(i*2)]=(byte)(response>>8);
                  data[bytecounter+5+(i*2)]=(byte)response;
              }
            }
            bytecounter=bytecounter+6;
        }
        if(transmit==1) send_packet(0xFF, bytecounter);
    }
    
  int debugdata(uint16_t result, int quantity){
    int dat_len=0;
    if (result == node.ku8MBSuccess)
      {
        //Serial.println(" - bytes: ");
        for (int i=0; i<quantity; i++){
            uint16_t response =  node.getResponseBuffer(i);
            data[(2*i)+6]=(byte)(response>>8);
            data[(2*i)+7]=(byte)response;
            //Serial.println(data[(2*i)+6],BIN);
            //Serial.println(data[(2*i)+7],BIN);
            dat_len = (2*i)+8;
        }
         send_packet(0xFF, dat_len);
      }else {
          data[0]=0xAA;
          //send_packet(0xFF, 1);
      }
    }

/*===========================================================
///////////////////// SERIAL FUNCTIONS   ////////////////////
===========================================================*/

int getData(int ldata){   
    dataBufferLength=ldata;  
    for (int i=0; i<ldata; i=i+1){
               dataBuffer[i]=packet[6+i];
     } 
}

int clear_serial(){
  
  for(int i=0; i<10; i=i+1){
      packet[i]=0;
    }
    packet_end=0;
    packet_ini=0; 
    packet_counter=0;
    packet_size=0;
}  
  


int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera      :   0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de unidad maestra
    3 address       :   direccion de este dispositivo
    4 zone          :   zona de ubicacion de este dispositivo
    5 largodatos    :   largo de datos ( 1 comando + 1 codigo + largo)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
_____________________________________________________________________________________________________________________________

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================*/
  
    if (Serial1.available()) {
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      packet[packet_counter] = Serial1.read();
      //Serial.write(packet[packet_counter]);
      if(packet_counter==6 && packet_ini>0 && packet_size>0 )  {command=packet[packet_counter];}//systemMessage("    x");}
      else if(packet_counter==7 && packet_ini>0 && packet_size>1)  {code=packet[packet_counter];}//systemMessage("    x");}
      
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {r_app_id=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {r_master_ad=packet[packet_counter]; packet_counter= packet_counter+1;
      } //if (r_master_ad!=address && r_master_ad!=0) {Serial.write(0xBB);Serial.write(r_ad);packet_counter=0; packet_size=0; packet_ini=0;}}//systemMessage(" x");}
      else if(packet_counter==3 && packet_ini>0)  {r_ad=packet[packet_counter]; packet_counter= packet_counter+1;  }//systemMessage(" x");}
      else if(packet_counter==4 && packet_ini>0)  {r_zone=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==5 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}     
      else if(packet_counter<6+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==6+packet_size && packet_ini>0){r_chck=packet[packet_counter];packet_counter= packet_counter+1;}//systemMessage("       x!");}
      else if(packet_counter==7+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;}//systemMessage("       x!");}
    }
    
  }  
  

  int checksum(){
       int checksm = packet[1];
       for (int i=2; i<packet_size+8; i=i+1){
           checksm = packet[i] ^ checksm;
        }  
       if(checksm==packet[packet_size+8]) return 0;
       else {               
             return 1;
       }
    }




  int send_packet(int s_address, int ldata){
    
    
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de este dispositivo
    3 address       :   direccion de unidad maestra
    4 zone          :   zona de ubicacion de este dispositivo
    5 largo         :   largo de data ( 1 comando + 1 codigo + datos)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
=============================================================================================================================*/

         int chcksm=0;
         packet_o[0] = 0xFE; 
         packet_o[1] = app_id;
         chcksm = chcksm ^ packet_o[1];
         packet_o[2] = master_ad;
         chcksm = chcksm ^ packet_o[2];
         packet_o[3] = address;
         chcksm = chcksm ^ packet_o[3];
         packet_o[4] = zone;
         chcksm = chcksm ^ packet_o[4];
         packet_o[5] = ldata;
         chcksm = chcksm ^ packet_o[5];
         for (int i=0; i<ldata; i=i+1){
               packet_o[6+i] = data[i];
               chcksm = chcksm ^ packet_o[6+i];
        } 
        packet_o[6+ldata] =  chcksm; 
        packet_o[7+ldata] =  0xFD;
        for (int i=0; i<4;i++){
          Serial1.write(0);
        }
        for (int i=0; i<10+ldata;i=i+1){
          Serial1.write(packet_o[i]);
       }
    }



