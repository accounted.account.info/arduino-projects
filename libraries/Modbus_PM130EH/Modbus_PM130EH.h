/*--------------------------------------------------------------------------------------
DMD2.h   - Arduino library for PM130EH.

By Felipe Bravo

---

This program is free software: you can redistribute it and/or modify it under the terms
of the version 3 GNU General Public License as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.


*/
#define BASIC_REGISTER 256


/*------------- OFFSETS --------------*/
#define V1V12_VOLTAGE 0
#define V2V23_VOLTAGE 1
#define V3V31_VOLTAGE 2
#define I1_CURRENT    3
#define I2_CURRENT    4
#define I3_CURRENT    5
#define KW_L1         6
#define KW_L2         7
#define KW_L3         8
#define KVAR_L1       9
#define KVAR_L2       10
#define KVAR_L3       11
#define KVA_L1        12
#define KVA_L2        13
#define KVA_L3        14
#define PF_L1         15
#define PF_L2         16
#define PF_L3         17

#define TOTAL_PF      18
#define TOTAL_KW      19
#define TOTAL_KVAR    20
#define TOTAL_KVA     21

#define IN_NEUTRAL_C  22
#define FREQUENCY     23

#define MAX_KW_ISWD   24 #MAXIMUM KW IMPORT SLIDING WINDOW DEMAND

#define KW_IAD        25 #KW IMPORT ACUMULATED DEMAND
#define MAX_KVA_SWD   26 #MAXIMUM KVA SLIDING WINDOW DEMAND
#define KVA_ACUM_DEM  27 #KVA ACUMULATED DEMAND

#define I1_MAX_AMP_D  28 #I1 MAXIMUM AMPER DEMAND
#define I2_MAX_AMP_D  29 #I2 MAXIMUM AMPER DEMAND
#define I3_MAX_AMP_D  30 #I3 MAXIMUM AMPER DEMAND

#define KWH_IMPORT_L  31
#define KWH_IMPORT_H  32
#define KWH_EXPORT_L  33
#define KWH_EXPORT_H  34
#define MKVAR_NET_L   35
#define MKVAR_NET_H   36
#define PKVAR_NET_L   37
#define PKVAR_NET_H   38
#define V1V12_V_THD   39
#define V2V23_V_THD   40
#define V3V31_V_THD   41
#define I1_C_THD      42
#define I2_C_THD      43
#define I3_C_THD      44
#define KVAH_L        45
#define KVAH_H        46
#define P_KW_I_SWD    47 #PRESENT KW IMPORT SLIDING WINDOW DEMAND
#define P_KVA_SWD     48 #PRESENT KVA SLIDING WINDOW DEMAND
#define PF_IM_KVA_SWD 49 #PF IMPORT AT MAX KVA SLIDING WINDOW DEMAND
#define I1_C_TDD      50
#define I2_C_TDD      51
#define I3_C_TDD      52

/*------------- POINT ID --------------*/
#define V1V12_VOLTAGE_IDB 0x1100
#define V2V23_VOLTAGE_IDB 0x1101
#define V3V31_VOLTAGE_IDB 0x1102
#define I1_CURRENT_IDB    0x1103
#define I2_CURRENT_IDB    0x1104
#define I3_CURRENT_IDB    0x1105
#define KW_L1_IDB         0x1106
#define KW_L2_IDB         0x1107
#define KW_L3_IDB         0x1108
#define KVAR_L1_IDB       0x1109
#define KVAR_L2_IDB       0x110A
#define KVAR_L3_IDB       0x110B
#define KVA_L1_IDB        0x110C
#define KVA_L2_IDB        0x110D
#define KVA_L3_IDB        0x110E
#define PF_L1_IDB         0x110F
#define PF_L2_IDB         0x1110
#define PF_L3_IDB         0x1111

#define TOTAL_PF_IDB      0x1403
#define TOTAL_KW_IDB      0x1400
#define TOTAL_KVAR_IDB    0x1401

#define IN_NEUTRAL_C_IDB  0x1501
#define FREQUENCY_IDB     0x1502

#define MAX_KW_ISWD_IDB   0x3709 #MAXIMUM KW IMPORT SLIDING WINDOW DEMAND

#define KW_IAD_IDB        0x160F #KW IMPORT ACUMULATED DEMAND
#define MAX_KVA_SWD_IDB   0x370B #MAXIMUM KVA SLIDING WINDOW DEMAND
#define KVA_ACUM_DEM_IDB  0x1611 #KVA ACUMULATED DEMAND

#define I1_MAX_AMP_D_IDB  0x3703 #I1 MAXIMUM AMPER DEMAND
#define I2_MAX_AMP_D_IDB  0x3704 #I2 MAXIMUM AMPER DEMAND
#define I3_MAX_AMP_D_IDB  0x3705 #I3 MAXIMUM AMPER DEMAND
