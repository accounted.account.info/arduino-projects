/*=================================================================================
////////////////////////  SOFTWARE UGMSG SIGMA TELECOM  ///////////////////////////
===================================================================================

INGENIERO: FELIPE EDUARDO BRAVO SILVA
NOMBRE P.: SISTEMA DE MENSAJES EN PANELES LED UGMSG
ESTADO   : EN DESARROLLO
FECHA ACT: 26-09-16
FECHA COM: 1-09-16
PROTOCOLO: CUSTOM FBS PROTOCOL (PROPIETARIO DE FELIPE BRAVO BAJO LICENCIA COPYLEFT)


=================================================================================*/



#include <SPI.h>
#include <DMD2.h>
#include <fonts/Sigma9_2.h>


#define DISPLAYS_WIDE 4
#define DISPLAYS_HIGH 1



/*===========================================================
///////////////////////  SYS VARS   /////////////////////////
===========================================================*/

SoftDMD dmd(DISPLAYS_WIDE,DISPLAYS_HIGH);
DMD_TextBox box(dmd, 0, 0, 32, 16);

/*/-- System --/*/
int doCommand=0;
int bufferToWrite=0;
int app_id=0x02;
int zone=0;
int currentBuffer=4;
int currentLine=0;
const int max_per_line=15;

/*/-- address--/*/
int address= 0x0B; //direccion de este equipo
int master_ad= 0xFF;

/*/-- packet data --/*/
int command=0, data[200];
int code=0;
int r_zone;
int r_master_ad=0;
int r_ad=0;
int r_app_id=0;
int r_chck=0;

/*/-- packet bytes --/*/
  int o_address = 0x00;
  int packet[255];
  int packet_o[255];
  int packet_counter=0;
  int packet_ini=0;
  int packet_end=0;
  int packet_size=0;
  int dataBuffer[255];
  int dataBufferLength=0;
  
/*/-- system time handle --/*/
  unsigned long start_t=0 ;  //SERIAL
  unsigned long work_last=0;
  unsigned long buzzer_start=0 ;  //SERIAL
  unsigned long buzzer_current=0;  
  unsigned long work_current=0;
  const unsigned long work_frame=600;
  const unsigned long answer_time=600;
  const unsigned long line_time=2000;
  unsigned long line_time_start=0;
  unsigned long line_time_current=0;
/*/-- LED system buffers and flags --/*/
  char ledbuffer[5][50];  // 4 buffers de 50 caracteresz
  char ledSubBuffer[5][4][20];  // 4 buffers con 4 subbuffers de 14 caracteresz
  int ledflags[5][4];     // 4 flags: 0-set 1-largo 2-lineas calculadas 3-not used 


/*/-- system PINOUT --/*/

int button_pin = 22;
int alarm_pin = 26;
 
/*/-- System flags --/*/ 

int button_pressed=0;
int alarm_flag=0;

/*========================================================================
    
    flags:
        0:set => 0 sin mensajes, 1 con mensaje, 2+ con mensaje emergencia 
               si setea mensaje emergencia, toma prioridad sobre otros 
               mensajes de emergencia(set+1 del mensaje de emergencia 
               con mayor prioridad), si existe almenos 1 mensaje de 
               emergencia, los mensajes normales son ignorados.
               Si solo existe un mensaje de emergencia, tomara el valor 2.
        1:largo => indica el largo del mensaje
        
========================================================================*/

/*========================================================================

chars especiales:
$ : escape
& : vehiculo
@ : exclamacion en triangulo
^ : peligro electrico
{ : fuego
} : derrumbe
~ : eñe
0x7F : FULL

========================================================================*/

/*===========================================================
/////////////////////////  SETUPS   /////////////////////////
===========================================================*/


void setup() {
  dmd.setBrightness(255);
  dmd.selectFont(Sigma9);
  dmd.begin();
  Serial1.begin(9600);
  pinMode(button_pin,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(button_pin), irq_button, RISING);
  sampleText();
  clearDataBuffer();
  startDelta();
}


/*===========================================================
/////////////////////////  LOOP    //////////////////////////
===========================================================*/

void loop() {
  packet_check();
  if(checkPacket()==1){
    doFunction(doCommand);
    doCommand=0;
  }
  writeMessages();
  check_state();
  delay(1);
}

/*===========================================================
/////////////////////////  FUNCIONES  ///////////////////////
===========================================================*/
/////////////=============  DEMO  ===============////////////

int sampleText(){
    char sample[]="hola";
    for(int i=0; i<5; i++)
    {
      ledbuffer[4][i] =sample[i];
      ledSubBuffer[4][0][i]=sample[i];
    } 
    ledflags[4][0]=1;
    ledflags[4][1]=4;
    ledflags[4][2]=1; 
}

/////////////=============   IRQ  ===============////////////

void irq_button(){
    button_pressed=1;
  }

/////////////============= SYSTEM ===============////////////
int doFunction(int doCommand_){
  
  switch(doCommand_){
  
    case  1  :    savebuffer(dataBufferLength-2,bufferToWrite);
                  break;
    default  :    error(0x03);
                  break;
  }

}

int check_state(){
      if(button_pressed==1 && alarm_flag==0) {
                 alarm_flag=1;
                 button_pressed=0;
                 digitalWrite(alarm_pin, HIGH); 
        }
      else if (button_pressed==1 && alarm_flag==1) {
                 alarm_flag=0;
                 button_pressed=0;
                 digitalWrite(alarm_pin, LOW); 
        }
  }


int savebuffer(int bufferlen,int bufferToWrite_){
  
  if(bufferlen<50){
      for(int i=0;i<50 ;i++){
        ledbuffer[bufferToWrite_][i]= dataBuffer[2+i];       
      }
      ledflags[bufferToWrite_][0]=dataBuffer[0];
      if(bufferToWrite_==4)  ledflags[bufferToWrite_][0]=1;
      ledflags[bufferToWrite_][1]=bufferlen;
      ledflags[bufferToWrite_][2]=calcLines(bufferlen);
      for(int i=0;i<ledflags[bufferToWrite_][2] ;i++){
        for(int j=0;j<20 ;j++){
          if(j<max_per_line)ledSubBuffer[bufferToWrite_][i][j]= ledbuffer[bufferToWrite_][(i*max_per_line)+j]; 
          else  ledSubBuffer[bufferToWrite_][i][j]= 0x20;      
        }      
      }
  }
  Serial1.write(0xFA);
  Serial1.write(bufferToWrite_);
  Serial1.write(ledflags[bufferToWrite_][0]);
  Serial1.write(0xFB);
  clearDataBuffer();
}

int deleteBuffer(int buffernum){
  for(int i=0;i<50;i++){
    ledbuffer[buffernum][i]= 0;
  }
      ledflags[buffernum][0]=0;
      ledflags[buffernum][1]=0;
      ledflags[buffernum][2]=0;
}

int deleteAllBuffers(){

  for(int j=0;j<5;j++){
     for(int i=0;i<50;i++){
      ledbuffer[j][i]= 0;
    }
        ledflags[j][0]=0;
        ledflags[j][1]=0;
        ledflags[j][2]=0;
  }
  
}

int clearDataBuffer(){

  for(int j=0;j<255;j++){
      dataBuffer[j]= 0x20;    
  }
  
}

int error(int code){
  /*
    Error codes:
    0x00: buffer incorrecto
    0x01: largo incorrecto de mensaje
    0x02: codigos de error 0x00 y 0x01 juntos
    0x03: comando incorrecto/ error de sistema 
  */
  send_packet(0x0E, code, 0xFF, 0x00);
}
/////////////============= PANEL LED ============////////////

int topPriority(){
  int topprior=0;
  for(int i=0;i<5;i++){
      if (topprior<ledflags[i][0]) topprior=ledflags[i][0];
  }
  return topprior;
}

int calcLines(int sizeOfBuffer){
  for(int i=1;i<=4 && sizeOfBuffer>0 ;i++){
    if (sizeOfBuffer<=(max_per_line*i)) return i;  
  }
  return 0;
}

long getDelta(){
    line_time_current=millis();
    return line_time_current-line_time_start;
  }

int startDelta(){
    line_time_current=millis();
    line_time_start=millis();
  }
  
int writeMessages(){
  int topprior = topPriority();
  long time_delta=getDelta();
  if(ledflags[currentBuffer][0]==topprior && time_delta>line_time){
     /* Serial1.write(0xF0);
      Serial1.write(topprior);
      Serial1.write(currentBuffer);
      Serial1.write(currentLine);
      Serial1.write(ledflags[currentBuffer][0]);
      Serial1.write(0xF1);*/

      dmd.drawString(0, 0, ledSubBuffer[currentBuffer][currentLine]);
      startDelta();  
      currentLine++; 
      if (currentLine >= ledflags[currentBuffer][2] | currentLine >3) {
          currentLine=0;
          currentBuffer++;
          if(currentBuffer>4) currentBuffer=0;
        }  
      
   }else if (ledflags[currentBuffer][0]!=topprior) {
    /*
      Serial1.write(0xFA);
      Serial1.write(topprior);
      Serial1.write(currentBuffer); 
      Serial1.write(ledflags[currentBuffer][0]);
      Serial1.write(0xFB);*/
      currentBuffer++;
      currentLine=0;
      if(currentBuffer>4) currentBuffer=0;
   }
}

/////////////============= PANEL LED ============////////////

int systemMessage(char towrite[]){
  //dmd.drawString(0, 0, F(towrite));
  char *next = towrite;
  box.print(*next);
}

/////////////=============  COMM  ==============/////////////

int checkPacket(){
  int result=0;
  if(packet_end==1 && command>0 ){
      doCommand=command;
      getMessage();
      if(code<5 && dataBufferLength<52 ) {
          bufferToWrite= code;             // buffer=code
          send_packet(bufferToWrite, dataBufferLength, 0xFF, 0x00);  // recibido
          result=1;
      }
      else {
          if(code>4 && dataBufferLength>51)  error(0x02); //buffer incorrecto y tamaño muy grande
          else if(code>4) error(0x00); //buffer incorrecto
          else error(0x01); //tamaño incorrecto (mayor a 50 caracteres)
          doCommand=0;  //borrar comando
          result=0;
      }
      clear_serial();
  }
  else if(packet_end==1 && command==0){
      doCommand=command;
      if(code<5) {
          bufferToWrite= code;
          send_packet(0x01, 0x00, 0xFF, 0x00);
          result=1;
                            }
      else {
          error(0x00); //buffer incorrecto
          doCommand=0xFF;  //borrar comando
          result=0;
      }
      clear_serial();
  }
  return result;
}

int wait_answer(){
    unsigned long current_t=0, delta_t=0;
    start_t=millis();

    while(delta_t<answer_time){
      current_t = millis();
      delta_t = current_t-start_t;
      packet_check();
      if(packet_end==1)  {  return 1; }
    }
    return 0;
  }

int getMessage(){
    if(packet_size>=2)getData(packet_size);
}
  
int getData(int ldata){   
    dataBufferLength=ldata;  
    for (int i=0; i<ldata; i=i+1){
               dataBuffer[i]=packet[6+i];
     } 
}

int clear_serial(){
  
  for(int i=0; i<10; i=i+1){
      packet[i]=0;
    }
    packet_end=0;
    packet_ini=0; 
    packet_counter=0;
    packet_size=0;
}  
  
//////////=============  PROTOCOLO  ==============//////////
  

int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera      :   0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de unidad maestra
    3 address       :   direccion de este dispositivo
    4 zone          :   zona de ubicacion de este dispositivo
    5 largodatos    :   largo de datos ( 1 comando + 1 codigo + largo)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
_____________________________________________________________________________________________________________________________

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================*/
  
    if (Serial1.available()) {
      
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      packet[packet_counter] = Serial1.read();

      if(packet_counter==6 && packet_ini>0 && packet_size>0 )  {command=packet[packet_counter];}//systemMessage("    x");}
      else if(packet_counter==7 && packet_ini>0 && packet_size>1)  {code=packet[packet_counter];}//systemMessage("    x");}
      
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {r_app_id=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {r_master_ad=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==3 && packet_ini>0)  {r_ad=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==4 && packet_ini>0)  {r_zone=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==5 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}     
      else if(packet_counter<6+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==6+packet_size && packet_ini>0){r_chck=packet[packet_counter];packet_counter= packet_counter+1;}//systemMessage("       x!");}
      else if(packet_counter==7+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;}//systemMessage("       x!");}
    }
    
  }  
  

  int checksum(){
       int checksm = packet[1];
       for (int i=2; i<packet_size+8; i=i+1){
           checksm = packet[i] ^ checksm;
        }  
       if(checksm==packet[packet_size+8]) return 0;
       else {               
             systemMessage("Error Checksum");
             //Serial.print("::: chk: ");
             //Serial.print(checksm);
             //Serial.print(" pchk: ");
             //Serial.print(packet[packet_size+5]);
             //Serial.print(" ::: ");
             return 1;
       }
    }
       

  int send_packet(int commando, int codigo, int s_address, int ldata){
    
    
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de unidad maestra
    3 address       :   direccion de este dispositivo
    4 zone          :   zona de ubicacion de este dispositivo
    5 largo         :   largo de data ( 1 comando + 1 codigo + datos)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
=============================================================================================================================*/

         int chcksm=0;
         packet_o[0] = 0xFE; 
         packet_o[1] = app_id;
         chcksm = chcksm ^ packet_o[1];
         packet_o[2] = master_ad;
         chcksm = chcksm ^ packet_o[2];
         packet_o[3] = address;
         chcksm = chcksm ^ packet_o[3];
         packet_o[4] = zone;
         chcksm = chcksm ^ packet_o[4];
         packet_o[5] = ldata+2;
         chcksm = chcksm ^ packet_o[5];
         packet_o[6] = commando;
         chcksm = chcksm ^ packet_o[6];
         packet_o[7] = codigo;
         chcksm = chcksm ^ packet_o[7];
         for (int i=0; i<ldata; i=i+1){
               packet_o[8+i] = data[i];
               chcksm = chcksm ^ packet_o[8+i];
        } 
        packet_o[8+ldata] =  chcksm; 
        packet_o[9+ldata] =  0xFD;
        for (int i=0; i<4;i++){
          Serial1.write(0);
        }
        for (int i=0; i<10+ldata;i=i+1){
          Serial1.write(packet_o[i]);
       }
    }



/*===========================================================
/////////////////////////  RECICLAJE  ///////////////////////
===========================================================*/
 /*
 
   int send_packet(int commando, int codigo, int s_address, int ldata){
       int chcksm=0;
       packet_o[0] = 0xFE; 
       packet_o[1] = app_id; //0x02 = app de mensaj
       packet_o[1] = ldata+1; 
       chcksm = chcksm ^ packet_o[1];
       packet_o[2] = address; 
       chcksm = chcksm ^ packet_o[2];
       packet_o[3] = master_ad; 
       chcksm = chcksm ^ packet_o[3];
       packet_o[4] = commando; 
       chcksm = chcksm ^ packet_o[4];
       packet_o[5] = codigo; 
       chcksm = chcksm ^ packet_o[5];
       for (int i=0; i<ldata; i=i+1){
               packet_o[6+i] = data[i];
               chcksm = chcksm ^ packet_o[5+i];
        } 
       packet_o[6+ldata] =  chcksm; 
       packet_o[7+ldata] =  0xFD;
       for (int i=0; i<8+ldata;i=i+1){
          Serial.write(packet_o[i]);
       }
    }



int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 Tamaño:   Bytes de data 0xDD;
    2 Direccion:0xXX; (0x10)
    3 Comando:  0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    4 Data :    D1: codigo (buffer a escribir/borrar 0->4), D1: prioridad (ver explicacion de prioridades y buffers)
    5 Chksum:   Cabecera (Xor) Direccion (Xor) Commando (Xor) Data ;
    
=============================================================================================================================*/
/*============================================================================================================================

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================/
  
    if (Serial.available()) {
      
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      packet[packet_counter] = Serial.read();
      //Serial.write(packet_counter);
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {packet_counter= packet_counter+1;}//systemMessage("  x");}
      else if(packet_counter==3 && packet_ini>0 && packet[packet_counter]!=address && packet[packet_counter]!=0) {packet_counter= 0; packet_ini=0;}
      else if(packet_counter==3 && packet_ini>0 && (packet[packet_counter]==address || packet[packet_counter]==0)) {packet_counter= packet_counter+1;}//systemMessage("   x");}
      else if(packet_counter==4 && packet_ini>0){ command=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage("    x");}
      else if(packet_counter<5+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==5+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;}//systemMessage("       x!");}
    }
    
  }

 */
