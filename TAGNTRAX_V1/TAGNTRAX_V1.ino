/*=================================================================================
////////////////////////  SOFTWARE UGMSG SIGMA TELECOM  ///////////////////////////
===================================================================================

INGENIERO: FELIPE EDUARDO BRAVO SILVA
NOMBRE P.: SISTEMA DE MENSAJES EN PANELES LED UGMSG
ESTADO   : EN DESARROLLO
FECHA ACT: 26-09-16
FECHA COM: 1-09-16
PROTOCOLO: CUSTOM FBS PROTOCOL (PROPIETARIO DE FELIPE BRAVO BAJO LICENCIA COPYLEFT)


=================================================================================*/



#include <Wire.h>
#define SLAVE_ADDRESS 0x04

#define DISPLAYS_WIDE 4
#define DISPLAYS_HIGH 1



/*===========================================================
///////////////////////  SYS VARS   /////////////////////////
===========================================================*/

/*/----SETUP---- /*/
int const test = 0;

/*/----Start---- /*/

/*/-- address--/*/
int address= 0x0B; //direccion de este equipo
int master_ad= 0xFF;
int zone=0;

/*/-- System --/*/
int doCommand=0;
int bufferToWrite=0;
int app_id=0x03; 
int currentBuffer=4;
int currentLine=0;
const int max_per_line=15;
int number = 0;
int state = 0;
int sendflag=0;
int i2ciocounter=0;
int getflag=0;
int antenna=0;
int tagwarning=0;
int last_frame=0;
/*===================================================================*/
byte idTable[100][18];            //tabla IDs
int idFrames=100;
/*-------------------------------------------------------
ID : tag number (12 Hex numbers)
TZA: A reader's reading order
TZB: B reader's reading order
Z  : matched zone
NRC: Not readed counter
InUse: db space being used (1), ready to send (2), or empty space (0)

|  ID   |  Zcase | Z | NRC | InUse |  CRC
   12        1     1    1      1      all ID bytes Xor
 -------------------------------------------------------*/
const int zcase=12; //location of zcase byte inside the database array
const int z=13;  //location of z byte inside the database array
const int NRC=14;   //location of NRC byte inside the database array
const int inUse=15; //location of InUse byte inside the database array
const int IDCRC = 16; //location of ID CRC byte inside the database array
const int IDSUM = 17; //location of ID CRC byte inside the database array
/*===================================================================*/
byte tagbuffer[14];


/*/----i2c---- /*/
byte i2cdata[255];
int  i2csize= 1;


/*/-- packet data --/*/
int command=0, data[230];
int code=0;
int r_zone;
int r_master_ad=0;
int r_ad=0;
int r_app_id=0;
int r_chck=0;
//-- RFID A
int t_type=0;
int t_chck=0;
//-- RFID B
int t_type_2=0;
int t_chck_2=0;

/*/-- packet bytes --/*/
  int o_address = 0x00;
  int packet[255];
  int packet_o[255];
  int packet_counter=0;
  int packet_ini=0;
  int packet_end=0;
  int packet_size=0;
  int last_psize= 0;
  const int frames_per_packet= 2; //max number of frames inside a single data packet sent to the server, i2c limit is 2 (20 bytes max)
  const int framesize=15;          //size in bytes of every frame inside the data packet sent to the server


/*/-- packet bytes RFID--/*/
  //-- RFID A
  int packet_t[50];
  int packet_t_counter=0;
  int packet_t_ini=0;
  int packet_t_end=0;
  int packet_t_size=0;
  int t_in_data[50];
  //-- RFID B
  int packet_t_2[50];
  int packet_t_counter_2=0;
  int packet_t_ini_2=0;
  int packet_t_end_2=0;
  int packet_t_size_2=0;
  int t_in_data_2[50];
  //-- Buffer
  int dataBufferLength=0;
  int dataBuffer_rfid_A[20];
  int dataBuffer_rfid_B[20];
  byte i2cOBuffer[50];

  
/*/-- system time handle --/*/
  unsigned long start_t=0 ;  //SERIAL
  unsigned long work_last=0;
  unsigned long buzzer_start=0 ;  //SERIAL
  unsigned long buzzer_current=0;  
  unsigned long work_current=0;
  const unsigned long work_frame=600;
  const unsigned long answer_time=600;
  const unsigned long line_time=2000;
  unsigned long line_time_start=0;
  unsigned long line_time_current=0;
  unsigned long line_time_start_rfid_A=0;
  unsigned long line_time_current_rfid_A=0;
  unsigned long line_time_start_rfid_B=0;
  unsigned long line_time_current_rfid_B=0;
  unsigned long line_time_start_countup=0;
  unsigned long line_time_current_countup=0;
  const unsigned long countup_time=1000;  //time in miliseconds for no read counter (NRC) increase
  const unsigned long rfid_read_time=300;
  const byte maxNRC = 120;// max no read counter in countup_time units (maxNRC x countup_time = max storage time in miliseconds),an ID frame reaching this value will be open to deletion, unless it's marked as ready to send (inUse=2) 

  int debug=0;
  const int sys_debug=0;
  
/*========================================================================
    
    flags:
        0:set => 0 sin mensajes, 1 con mensaje, 2+ con mensaje emergencia 
               si setea mensaje emergencia, toma prioridad sobre otros 
               mensajes de emergencia(set+1 del mensaje de emergencia 
               con mayor prioridad), si existe almenos 1 mensaje de 
               emergencia, los mensajes normales son ignorados.
               Si solo existe un mensaje de emergencia, tomara el valor 2.
        1:largo => indica el largo del mensaje
        
========================================================================*/

/*========================================================================

chars especiales:
$ : escape
& : vehiculo
@ : exclamacion en triangulo
^ : peligro electrico
{ : fuego
} : derrumbe
~ : eñe
0x7F : FULL

========================================================================*/

/*===========================================================
/////////////////////////  SETUPS   /////////////////////////
===========================================================*/


void setup() {
  Serial.begin(230400);
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);
  Serial1.begin(2400);
  Serial2.begin(115200);
  Serial3.begin(115200); 
  startDelta();
  startDelta_rfid_A();
  startDelta_rfid_B();
  startDelta_countup();
  //testEPC();
  i2cdefault();
}


/*===========================================================
/////////////////////////  LOOP    //////////////////////////
===========================================================*/

void loop() {
  check_coms();
  check_functions();
  check_countup();
  if(getflag==0) getAllEPC();
}

/*===========================================================
/////////////////////////  FUNCIONES  ///////////////////////
===========================================================*/
//////----------demo ------------/////////////////


/////////////============= DEBUG ===============////////////
 
 

/////////////============= SYSTEM ===============////////////

int check_coms(){
    packet_check();
    packet_check_RFID_A();
    packet_check_RFID_B();
  }

int doFunction(int doCommand_){
  if (sys_debug==1) Serial.write(0xFA);
  if (sys_debug==1) Serial.write(doCommand_);
  switch(doCommand_){

    case 0   :    sendRTSEPC();                  
                  break;
    case 1   :    deleteFrames();
                  sendRTSEPC();                  
                  break;
    case 2   :    setTime();
                  break;
    case 0xA0:    savetag();
                  break;
    case 0xA1:    sendtag();
                  break;  
    case 0xA2:    sendtagR();
                  break; 
    case 0xA3:    sendtagR2();
                  break;               
    default  :    error(0x03);
                  break;
  }
}

//send_packet(comnd,0x0,0xFF, lcounter);

void i2cStoreData(int dataSize){
    for(int i=0; i<dataSize;i++){
          i2cdata[i+1]= data[i];
      }
    i2csize=dataSize;  
  }


void i2cCleanData(){
    for(int i=0; i<0x20;i++){
          i2cdata[i+1]= 0;
      }
    i2csize=2;  
  }

void i2cdefault(){
   i2cdata[0] = 5 ;
   i2cdata[1] = 0 ;
   i2csize = 2 ;
  }  
  
void setTime(){
    i2cdata[0]=3;
    i2cStoreData(last_psize);
  }

void savetag(){
    if(test==0) if (sys_debug==1) Serial.write("<savetag>");  
    savedataTest();                
    getAllEPC();
    i2cdata[0]=0x04;
    i2csize=last_psize;
  }

void sendtag(){
    i2cdata[0]=0xA1;
    i2cStoreData(last_psize);
  }

void sendtagR(){
    i2cdata[0]=0xA2;
    i2cStoreData(last_psize);
  }

void sendtagR2(){
    i2cdata[0]=0xA3;
    i2cStoreData(last_psize);
  }

int sendRTSEPC(){
      int frames= getEPC();
      int d_size= frames* framesize;
      int commando = 0;
      if (frames>0) commando =2;
      send_packet(commando,frames,0xFF, d_size);
  }  

void geti2cCRC(){
      byte CRCi2c = 0;
      int SUMi2c = 0;
      for(int i=0; i<0x18 ; i++){
          CRCi2c = CRCi2c ^ (byte)i2cdata[i];
          SUMi2c = SUMi2c + i2cdata[i];
        }
      i2cdata[0x19] = CRCi2c;
      i2cdata[0x18] = lowByte(SUMi2c);
  }

void sendData() {
   //Serial.print("\n<sending i2c>\n");
   geti2cCRC();
   Wire.write(i2cdata,0x20);
   i2cdefault();
}  
  
void receiveData(int byteCount) {
      int lcounter=0;
      //Serial.print("\n<receiving i2c>\n");
      int comnd  = Wire.read();
      //if (comnd == 0) {Serial.write(0xBB);return;}
      //else 
      //Serial.write(0xAB);
      while(Wire.available()) {
        data[lcounter] = Wire.read();
        lcounter++;
     }
     i2ciocounter=lcounter;
     if (comnd == 0xA1) {send_packet(comnd,0x0,0xFF, lcounter);}
     else if (comnd == 0xA2) deleteTagFrame(data[0]);
     getflag=0;
  }

int deleteTagFrame(int frametodelete){
      if (sys_debug==1) {
      Serial.print("\n deleting frame ");
      Serial.print(frametodelete);
      Serial.print("\n");
      }
      idTable[frametodelete][inUse]=0;
      for(int i=0; i<16;i++){
          idTable[frametodelete][i]=0;
        }
           getflag=0;
  }


int check_functions(){
    if(checkPacket()==1){
      doFunction(doCommand);
      doCommand=0;
    }
    check_rfid_tags();
  }

int check_rfid_tags(){
    check_rfid_time();
    checkPacket_rfid();
  }


int check_rfid_time(){
    if (getDelta_rfid_A()> rfid_read_time){
        startDelta_rfid_A();
        request_rfid_tags(1);
      } //else printHex(getDelta_rfid_A(),6);
      
    if (getDelta_rfid_B()> rfid_read_time){
        startDelta_rfid_B();
        request_rfid_tags(2);
      }
  }

int getData(int ldata){   
    for (int i=0; i<ldata+2; i=i+1){
               data[i]=packet[8+i];
     } 
}


int error(int code){
 /*
  *  Error codes:
  *  0x00: buffer incorrecto
  *  0x01: largo incorrecto de mensaje
  *  0x02: codigos de error 0x00 y 0x01 juntos
  *  0x03: comando incorrecto/ error de sistema 
  */
  send_packet(0x0E, code, 0xFF, 0x00);
}

///////////////============= TAG SYSTEM ===============/////////////

/*------------------------------------------------------------------
 * byte idTable[600][16];            //tabla IDs
 * 
 * byte cache_a[50][12];             //ID cache lector A
 * byte cache_b[50][12];             //ID cache lector B
 * --------------------------------------------------------------------
 * ID : tag number (12 Hex numbers)
 * TZA: A reader's reading order
 * TZB: B reader's reading order
 * Z  : matched zone
 * NRC: Not readed counter
 * InUse: db space being used (1), ready to send (2), or empty space (0)
 * 
 * |  ID   |  Zcase | Z | NRC | InUse
 *    12        1     1    1      1
 *    
 *  zcase=12; //location of zcase byte inside the database array
 *  z=13;  //location of z byte inside the database array
 *  NRC=14;   //location of NRC byte inside the database array
 *  inUse=15; //location of InUse byte inside the database array
 * 
 *    Tag data
 *    Data 2  -> EPC byte 11 (MSB)
 *    Data 3  -> EPC byte 10
 *    .....
 *    Data 12 -> EPC byte 1
 *    Data 13 -> EPC byte 0  (LSB)
 * 
 *    Data 14 -> RSSI MSB
 *    Data 15 -> RSSI LSB
 * 
 *    Data 16 -> AntenaID
 * -------------------------------------------------------------------
 *  cache
 * 
 * |  ID   |  InUse
 *    12        1
 *    
 * ------------------------------------------------------------------*/

int getEPC(){   //get tags ready to be sent
  
      int frame=0,f_count;
      for(frame=0; frame<idFrames && f_count<frames_per_packet; frame++){
            if(idTable[frame][inUse]==2 && idTable[frame][NRC]>=maxNRC) {
                  for(int j=0; j<12; j++){
                        data[j+(framesize*f_count)]= idTable[frame][j];
                    }
                    data[12+(framesize*f_count)]= (byte)frame;
                    data[13+(framesize*f_count)]= idTable[frame][z];
                    f_count++;              
              }
      }
      
      for(frame=0; frame<idFrames && f_count<frames_per_packet; frame++){
            if(idTable[frame][inUse]==2 && idTable[frame][NRC]<maxNRC) {
                  for(int j=0; j<12; j++){
                        data[j+(framesize*f_count)]= idTable[frame][j];
                    }
                    data[12+(framesize*f_count)]= (byte)frame;
                    data[13+(framesize*f_count)]= idTable[frame][z];
                    f_count++;              
              }
      }
      return f_count;     
  }

int checkEPC(){
      int frame=0,f_count=0;
      for(frame=0; frame<idFrames; frame++){
            
            if(idTable[frame][inUse] >= 1 ) {
                  for(int j=0; j<12; j++){
                        f_count= f_count+idTable[frame][j];
                    }
                    if (f_count==0)  idTable[frame][inUse]=0;  
                        f_count=0;      
              }         
      }
  }


int getAllEPC(){   //get tags ready to be sent
      int offset = 2;
      int exor_=0;
      int sum_=0;
      i2cCleanData();
      checkEPC();
      int frame=0,f_count=0;
//      sys_debug=1;
      //Serial.print("<getting EPCs >\n");
      for(frame=last_frame+1; frame<idFrames && f_count<frames_per_packet; frame++){
            
            if((idTable[frame][inUse] == 1 || idTable[frame][inUse] == 2) && (idTable[frame][z]==1 || idTable[frame][z]==2) ) {
                    if (sys_debug==1) {Serial.write("\n<sending frame:");Serial.print(frame);Serial.print(":");Serial.print(idTable[frame][inUse]);Serial.print(" - ");   }         
                  exor_=0;
                  sum_=0;
                  for(int j=0; j<12; j++){
                        //if (j==0 && idTable[frame][j]!=0xE2 ) 
                        i2cdata[j+(framesize*f_count)+offset]= idTable[frame][j];
                        exor_=exor_ ^ i2cdata[j+(framesize*f_count)+offset];
                        sum_=sum_+ i2cdata[j+(framesize*f_count)+offset];
                        if (sys_debug==1) {printHex(i2cdata[j+(framesize*f_count)+offset],2);  }  
                    }
                    i2cdata[12+(framesize*f_count)+offset]= idTable[frame][inUse];
                    if (sys_debug==1) {printHex(i2cdata[12+(framesize*f_count)+offset],2);}
                    i2cdata[13+(framesize*f_count)+offset]= idTable[frame][z];
                    if (sys_debug==1) {printHex(i2cdata[13+(framesize*f_count)+offset],2);}
                    i2cdata[14+(framesize*f_count)+offset]= frame;
                    if (sys_debug==1) {printHex(i2cdata[14+(framesize*f_count)+offset],2);}
                    if(exor_!=idTable[frame][IDCRC]){
                        Serial.print("\n CRC ERROR ");
                        idTable[frame][inUse]=0; 
                    } else if(sum_==0 || lowByte(sum_)!= idTable[frame][IDSUM]){
                        Serial.print("\n SUM ERROR ");
                        idTable[frame][inUse]=0; 
                    } else f_count++; 
                    if (sys_debug==1) {Serial.print(">\n");   }  
                    
                    //idTable[frame][inUse]=0;             
              }
      }
      
      for(frame=0; frame<idFrames && frame<=last_frame && f_count<frames_per_packet; frame++){
//            sys_debug=1;
            if((idTable[frame][inUse] == 1 || idTable[frame][inUse] == 2) && (idTable[frame][z]==1 || idTable[frame][z]==2) ) {
                    if (sys_debug==1) {Serial.write("\n<sending frame:");Serial.print(frame);Serial.print(":");Serial.print(idTable[frame][inUse]);Serial.print(" - ");   }         
                  exor_=0;
                  sum_=0;
                  for(int j=0; j<12; j++){
                        //if (j==0 && idTable[frame][j]!=0xE2 ) 
                        i2cdata[j+(framesize*f_count)+offset]= idTable[frame][j];
                        exor_=exor_ ^ i2cdata[j+(framesize*f_count)+offset];
                        sum_=sum_+ i2cdata[j+(framesize*f_count)+offset];
                        if (sys_debug==1) {printHex(i2cdata[j+(framesize*f_count)+offset],2);  }  
                    }
                    i2cdata[12+(framesize*f_count)+offset]= idTable[frame][inUse];
                    if (sys_debug==1) {printHex(i2cdata[12+(framesize*f_count)+offset],2);}
                    i2cdata[13+(framesize*f_count)+offset]= idTable[frame][z];
                    if (sys_debug==1) {printHex(i2cdata[13+(framesize*f_count)+offset],2);}
                    i2cdata[14+(framesize*f_count)+offset]= frame;
                    if (sys_debug==1) {printHex(i2cdata[14+(framesize*f_count)+offset],2);}
                    if(exor_!=idTable[frame][IDCRC]){
                        Serial.print("\n CRC ERROR ");
                        idTable[frame][inUse]=0; 
                    } else if(sum_==0 || lowByte(sum_)!= idTable[frame][IDSUM]){
                        Serial.print("\n SUM ERROR ");
                        idTable[frame][inUse]=0; 
                    } else f_count++; 
                    if (sys_debug==1) {Serial.print(">\n");   }  
                    
                    //idTable[frame][inUse]=0;                   
              }
      }
      last_frame=frame;
//      sys_debug=0;
      i2cdata[offset-2]= 4;
      i2cdata[offset-1]= f_count;
      last_psize = offset + (f_count * 14 ) ;
      if(f_count>0) getflag=1;     
      if(f_count>0) if (sys_debug==1) Serial.write(f_count); 
      //Serial.write((byte)(f_count+0x30));
      return f_count;     
  }  

int testEPC(){   

      int frame=0,f_count;
      for(frame=0; frame<idFrames && f_count<20; frame++){
                  for(int j=0; j<12; j++){
                        idTable[frame][j]=frame;
                    }
                    if (sys_debug==1) Serial.write("test");
                    idTable[frame][inUse]=1;
                    idTable[frame][z]=1;
                    idTable[frame][NRC]=0;
                    f_count++;              
      }
      return f_count;     
  }  

int deleteFrames(){
      getData(packet_size);
  }

int sendRTSEPC2(){
      int frames= getEPC();
      int d_size= frames* framesize;
      int commando = 0;
      if (frames>0) commando =2;
      send_packet(commando,frames,0xFF, d_size);
  }  

int findTag_db (byte tagid[]){
      int i = 0, j=0, noC=0,found = 0;

      for(i = 0 ; i<idFrames && found==0 ;  i++){                        // while no result found inside the 400 ID frames
            for (j=0;j<12 && noC==0 && idTable[i][inUse]!=0;j++) {  // while the numbers match, and the frame is not empty
                if ( idTable[i][j] == tagid[j] )  noC=0;            // if they match, no coincidences is marked as false
                else noC=1;                                         // else, no coincidences is marked as true
              }
            if (j==12 && noC==0) found = i+1;                       //  if all 12 bytes match, the frame is registered as found +1 
        }
      return found;                                                 //returns position on table +1, 0 means no tags found
  }

int findEmpty_db (){
      int i = 0, found = 0;

      for(i = 0 ; i<idFrames && found==0 ;  i++){                        // while no result found inside the 400 ID frames
            if (idTable[i][inUse]==0) found = i+1;                  // if inUse=0, register as empty frame available +1
        }
      return found;                                                 //returns empty frame available +1, 0 means no frames found
  }
  
int getTagNumber(int lector){
      //t_in_data  
      int sum_ = 0;
      int xor_= 0;
      //Serial.print("\n ");
      for (int i=0; i<12; i++){
             
             switch (lector){
                case 1: tagbuffer[i]=dataBuffer_rfid_A[i+1]; 
                        break;
                case 2: tagbuffer[i]=dataBuffer_rfid_B[i+1];
                        break;
                default:tagbuffer[i]=dataBuffer_rfid_B[i+1];
                        break;              
              }   
              sum_ = sum_ + tagbuffer[i]; 
              xor_ = xor_ ^ tagbuffer[i];
              if (sys_debug==1)  if(i==0 && tagbuffer[i]!=0xE2) { Serial.print(" WARNING ! : "); tagwarning=1;}
              else tagwarning=0;
              if (sys_debug==1) {printHex(tagbuffer[i], 2);   }
        }
      tagbuffer[12]=xor_;
      tagbuffer[13]=lowByte(sum_);
      return sum_;
  }  

void printHex(int num, int precision) {
     char tmp[16];
     char format[128];

     sprintf(format, "0x%%.%dX", precision);

     sprintf(tmp, format, num);
     Serial.print(tmp);
     Serial.print(" ");
}
 
int savedata (int lector){  
     //sys_debug=1;
     if (sys_debug==1) Serial.print("\n<saved rfid>");
     if (sys_debug==1) Serial.print("<Lector ");
     if (sys_debug==1) Serial.print(lector);
     if (sys_debug==1) Serial.print(" ");
     int num_ = getTagNumber(lector);                     //gets tag number from reader's in-data buffer
     if (sys_debug==1 && num_ == 0 ) Serial.print(" <notag>\n");
     else if (sys_debug==1) Serial.print(">\n");
     if (num_ == 0 ) return 0;
     else if(tagwarning==1) return 0;
     int frame = 0 ;
     int  found = findTag_db(tagbuffer);          //searchs tag number on database 
     int testzone=1;
          if(antenna>0 && antenna<3) testzone=1;
          else testzone=2;
          if (sys_debug==1) {
          Serial.print(" \nantenna : ");
          Serial.print(testzone);
          Serial.print(" \n");
          }
     if (found>0){                             //if tag number is found
          frame= found-1;
          idTable[frame][NRC]=0;
          
          switch(lector){
            
              case 1: if(idTable[frame][zcase]==2) idTable[frame][inUse]=2;  
                      idTable[frame][z]=testzone;
                      break;
              case 2: if(idTable[frame][zcase]==1) idTable[frame][inUse]=2;
                      idTable[frame][z]=testzone; 
                      break;
            
            }
      
      } else {
          frame = findEmpty_db();
          for (int j=0;j<12 ;j++) {  
                idTable[frame][j] = tagbuffer[j];   
              }
          idTable[frame][IDSUM]=tagbuffer[13]; 
          idTable[frame][IDCRC]=tagbuffer[12]; 
          idTable[frame][zcase]=testzone;
          idTable[frame][z]=testzone; 
          idTable[frame][inUse]=1;  
          idTable[frame][NRC]=0;     
      }
          if (sys_debug==1) {
          Serial.print(" saving in frame "+frame);
          Serial.print("as inuse "+idTable[frame][inUse]);
          Serial.print("\n");
          }
     //sys_debug=0;
  }

int getTagNumberTest(){
      //t_in_data  
      int sum_ = 0;
      for (int i=0; i<12; i++){
             tagbuffer[i]=data[i];
             sum_ = sum_ + tagbuffer[i]; 
        }
      return sum_;
  } 
  
int savedataTest (){
     if (sys_debug==1) {Serial.write("<");Serial.print(data[12]);Serial.write(">");}
     int num_ = getTagNumberTest();                     //gets tag number from reader's in-data buffer
     if (sys_debug==1) {Serial.write("<::");Serial.print(num_);Serial.write(">");}
     if (num_ == 0 ) return 0;
     int  found = findTag_db(tagbuffer);          //searchs tag number on database 
     if (found>0){                             //if tag number is found
          int frame= found-1;
          if (sys_debug==1) {Serial.write("<saving frame:");Serial.print(frame);Serial.write(">");}
          idTable[frame][NRC]=0;
          switch(data[12]){
            
              case 1: if(idTable[frame][zcase]==2) idTable[frame][inUse]=2;  
                      idTable[frame][z]=1;
                      break;
              case 2: if(idTable[frame][zcase]==1) idTable[frame][inUse]=2;
                      idTable[frame][z]=2; 
                      break;
              default: break;
            
            }
      
      } else {
          int frame = findEmpty_db();
          if (sys_debug==1) {Serial.write("<saving frame:");Serial.print(frame);Serial.write(">");}
          int sum_ = 0;
          for (int j=0;j<12 ;j++) {  
                idTable[frame][j] = tagbuffer[j];  
                sum_ = sum_ + idTable[frame][j]; 
              }
          if (sum_==0) idTable[frame][inUse]=0; 
          else idTable[frame][inUse]=1; 
          idTable[frame][z]=data[12];
          idTable[frame][NRC]=0;     
      }
  }


/////////////============= Time and Data ============////////////

long getDelta(){
    line_time_current=millis();
    return line_time_current-line_time_start;
  }

int startDelta(){
    line_time_current=millis();
    line_time_start=line_time_current;
  }

long getDelta_rfid_A(){
    line_time_current_rfid_A=millis();
    return line_time_current_rfid_A-line_time_start_rfid_A;
  }

int startDelta_rfid_A(){
    
    line_time_start_rfid_A=millis();
    line_time_current_rfid_A=millis();
  }


long getDelta_rfid_B(){
    line_time_current_rfid_B=millis();
    return line_time_current_rfid_B-line_time_start_rfid_B;
  }

int startDelta_rfid_B(){
    line_time_current_rfid_B=millis();
    line_time_start_rfid_B=line_time_start_rfid_B;
  }

long getDelta_countup(){
    line_time_current_countup=millis();
    return line_time_current_countup-line_time_start_countup;
  }

int startDelta_countup(){
    line_time_current_countup=millis();
    line_time_start_countup=millis();
  }

/*-----------------------------------
 * line_time_start_countup=0;
 * line_time_current_countup=0; 
 * countup_time=1000;
 * 
 * 
-------------------------------------*/

void check_countup(){
     // if (getDelta_countup()> countup_time){
     //     startDelta_countup();
     //     countup();
     // }
  }
 //maxNRC

int deleteTimedOutFrame(int frame){
      if(idTable[frame][NRC]>=maxNRC) idTable[frame][inUse]=0; //ID frame marked as deleted/not used, saves time not deleting the whole frame, also useful for searching reasons  
  }
 
int countup(){
      for(int i = 0 ; i<idFrames ;  i++){   
            if (idTable[i][inUse]!=0){
                if (idTable[i][NRC]<255) idTable[i][NRC]++;                   
                if (idTable[i][inUse]==1) deleteTimedOutFrame(i);
              }  
        }
  }
  
/////////////============= MISC ============////////////


/////////////=============  COMM  ==============/////////////

int checkPacket(){
  int result=0;
  if(packet_end==1){
      doCommand=command;
      getData(packet_size);
      last_psize=packet_size;
      result=1;
      clear_serial();
  }
  return result;
}


void debug_serial_a(){
   if (sys_debug==1) Serial.print("\n<packet: ");
   for(int i=0;i<packet_t_counter;i++){
        printHex(packet_t[i], 2);
    }
   if (sys_debug==1)  Serial.print(">\n");
  // 
  }

bool CRC_a(){
   int sumx=0;
   for(int i=1;i<packet_t_counter-3;i++){
        sumx=sumx+packet_t[i];
    }
    sumx= lowByte(sumx);
    if (sys_debug==1) {
        Serial.print("\n<CRC : ");
        printHex(sumx,2);
        Serial.print(" Vs ");
        printHex(packet_t[packet_t_counter-3],2);
        Serial.print(">\n");
    }
    if (packet_t[packet_t_counter-3]==sumx){
           return 1;
    }else{ 
      if (sys_debug==1) {Serial.print("\n<CRC : FAIL >\n ");}
      return 0;}
  // 
  }


int checkPacket_rfid(){
  int result=0;
  if(packet_t_end==1){
      if(CRC_a()){
          if (sys_debug==1) debug_serial_a();
          startDelta_rfid_A();
          if(getData_rfid(1)==1) savedata(1);      
          clear_serialA();
        } else {
          if (sys_debug==1) debug_serial_a();
          clear_serialA();      
        }    
  } 
  if(packet_t_end_2==1 ){
      startDelta_rfid_B();
      //getData_rfid(2);
      //savedata(2);
      clear_serialB();
      //Serial.write(0x0b);
      
  }
  return 1;
}

int wait_answer(){
    unsigned long current_t=0, delta_t=0;
    start_t=millis();

    while(delta_t<answer_time){
      current_t = millis();
      delta_t = current_t-start_t;
      packet_check();
      if(packet_end==1)  {  return 1; }
    }
    return 0;
  }

int getData_rfid(int reader){  
  switch (reader) {
      case 1:  if (t_type==0x96 && packet_t_size==0x11){
                    line_time_start_rfid_A=line_time_start_rfid_A-rfid_read_time;
                    antenna= packet_t[19];
                    getflag=0;
                    if (sys_debug==1) Serial.print("<getting data: "); 
                    for (int i=0; i<0x11; i++){
                        dataBuffer_rfid_A[i]=packet_t[4+i];
                        if (sys_debug==1) printHex(dataBuffer_rfid_A[i], 2);                         
                    } 
                    if (sys_debug==1) Serial.print(">\n");
                    return 1; 
                }
               return 0; 
                             

      case 2:  for (int i=0; i<packet_t_size_2 && t_type_2==0x96; i=i+1){
                        dataBuffer_rfid_B[i]=t_in_data_2[i];
               }
               break;

      default: break;
    
    }
    return 0;
}

int clear_serial(){
  
  for(int i=0; i<10; i=i+1){
      packet[i]=0;
    }
    packet_end=0;
    packet_ini=0; 
    packet_counter=0;
    packet_size=0;
}  


int clear_serialA(){
  
  for(int i=0; i<30; i=i+1){
      packet_t[i]=0;
      t_in_data[i]=0;
    }
    //-- RFID A
    t_type=0;
    t_chck=0;
    packet_t_counter=0;
    packet_t_ini=0;
    packet_t_end=0;
    packet_t_size=0;  
}  


int clear_serialB(){
  
  for(int i=0; i<30; i=i+1){
      packet_t_2[i]=0;
      t_in_data_2[i]=0;
    }
    //-- RFID B
    t_type_2=0;
    t_chck_2=0;
    packet_t_counter_2=0;
    packet_t_ini_2=0;
    packet_t_end_2=0;
    packet_t_size_2=0;  
}  



int clear_serial_rfid_A(){
     
  for(int i=0; i<30; i=i+1){
      packet_t[i]=0;
      t_in_data[i]=0;
    }
    //-- RFID A
    t_type=0;
    t_chck=0;
    packet_t_counter=0;
    packet_t_ini=0;
    packet_t_end=0;
    packet_t_size=0;   
}  

int clear_serial_rfid_B(){
     
  for(int i=0; i<30; i=i+1){
      packet_t_2[i]=0;
      t_in_data_2[i]=0;
    }
    //-- RFID B
    t_type_2=0;
    t_chck_2=0;
    packet_t_counter_2=0;
    packet_t_ini_2=0;
    packet_t_end_2=0;
    packet_t_size_2=0;   
}  

  
//////////=============  PROTOCOLO  ==============//////////
  

int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera      :   0xFE;
    1 app id        :   id de aplicacion (3 en este caso)
    2 master address:   direccion de unidad maestra
    3 address       :   direccion de este dispositivo
    4 zone          :   zona de ubicacion de este dispositivo
    5 largodatos    :   largo de datos ( 1 comando + 1 codigo + largo)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
_____________________________________________________________________________________________________________________________

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================*/
  
    if ((Serial1.available() && test==0) || (Serial.available() && test==1) ) {
      
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      if(test==1) packet[packet_counter] = Serial.read();
      else {
            packet[packet_counter] = Serial1.read();
            if (sys_debug==1) Serial.write(packet_counter+0x30);
        } 
      //Serial.write(packet_counter);
      if(packet_counter==6 && packet_ini>0 && packet_size>0 )  {command=packet[packet_counter];}//systemMessage("    x");}
      else if(packet_counter==7 && packet_ini>0 && packet_size>1)  {code=packet[packet_counter];}//systemMessage("    x");}
      
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {r_app_id=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {r_master_ad=packet[packet_counter]; packet_counter= packet_counter+1;if (r_master_ad!=address && r_master_ad!=0) {packet_counter=0; packet_size=0; packet_ini=0;}}//systemMessage(" x");}
      else if(packet_counter==3 && packet_ini>0)  {r_ad=packet[packet_counter]; packet_counter= packet_counter+1;  }//systemMessage(" x");}
      else if(packet_counter==4 && packet_ini>0)  {r_zone=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==5 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}     
      else if(packet_counter<6+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==6+packet_size && packet_ini>0){r_chck=packet[packet_counter];packet_counter= packet_counter+1;}//systemMessage("       x!");}
      else if(packet_counter==7+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;}//systemMessage("       x!");}
    }
    
  }  

  int checksum(){
       int checksm = packet[1];
       for (int i=2; i<packet_size+8; i=i+1){
           checksm = packet[i] ^ checksm;
        }  
       if(checksm==packet[packet_size+8]) return 0;
       else {               
             //Serial.print("::: chk: ");
             //Serial.print(checksm);
             //Serial.print(" pchk: ");
             //Serial.print(packet[packet_size+5]);
             //Serial.print(" ::: ");
             return 1;
       }
    }
       

  int send_packet(int commando, int codigo, int s_address, int ldata){
    
    
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de unidad maestra
    3 address       :   direccion de este dispositivo
    4 zone          :   zona de ubicacion de este dispositivo
    5 largo         :   largo de data ( 1 comando + 1 codigo + datos)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
=============================================================================================================================*/

         int chcksm=0;
         packet_o[0] = 0xFE; 
         packet_o[1] = app_id;
         chcksm = chcksm ^ packet_o[1];
         packet_o[2] = master_ad;
         chcksm = chcksm ^ packet_o[2];
         packet_o[3] = address;
         chcksm = chcksm ^ packet_o[3];
         packet_o[4] = zone;
         chcksm = chcksm ^ packet_o[4];
         packet_o[5] = ldata+2;
         chcksm = chcksm ^ packet_o[5];
         packet_o[6] = commando;
         chcksm = chcksm ^ packet_o[6];
         packet_o[7] = codigo;
         chcksm = chcksm ^ packet_o[7];
         for (int i=0; i<ldata; i=i+1){
               packet_o[8+i] = data[i];
               chcksm = chcksm ^ packet_o[8+i];
        } 
        packet_o[8+ldata] =  chcksm; 
        packet_o[9+ldata] =  0xFD;
        for (int i=0; i<4;i++){
          if(test==1) Serial.write(0);
          else Serial1.write(0);
        }
        for (int i=0; i<10+ldata;i=i+1){
          if(test==1) Serial.write(0);
          else Serial1.write(0);
       }
    }

    
int send_packet_RFID_A(int type, int ldata){
          send_packet_RFID( type, ldata, 1);
  }

  int send_packet_RFID_B(int type, int ldata){
          send_packet_RFID( type, ldata, 2);
  }
         
int send_packet_RFID(int type, int ldata, int lector){
    
  /*===========================================================================================================================

    Estructura:
    n.byte   |   campo      |   detalles
      0       Cabecera      :   0xBB;
      1       Tipo          :   tipo de instruccion
      2       data length   :   largo de data
      3       data          :   datos
      3+largo Chksum        :   suma de 1 byte con override desde byte1 hasta ultimo byte de data (lowByte de suma) ;
      4+largo end1          :   0x0D;
      5+largo end1          :   0x0A;
      
  _____________________________________________________________________________________________________________________________

==============================================================================================================================*/
      
         int chcksm=0;
         packet_o[0] = 0xBB; 
         packet_o[1] = type;
         chcksm = chcksm + packet_o[1];
         packet_o[2] = ldata;
         chcksm = chcksm + packet_o[2];
         for (int i=0; i<ldata; i=i+1){
               packet_o[2+i] = data[i];
               chcksm = chcksm + packet_o[2+i];
        } 
        packet_o[3+ldata] =  lowByte(chcksm); 
        packet_o[4+ldata] =  0x0D; 
        packet_o[5+ldata] =  0x0A;

        for (int i=0; i<6+ldata;i=i+1){
         if (lector==1) Serial2.write(packet_o[i]);
         else if (lector==2) Serial3.write(packet_o[i]);
       }
    }

int request_rfid_tags(int lector){ 
  /*
         int chcksm=0;
         packet_o[0] = 0xBB; 
         packet_o[1] = 0x16;
         packet_o[2] = 0x00;
         packet_o[3] = 0x16;
         packet_o[4] = 0x0D;
         packet_o[5] = 0x0A;
         
        for (int i=0; i<6;i=i+1){
         if (lector==1) Serial2.write(packet_o[i]);
         else if (lector==2) Serial3.write(packet_o[i]);
       }*/
       send_packet_RFID(0x16,0,lector);       
    }

  

    
  

//////////=============  PROTOCOLO RECEPCION LECTOR TAG PASIVOS A ==============//////////
  

int packet_check_RFID_A(){
  
/*===========================================================================================================================

  Estructura:
  n.byte   |   campo      |   detalles
    0       Cabecera      :   0xBB;
    1       Tipo          :   tipo de instruccion
    2       data length   :   largo de data
    3       data          :   datos
    3+largo Chksum        :   suma de 1 byte con override desde byte1 hasta ultimo byte de data ;
    4+largo end1          :   0x0D;
    5+largo end1          :   0x0A;
    
_____________________________________________________________________________________________________________________________

    
==============================================================================================================================*/
    if (Serial2.available()) startDelta_rfid_A();
    while (Serial2.available() && packet_t_end!=1) {
      if ( packet_t_counter > 30) {clear_serial_rfid_A(); Serial.print("*");}
      if(packet_t_counter>0 && packet_t_ini==0){packet_t_counter=0; packet_t_size=0;}
      packet_t[packet_t_counter] = Serial2.read();
      /*if (packet_t_counter==0) Serial.print("\n");
      printHex(packet_t[packet_t_counter],2);
      //printHex(packet_t_counter,2);*/
      if(debug==1) if(sys_debug==1) Serial.print(packet_t_counter);
      if(packet_t[packet_t_counter]==0xBB && packet_t_ini==0) {packet_t[0]==0xBB;packet_t_ini=1; packet_t_counter= packet_t_counter+1;}//systemMessage("x");}//
      else if(packet_t_counter==1 && packet_t_ini>0)  {t_type=packet_t[packet_t_counter]; packet_t_counter= packet_t_counter+1;}//systemMessage(" x");}
      else if(packet_t_counter==2 && packet_t_ini>0)  {packet_t_size=packet_t[packet_t_counter]; packet_t_counter= packet_t_counter+1;}//systemMessage(" x");}     
      else if(packet_t_counter<3+packet_t_size  && packet_t_ini>0 ){t_in_data[packet_t_counter-3]=packet_t[packet_t_counter];packet_t_counter= packet_t_counter+1;}//systemMessage("     x");systemdebug(packet_t_counter);}
      else if(packet_t_counter==3+packet_t_size && packet_t_ini>0){t_chck=packet_t[packet_t_counter];packet_t_counter= packet_t_counter+1;}//systemMessage("       x!");}
      else if(packet_t_counter==4+packet_t_size && packet_t_ini>0 && packet_t[packet_t_counter]==0x0D ){packet_t_counter= packet_t_counter+1;}//systemMessage("       x!");}
      else if(packet_t_counter==5+packet_t_size && packet_t_ini>0 && packet_t[packet_t_counter]==0x0A ){packet_t_end=1; packet_t_counter= packet_t_counter+1;}//systemMessage("       x!");}
      else if(packet_t_ini>0 && packet_t_end!=1) packet_t_counter++;
    }
    
  }  
  

  int checksum_t(){
       int checksm = packet_t[1];
       for (int i=2; i<packet_t_size+8; i=i+1){
           checksm = packet_t[i] ^ checksm;
        }  
       if(checksm==packet_t[packet_t_size+8]) return 0;
       else {               
             //Serial.print("::: chk: ");
             //Serial.print(checksm);
             //Serial.print(" pchk: ");
             //Serial.print(packet_t[packet_t_size+5]);
             //Serial.print(" ::: ");
             return 1;
       }
    }

//////////=============  PROTOCOLO RECEPCION LECTOR TAG PASIVOS B ==============//////////
  

int packet_check_RFID_B(){
  
/*===========================================================================================================================

  Estructura:
  n.byte   |   campo      |   detalles
    0       Cabecera      :   0xBB;
    1       Tipo          :   tipo de instruccion
    2       data length   :   largo de data
    3       data          :   datos
    3+largo Chksum        :   suma de 1 byte con override desde byte1 hasta ultimo byte de data ;
    4+largo end1          :   0x0D;
    5+largo end1          :   0x0A;
    
_____________________________________________________________________________________________________________________________

    
==============================================================================================================================*/
  
    if (Serial3.available()) {
      
      if(packet_t_counter_2>0 && packet_t_ini_2==0){packet_t_counter_2=0; packet_t_size_2=0;}
      packet_t_2[packet_t_counter_2] = Serial3.read();
      /*if(debug==1) Serial3.write(packet_t_2[packet_t_counter_2]);
      Serial.print("<rfidB:");
      Serial.write(packet_t_counter_2+0x30);   
      Serial.print(">"); 
    */
      if(packet_t_2[packet_t_counter_2]==0xBB && packet_t_ini_2==0) {packet_t_2[0]==0xBB;packet_t_ini_2=1; packet_t_counter_2= packet_t_counter_2+1;}//systemMessage("x");}//
      else if(packet_t_counter_2==1 && packet_t_ini_2>0)  {t_type_2=packet_t_2[packet_t_counter_2]; packet_t_counter_2= packet_t_counter_2+1;}//systemMessage(" x");}
      else if(packet_t_counter_2==2 && packet_t_ini_2>0)  {packet_t_size_2=packet_t_2[packet_t_counter_2]; packet_t_counter_2= packet_t_counter_2+1;}//systemMessage(" x");}     
      else if(packet_t_counter_2<3+packet_t_size_2  && packet_t_ini_2>0 ){t_in_data_2[packet_t_counter_2-3]=packet_t_2[packet_t_counter_2];packet_t_counter_2= packet_t_counter_2+1;}//systemMessage("     x");systemdebug(packet_t_counter_2);}
      else if(packet_t_counter_2==3+packet_t_size_2 && packet_t_ini_2>0){t_chck_2=packet_t_2[packet_t_counter_2];packet_t_counter_2= packet_t_counter_2+1;}//systemMessage("       x!");}
      else if(packet_t_counter_2==4+packet_t_size_2 && packet_t_ini_2>0 && packet_t_2[packet_t_counter_2]==0x0D ){packet_t_counter_2= packet_t_counter_2+1;}//systemMessage("       x!");}
      else if(packet_t_counter_2==5+packet_t_size_2 && packet_t_ini_2>0 && packet_t_2[packet_t_counter_2]==0x0A ){packet_t_end_2=1; packet_t_counter_2= packet_t_counter_2+1;}//systemMessage("       x!");}
    }
    
  }  
  




/*===========================================================
/////////////////////////  RECICLAJE  ///////////////////////
===========================================================*/
 /*
 
   int send_packet(int commando, int codigo, int s_address, int ldata){
       int chcksm=0;
       packet_o[0] = 0xFE; 
       packet_o[1] = app_id; //0x02 = app de mensaj
       packet_o[1] = ldata+1; 
       chcksm = chcksm ^ packet_o[1];
       packet_o[2] = address; 
       chcksm = chcksm ^ packet_o[2];
       packet_o[3] = master_ad; 
       chcksm = chcksm ^ packet_o[3];
       packet_o[4] = commando; 
       chcksm = chcksm ^ packet_o[4];
       packet_o[5] = codigo; 
       chcksm = chcksm ^ packet_o[5];
       for (int i=0; i<ldata; i=i+1){
               packet_o[6+i] = data[i];
               chcksm = chcksm ^ packet_o[5+i];
        } 
       packet_o[6+ldata] =  chcksm; 
       packet_o[7+ldata] =  0xFD;
       for (int i=0; i<8+ldata;i=i+1){
          Serial.write(packet_o[i]);
       }
    }



int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 Tamaño:   Bytes de data 0xDD;
    2 Direccion:0xXX; (0x10)
    3 Comando:  0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    4 Data :    D1: codigo (buffer a escribir/borrar 0->4), D1: prioridad (ver explicacion de prioridades y buffers)
    5 Chksum:   Cabecera (Xor) Direccion (Xor) Commando (Xor) Data ;
    
=============================================================================================================================*/
/*============================================================================================================================

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================/
  
    if (Serial.available()) {
      
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      packet[packet_counter] = Serial.read();
      //Serial.write(packet_counter);
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {packet_counter= packet_counter+1;}//systemMessage("  x");}
      else if(packet_counter==3 && packet_ini>0 && packet[packet_counter]!=address && packet[packet_counter]!=0) {packet_counter= 0; packet_ini=0;}
      else if(packet_counter==3 && packet_ini>0 && (packet[packet_counter]==address || packet[packet_counter]==0)) {packet_counter= packet_counter+1;}//systemMessage("   x");}
      else if(packet_counter==4 && packet_ini>0){ command=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage("    x");}
      else if(packet_counter<5+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==5+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;}//systemMessage("       x!");}
    }
    
  }



int request_rfid_tags(){ //PARA ACTIVOS
    
    
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 id            :   id equipo destino
    2 datasize      :   tamaño de data
    3 comando       :   comando al dispositivo
    4 data 1        :   data 1 del comando
    5 data 2        :   data 2 del comando
    6 chck          :   checksum;
    
=============================================================================================================================

         int chcksm=0;
         packet_o_rfid[0] = 0xA5; 
         packet_o_rfid[1] = 0x00;
         packet_o_rfid[2] = 0x04;
         packet_o_rfid[3] = 0x3C;
         packet_o_rfid[4] = 0x02;
         packet_o_rfid[5] = 0xC8;
         packet_o_rfid[6] = 0x51;
         
        for (int i=0; i<7;i=i+1){
         Serial2.write(packet_o_rfid[i]);
       }
    }


//////////=============  CUSTOM DRIVER LECTOR RFID  ==============//////////
  

int packet_check_RFID(){
  
/*===========================================================================================================================

PROTOCOLO PRIVATIVO DEL EQUIPO LECTOR DE TARJETAS RFID

  Estructura:
    0 Cabecera      :   0xE5;
    1 DataBytes H   :   tamaño de data MSB (tamaño incluye CHCK)
    2 DataBytes L   :   tamaño de data LSB (Tamaño incluye CHCK)
    3 ???????????   :   ??????
    4 ???????????   :   ??????
    5 Tarjetas      :   cantidad de tarjetas dentro de la data
    6 ???????????   :   ??????
    
    7 hasta ->   
    7+Databytes-1   :   Data (info de tarjetas)
    7+Databytes     :   CHCK (formula desconocida)

    DATABYTES:
    0 TIPO          :   tipo de tarjeta
    1 hasta ->
    5 ???????????   :   ??????
    6 hasta ->
    8 ID            :   numero ID de la tarjeta
    9 hasta ->
    11 ???????????  :   ??????
   
_____________________________________________________________________________________________________________________________


    
==============================================================================================================================
  
    if (Serial2.available()) {
      
      if(packet_counter_rfid>0 && packet_ini_rfid==0){packet_counter_rfid=0; packet_size_rfid=0;}
      packet_rfid[packet_counter_rfid] = Serial2.read(); 
      

      if     (packet_rfid[packet_counter_rfid]==0xE5 && packet_ini_rfid==0)  {packet_rfid[0]==0xE5;packet_ini_rfid=1; packet_counter_rfid= packet_counter_rfid+1;}//systemMessage("x");}//
      else if(packet_counter_rfid==1 && packet_ini_rfid>0                 )  {packet_data_size_rfid_h=packet_rfid[packet_counter_rfid]; packet_counter_rfid= packet_counter_rfid+1;}//systemMessage(" x");}
      else if(packet_counter_rfid==2 && packet_ini_rfid>0                 )  {packet_data_size_rfid_l=packet_rfid[packet_counter_rfid]; packet_size_rfid=(packet_data_size_rfid_h*256)+ packet_data_size_rfid_l;  packet_counter_rfid= packet_counter_rfid+1;}
      else if(packet_counter_rfid==3 && packet_ini_rfid>0                 )  {packet_counter_rfid= packet_counter_rfid+1;}//systemMessage(" x");}
      else if(packet_counter_rfid==4 && packet_ini_rfid>0                 )  {packet_counter_rfid= packet_counter_rfid+1;}//systemMessage(" x");}
      else if(packet_counter_rfid==5 && packet_ini_rfid>0                 )  {rfid_tags=packet_rfid[packet_counter_rfid]; packet_counter_rfid= packet_counter_rfid+1;}//systemMessage(" x");}
      else if(packet_counter_rfid==6 && packet_ini_rfid> 0                )  {packet_counter_rfid= packet_counter_rfid+1;}//systemMessage(" x");}

      //DATABYTES
      else if(packet_counter_rfid<2+packet_size_rfid  && packet_ini_rfid>0 ){packet_counter_rfid= packet_counter_rfid+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter_rfid==2+packet_size_rfid && packet_ini_rfid>0 ){rfid_chck=packet_rfid[packet_counter_rfid]; packet_end_rfid=1; packet_counter_rfid= packet_counter_rfid+1;}//systemMessage("       x!");}
      if(debug==2){ 
                            Serial2.write('<');
                            Serial2.write(packet_counter_rfid);
                            Serial2.write(packet_data_size_rfid_l);
                            Serial2.write(packet_end_rfid);
                            Serial2.write('>');
                    }
    }
    
  }  


int request_rfid_tags(){ //TAGS PASIVOS
    
    
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 id            :   id equipo destino
    2 datasize      :   tamaño de data
    3 comando       :   comando al dispositivo
    4 data 1        :   data 1 del comando
    5 data 2        :   data 2 del comando
    6 chck          :   checksum;
    
=============================================================================================================================

         int chcksm=0;
         packet_o_rfid[0] = 0xA5; 
         packet_o_rfid[1] = 0x00;
         packet_o_rfid[2] = 0x04;
         packet_o_rfid[3] = 0x3C;
         packet_o_rfid[4] = 0x02;
         packet_o_rfid[5] = 0xC8;
         packet_o_rfid[6] = 0x51;
         
        for (int i=0; i<7;i=i+1){
         Serial2.write(packet_o_rfid[i]);
       }
    }

    
 */
