
/*
  Web Server
 
 A simple web server that shows the value of the analog input pins.
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 * Analog inputs attached to pins A0 through A5 (optional)
 
 created 18 Dec 2009
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe
 
 */

#include <SPI.h>
#include <Ethernet.h>
#include <ArduinoJson.h>
#include <EEPROM.h>

const int frameSize = 21;  //bytes = 10 bytes + state byte
const int maxIndex=48;  // max memory > maxIndex * frameSize Bytes
byte Eframe[frameSize-1];
byte EfState;

char* thisEmail;
char* thisName;
String stringName;
String stringEmail;

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xA0, 0xFE, 0xED };
IPAddress ip(192,168,60,81);

// Initialize the Ethernet server library
// with the IP address and port you want to use 
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }


  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());

  
  // Allocate the JSON document
  //
  // Inside the brackets, 200 is the capacity of the memory pool in bytes.
  // Don't forget to change this value to match your JSON document.
  // Use arduinojson.org/v6/assistant to compute the capacity.
  StaticJsonDocument<200> doc;

  // StaticJsonDocument<N> allocates memory on the stack, it can be
  // replaced by DynamicJsonDocument which allocates in the heap.
  //
  // DynamicJsonDocument doc(200);

  // JSON input string.
  //
  // Using a char[], as shown here, enables the "zero-copy" mode. This mode uses
  // the minimal amount of memory because the JsonDocument stores pointers to
  // the input buffer.
  // If you use another type of input, ArduinoJson must copy the strings from
  // the input to the JsonDocument, so you need to increase the capacity of the
  // JsonDocument.
  char json[] =
      "{\"name\":\"Felipe\",\"email\":\"emailserver.com\",\"data\":[48.756080,2.302038]}";

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, json);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }

  // Fetch values.
  //
  // Most of the time, you can rely on the implicit casts.
  // In other case, you can do doc["time"].as<long>();
  
  thisName  = doc["name"];
  thisEmail = doc["email"];

  readCharsFromE(0,thisName);
   Serial.println(thisName); 
  Serial.println();  
  Serial.println();  
  
  readCharsFromE(1,thisEmail);
  Serial.println(thisEmail);  
  Serial.println();  
  Serial.println();  
  
  double latitude = doc["data"][0];
  double longitude = doc["data"][1];
  stringName=String(thisName);
  stringEmail= String(thisEmail);
  // Print values.          
 
   
  Serial.println(stringName);
  Serial.println(stringEmail);
  Serial.println(latitude, 6);
  Serial.println(longitude, 6);
}


void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header 
          String thisString = "<input type=\"text\" id=\"name\" placeholder=\"Your name\" value=\""; 
          String thisString2 = "    <input type=\"text\" id=\"email\" placeholder=\"Your Email\" value=\"" ;
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println();
          client.print("<!DOCTYPE html>  <html>   <head>   <title>   JavaScript | Sending JSON data to server.   </title>    </head>    <body style=\"text-align:center;\" id=\"body\">      <h1 style=\"color:green;\">  HTML TEST                    </h1>                                      <p>                         <!-- Making a text input -->                       ");
          client.print(thisString);
          client.print(stringName); 
          client.print("\"> ");
          client.print(thisString2);
          client.print(stringEmail); 
          client.print("\"> ");
          client.println("<!-- Button to send data -->                        <button onclick=\"sendJSON()\">Send JSON</button>                                        <!-- For printing result from server -->                      <p class=\"result\" style=\"color:green\"></p>                                          </p>                                    <!-- Include the JavaScript file -->                <script src=\"index.js\"></script>                                     </body>                 </html>  ");

          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disonnected");
  }
}

void writeFrame(int index){
  if(index>maxIndex) return;
  EEPROM.write((index * frameSize),EfState);
  EfState = EEPROM.read(index * frameSize);
  Serial.print((index * frameSize), HEX);
  Serial.println(" index Write state "); 
  Serial.print(EfState, HEX);
  Serial.println(" <<<"); 
  for(int i=1;i<frameSize;i++){
    if (i < sizeof(Eframe)) EEPROM.write(((index * frameSize)+i),Eframe[i-1]);
    else EEPROM.write(((index * frameSize)+i),0);
  }
}

void readFrame(int index){
  if(index>maxIndex) return;
  EfState = EEPROM.read(index * frameSize);
  for(int i=1;i<frameSize;i++){
    Eframe[i-1] = EEPROM.read((index * frameSize)+i);
  } 
  Serial.print((index * frameSize), HEX);
  Serial.println(" index Read state "); 
  Serial.print(EfState, HEX);
  Serial.println(" <<<"); 
}


char* byteToChar(){
  Serial.println();
  Serial.println("byte* to char*");
  const char * cBuffer  = reinterpret_cast<const char*>(Eframe);  
  byte efsize = sizeof(Eframe);  
  Serial.println(cBuffer);
  Serial.print(efsize, HEX);
  Serial.println(" size"); 
  return cBuffer;
}

void chartoByte(char bytes[]){ 
  byte charsize = strlen(bytes);
  byte efsize = sizeof(Eframe);
  for (int cnt = 0;  cnt < efsize; cnt++)
  {
    // convert ascii to its byte representation 
    if (cnt < charsize) Eframe[cnt]= (byte)bytes[cnt];
    else Eframe[cnt]= (byte)0;
    Serial.print(Eframe[cnt], HEX);
  }
  Serial.println();
  Serial.println("char* to byte*"); 
  Serial.print(efsize, HEX);
  Serial.println(" ef size"); 
  Serial.print(charsize, HEX);
  Serial.println(" chr size"); 
  Serial.println(bytes); 
}

void readCharsFromE(int index, char* original){ 
    Serial.println("original"); 
    Serial.println(original); 
    readFrame(index); 
    if( EfState == 1 ) return byteToChar();
    else  {
        EfState = (byte)1;
        chartoByte(original);
        writeFrame(index);
        readFrame(index); 
        original = byteToChar(); 
    }    
}
