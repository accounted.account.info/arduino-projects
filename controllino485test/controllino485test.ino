#include <SPI.h>
#include <Controllino.h>  /* Usage of CONTROLLINO library allows you to use CONTROLLINO_xx aliases in your sketch. */

/*
  CONTROLLINO - Demonstration of RS485 interface usage, Version 01.00

  Compatible with CONTROLLINO MAXI and MEGA.

  IMPORTANT INFORMATION!
  Please, select proper target board in Tools->Board->Controllino MAXI/MEGA before Upload to your CONTROLLINO.
  
  (Refer to https://github.com/CONTROLLINO-PLC/CONTROLLINO_Library if you do not see the CONTROLLINOs in the Arduino IDE menu Tools->Board.)

  This example sketch requires external 12V or 24V power supply for your CONTROLLINO and RS485 communication partner device (e.g. another CONTROLLINO :-) ). 

  Created 12 Jan 2017
  by Lukas

  https://controllino.biz/
  
  (Check https://github.com/CONTROLLINO-PLC/CONTROLLINO_Library for the latest CONTROLLINO related software stuff.)
*/

/* This function will test RS485 functionality. Possible modes are 0-2. 0 is all enabled, sended message should come back, 1 means RE is low,  so message should not be recieved, 
2 means DE is disabled, so no message should be sent at all.*/
void TestRS485 (int mode)
{
  DDRJ = DDRJ | B01100000;
  PORTJ = PORTJ & B10011111;
  switch (mode)
  {
    case 0:
      PORTJ = PORTJ & B10011111;
      PORTJ = PORTJ | B01000000;
      delay (10);
      Serial.println ("Sending test message, expected to return;"); 
      Serial3.print("case 0 \n");
      break;
      
    case 1:
      PORTJ = PORTJ & B10011111;
      PORTJ = PORTJ | B01100000;
      delay (10);
      Serial.println ("Sending test message, not expected to return;");
      Serial3.print("case 1 \n");
      break;
      
    case 2:
      PORTJ = PORTJ & B10011111;
      PORTJ = PORTJ | B00100000;
      delay (10);
      Serial.println ("Sending test message, not expected to be sended;"); 
      Serial3.print("case 2 \n");      
      RecieveRS485ET();
      break;
      
    default:
      Serial.println("Wrong mode!");
      return; 
  }
}

void RS485SEND(){
      //RS485Mode(1);
      Serial.print("\nSalida de datos\n");
      Controllino_SwitchRS485RE(1);
      Controllino_SwitchRS485DE(1);
  }

void RS485RECEIVE(){
      //RS485Mode(2);
      Serial.print("\nEntrada de datos\n");
      Controllino_SwitchRS485RE(0);
      Controllino_SwitchRS485DE(0);
  }

void RS485ALL(){
      RS485Mode(0);
  }

void RS485Mode (int mode)
{
  DDRJ = DDRJ | B01100000;
  PORTJ = PORTJ & B10011111;
  switch (mode)
  {
    case 0:   // ALL mode
      PORTJ = PORTJ & B10011111;
      PORTJ = PORTJ | B01000000; 
      break;
      
    case 1:  // SEND
      PORTJ = PORTJ & B10011111;
      PORTJ = PORTJ | B01100000; 
      break;
      
    case 2:  // RECEIVE
      PORTJ = PORTJ & B10011111;
      PORTJ = PORTJ | B00100000; 
      break;
      
    default:
      Serial.println("Wrong mode!");
      return; 
  }
}

/* This function enters loop and waits for any incoming data on serial 3.*/
void RecieveRS485()
{
  
  while(Serial3.available())
  {  
      Serial.print((char)Serial3.read()); 
  }
}

void RecieveRS485ET()
{
  if(Serial3.available()){
      while(Serial3.available())
      {  
           Serial.print((char)Serial3.read()); 
      }
    }
  
}

void RecieveUART()
{
  if(Serial.available()){
    while(Serial.available())
    {   
        Serial3.print((char)Serial.read()); 
    }
  }
}

void RecieveUART2()
{
  
  if(Serial.available()){
    RS485SEND();
    delay(2);
    while(Serial.available())
    {   
        Serial3.print((char)Serial.read()); 
    }
    delay(2);
    RS485RECEIVE();
  }  
  
}


void setup() {
  /* Here we initialize USB serial at 9600 baudrate for reporting */
  Serial.begin(9600);
  /* Here we initialize RS485 serial at 9600 baudrate for communication */
  Serial3.begin(9600);
  /* This will initialize Controllino RS485 pins */
  Controllino_RS485Init(); 
  RS485RECEIVE();
}

void loop() {
  /* Use only one of the two functions, either send or recieve */
  RecieveUART2();  
  RecieveRS485(); 
}

/* 2017-01-12: The sketch was successfully tested with Arduino 1.6.13, Controllino Library 1.0.0 and CONTROLLINO MAXI and MEGA. */
