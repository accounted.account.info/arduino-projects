#include <SPI.h>
#include <Controllino.h>


/*=================================================================================
//////////////////  SOFTWARE FIREWATCHER V3 SIGMA TELECOM  ////////////////////////
===================================================================================

INGENIERO: FELIPE EDUARDO BRAVO SILVA
NOMBRE P.: SISTEMA DE ALERTA DE INCENDIOS (VERSION CONTROLLINO)
ESTADO   : EN DESARROLLO
FECHA ACT: 26-09-16
FECHA COM: 1-09-16
PROTOCOLO: CUSTOM FBS PROTOCOL (PROPIETARIO DE FELIPE BRAVO BAJO LICENCIA COPYLEFT)


=================================================================================*/
/*
OE -> 9 Pin    

A -> 6 Pin / Controllino: 10

B -> 7 Pin / Controllino: 12

CKL -> 13 Pin 

SKL -> 8 Pin 

R -> 11


  conector DMD en panel
               ______
 pin9 <-  nOE  | 1 2 | A    -> pin6
  x   <-  GND  | 3 4 | B    -> pin7
  x   <-  GND _| 5 6 | C    ->  x
  x   <-  GND |  7 8 | CKL  -> pin13
  x   <-  GND |  9-10| SCKL -> pin8
  x   <-  GND  | 1 2 | R    -> pin11
  x   <-  GND  | 3 4 | G    ->  x
  G   <-  GND  | 5 6 | D    ->  x
               -------
               
conector DMD en panel (version CONTROLLINO)
 Vs Controllino X2 Header
               ______
 pin9 <-  nOE  | 1 2 | A    -> pin10
  G   <-  GND  | 3 4 | B    -> pin12
  x   <-  GND _| 5 6 | C    ->  x
  x   <-  GND |  7 8 | CKL  -> pin13
  x   <-  GND |  9-10| SCKL -> pin8
  x   <-  GND  | 1 2 | R    -> pin11
  x   <-  GND  | 3 4 | G    ->  x
  x   <-  GND  | 5 6 | D    ->  x
               -------           

      
  Controllino X2 Header  Vs Panel

                     ______
  -     x   <-  5V*  | 1 2 | GND  ->  G     3
  -     x   <-  A6   | 3 4 | A7   ->  x     -
  -     x   <-  A8   | 5 6 | A9   ->  x     -
  -     x   <-  A10  | 7 8 | A11  ->  x     -
  -     x   <-  A12  | 9-10| A13  ->  x     -
  -     x   <-  RST  | 1 2 | 8    ->  SCKL  10
  1    nOE  <-  9    | 3 4 | 10   ->  A     2
  12    R   <-  11   | 5 6 | 12   ->  B     4
  8    CKL  <-  13   | 7 8 | 42   ->  x     -
  -     x   <-  43   | 9-20| 44   ->  x     -
  -     x   <-  45   | 1 2 | 16   ->  x     -
  -     x   <-  17   | 3 4 | 3v3  ->  x     -
  -     x   <-  GND  | 5 6 | AREF ->  x     -
                    -------           
               
*/


#include <SPI.h>
#include <DMD2.h>
#include <EEPROM.h>
#include <Sigma9_fw.h>
#include <avr/wdt.h>



#define DISPLAYS_WIDE 1
#define DISPLAYS_HIGH 1 

//CONTROLLINO DMD2 PINS

#define PIN_NOE 9
#define PIN_A   10
#define PIN_B   12
#define PIN_SCK 8
#define PIN_CLK 13
#define PIN_R   11



/*===========================================================
///////////////////////  SYS VARS   /////////////////////////
===========================================================*/

int debug=1;

/*===========================================================
/////////////////////// + ADDRESS   /////////////////////////
===========================================================*/

/*/-- address--/*/
const int address= 15                        ; //direccion de este equipo
int master_ad= 0xFF;
const int zone=address;

/*===========================================================
///////////////////////  SYS VARS   /////////////////////////
===========================================================*/

SoftDMD dmd(DISPLAYS_WIDE,DISPLAYS_HIGH,PIN_NOE,PIN_A,PIN_B,PIN_SCK,PIN_CLK,PIN_R);
//SoftDMD dmd(DISPLAYS_WIDE,DISPLAYS_HIGH);
DMD_TextBox box(dmd, 0, 0, 32, 16);

/*/-- System --/*/
int alarmflags[101];
int doCommand=0;
int bufferToWrite=0;
int app_id=0x01;
int currentBuffer=4;
int currentLine=0;
const int max_per_line=4;
  
/*/-- system time handle --/*/
  unsigned long start_t=0 ;  //SERIAL
  unsigned long work_last=0;
  unsigned long buzzer_start=0 ;  //SERIAL
  unsigned long buzzer_current=0;  
  unsigned long work_current=0;
  const unsigned long work_frame=600;
  const unsigned long answer_time=2000;
  const unsigned long line_time=1000;
  unsigned long line_time_start=0;
  unsigned long line_time_current=0;
  unsigned long protocol_start=0;
  unsigned long protocol_current=0;
  
/*/-- LED system buffers and flags --/*/
  char ledbuffer[5][50];  // 4 buffers de 50 caracteresz
  char ledSubBuffer[5][4][20];  // 4 buffers con 4 subbuffers de 14 caracteresz
  int ledflags[5][4];     // 4 flags: 0-set 1-largo 2-lineas calculadas 3-not used 
  const char *MESSAGE = "       SIGMA TELECOM";

/*/-- system PINOUT --/*/  
const byte alarm_pin = CONTROLLINO_R6;
const byte interruptPin = CONTROLLINO_IN0;
const byte button_pin = CONTROLLINO_IN0;

/*/-- Button Handling --/*/
volatile byte state = LOW;

const byte activate = LOW;
const byte deactivate = HIGH;

unsigned long last_interrupt_time = 0;
unsigned long interrupt_time = millis();
 
/*/-- System flags --/*/ 
int button_pressed=0;
int button_pressedx=0;
int alarm_flag=0;

/*========================================================================
    
    flags:
        0:set => 0 sin mensajes, 1 con mensaje, 2+ con mensaje emergencia 
               si setea mensaje emergencia, toma prioridad sobre otros 
               mensajes de emergencia(set+1 del mensaje de emergencia 
               con mayor prioridad), si existe almenos 1 mensaje de 
               emergencia, los mensajes normales son ignorados.
               Si solo existe un mensaje de emergencia, tomara el valor 2.
        1:largo => indica el largo del mensaje
        
========================================================================*/

/*========================================================================

chars especiales:
$ : escape
& : vehiculo
@ : exclamacion en triangulo
^ : peligro electrico
{ : fuego
} : derrumbe
~ : eñe
0x7F : FULL

========================================================================*/

/*===========================================================
/////////////////////////  SETUPS   /////////////////////////
===========================================================*/


void setup() {
  alarm(0);
  dmd.setBrightness(255);
  dmd.selectFont(Sigma9);
  dmd.begin();
  Serial.begin(9600);
  Serial3.begin(9600); 
  attachInterrupt(digitalPinToInterrupt(interruptPin), irq_button, RISING); 
  testroutine();
  delay(500);
  clearDataBuffer();
  startDelta();
  pinMode(alarm_pin,OUTPUT);
  digitalWrite(alarm_pin,deactivate);
  delay(1);
  if(EEPROM.read(0)==(char)0x0A) button_pressed=1;
  int lastzone = (int)EEPROM.read(1);
//  if(lastzone>0 && lastzone<100) zone = lastzone;
  Controllino_RS485Init(); 
  RS485RECEIVE(false);
  delay(2000);
}  



/*===========================================================
////////  CONTROLLINO RS485 CONTROL FUNCTIONS    ////////////
===========================================================*/

void RS485SEND(){ 
      if(debug==1) Serial.print("\nSalida de datos\n");
      Controllino_SwitchRS485RE(1);
      Controllino_SwitchRS485DE(1);
  }

void RS485RECEIVE(boolean debugx){ 
      if(debug==1 && debugx) Serial.print("\nEntrada de datos\n");
      Controllino_SwitchRS485RE(0);
      Controllino_SwitchRS485DE(0);
  }

/*===========================================================
//////////////////    packet analyzer    ////////////////////
===========================================================*/
  const byte NoAlarm= 0x00; 
  const byte Alarm= 0x01; 
  const byte setThisZone= 0x02; 
  const byte zoneNoAlarm= 0xB0; 
  const byte zoneAlarm= 0xC1; 
  const byte serverPing= 0x03; 
  
/*/-- packet data --/*/
  int command=0, data[200];
  int code=0;
  int code2=0;
  int r_zone;
  int r_master_ad=0;
  int r_ad=0;
  int r_app_id=0;
  int r_chck=0;
/*/-- packet bytes --/*/
  int o_address = 0x00;
  int packet[255];
  int packet_o[255];
  int packet_counter=0;
  int packet_ini=0;
  int packet_end=0;
  int packet_size=0;
  int dataBuffer[255];
  int sys_dataBuffer[255];
  int dataBufferLength=0;
/*/-- packet logic --/*/
  boolean zoneExecution=false;
  boolean execution=false;
  boolean setZone=false;
  boolean isPing=false;
  boolean isAnAlarm=false;
  boolean isaTask=false;
  int zoneToExecute=0;
  

boolean packetAnalizer(){
  packet_check();
  isAnAlarm=false;
  execution=false;
  setZone=false;
  zoneExecution=false;
  isPing=false;
  isaTask=false;
  zoneToExecute=0;
  if(packet_end!=1) return false;
  if(checksum()!=0) return false;
  char messg0[]="                                  ";
  sprintf(messg0,"command:function state %d ",command);
  if(debug==1) Serial.print(messg0);
  
  switch(command){

      case NoAlarm    :   if(r_master_ad==address){
                              isAnAlarm=false;
                              execution=true;
                              setZone=false;
                              zoneExecution=false;
                              isPing=false;
                          } else return false;                          
                          break;
      case Alarm      :   if(r_master_ad==address){
                            isAnAlarm=true;
                            execution=true;
                            setZone=false;
                            zoneExecution=false;
                            isPing=false;
                          } else return false; 
                          break;
      case setThisZone:   if(r_master_ad==address){
                            isAnAlarm=false;
                            execution=false;
                            setZone=true;
                            zoneExecution=false;
                            isPing=false;
                            zoneToExecute=code2;
                          } else return false; 
                          break;
      case zoneNoAlarm:   isAnAlarm=false;
                          execution=false;
                          setZone=false;
                          zoneExecution=true;
                          isPing=false;
                          zoneToExecute=code;
                          break;
      case zoneAlarm  :   isAnAlarm=true;
                          execution=false;
                          setZone=false;
                          zoneExecution=true;
                          isPing=false;
                          zoneToExecute=code;
                          break;
      case serverPing :   isAnAlarm=false;
                          execution=false;
                          setZone=false;
                          zoneExecution=false;
                          isPing=true;
                          zoneToExecute=code;
                          break;
      default         :   isAnAlarm=false;
                          execution=false;
                          setZone=false;
                          zoneExecution=false;
                          isPing=false;
                          isaTask=false;
                          return false;    
  }
  
  if(packet_size>5 && (execution || setZone)){
    int startpoint=packet_size-4;
    for(int i=0;i<4;i++){
            data[i]=packet[6+i+startpoint];
    }
    isaTask=true;
  }
  return true;
}

/*===========================================================
///////////////////////    LOOP    //////////////////////////
===========================================================*/

void loop() {
  wdt_enable(WDTO_1S);
  if(packetAnalizer()){
      if(debug==1) Serial.print("\ncommand mode\n");
      commandMode();
      clear_serial();
  }      
  writeZones();// writeMessages();
  check_state();
  checkProtocol();
}

/*===========================================================
/////////////////////////  FUNCIONES  ///////////////////////
===========================================================*/
/////////////=============  DEMO  ===============////////////

int testroutine(){
  //dmd.drawFilledBox(0,0,32,16);
  //delay(3000);
  const char *next = MESSAGE; 
  while(*next) { 
    box.print(*next);
    delay(200);
    next++;
  }
  return 0;
}

/////////////=============   IRQ  ===============////////////

void irq_button(){
     interrupt_time = millis();
     // If interrupts come faster than 200ms, assume it's a bounce and ignore
     unsigned long delta_time =  interrupt_time - last_interrupt_time;
     delta_time = abs(delta_time);
     if (delta_time > 200) 
     {
        button_pressedx=1;
        detachInterrupt(digitalPinToInterrupt(interruptPin));
     }
     last_interrupt_time = interrupt_time;      
  }

/////////////============= SYSTEM ===============////////////
int commandMode(){
  if(execution){
    if(!isAnAlarm){
        alarm(0);
        code= 0x00;
        if(debug==1) Serial.print("\nAlarm desactivated from server\n");
    }else if(isAnAlarm){
        alarm(1);
        code= 0x01;
        if(debug==1) Serial.print("\nAlarm activated from server\n");
    }
  } else if (zoneExecution){
      doZoneFunction();  
  } else if(setZone){
      setCurrentZone(1);
      code= alarm_flag;
      if(debug==1) Serial.print("\nZone has been changed \n");
  } else if(isPing){
      code= alarm_flag;
      if(zone!=zoneToExecute) setCurrentZone(0);
      if(debug==1) Serial.print("\nServer ping\n");
  } 

  if(execution || setZone || isPing){
      if(debug==1) Serial.print("\sending answer ...\n");
      if(isaTask) send_packet(code,code, 0 , 4); 
      else send_packet(code,code, 0 , 0);  
  }
}

void setCurrentZone(int fromcode){
  if (zoneToExecute>0 && zoneToExecute<101){
        char messg0[]="                                  ";
        sprintf(messg0,"setzone %d %d ",zoneToExecute,fromcode);
        if(debug==1) Serial.println(messg0);
        if (zoneToExecute!=0 && zone!=zoneToExecute){
//          zone=zoneToExecute; 
          EEPROM.write(1,zone); 
        } 
        updatezone();
    }
}


void updatezone(){
    
    switch(alarm_flag){ //alarm_flag
      case  0  :    sys_messages(1);
                    break;
                  
      case  1  :    sys_messages(0);
                    break;
                    
      default  :    error(0x03);
                    break;
      
      }
  }
  
int doZoneFunction(){
  if (zoneToExecute>0 && zoneToExecute<101) alarmZ(); 
}


int sys_messages(int messagenum){
       char messg0[]="{   }       ";
       char messg1[]="ZONA    `";
       //messg0[12]=127;
      sprintf(messg0,"{   }    %02d ",zone);
      sprintf(messg1,"ZONA %02d `",zone);
       
       switch(messagenum){
      
        case  0  :    set_message(messg0,12);
                      break;
        case  1  :    set_message(messg1,9);
                      break;
        default  :    error(0x03);
                      break;
      }
  }

int sys_messagesZone(int messagenum, int zonex){
       char messg0[]="{   }       ";
       char messg1[]="ZONA    `";
       messg0[12]=127;
       sprintf(messg0,"{   ZONA %02d ",zonex);
       sprintf(messg1,"ZONA %02d `",zonex);
      
       switch(messagenum){
      
        case  0  :    set_messageZone(messg0,12,zonex);
                      break;
        case  1  :    zoneTurnOff(zonex);
                      break;
        default  :    error(0x03);
                      break;
      }
  }
  
int set_message(char messg[], int datlen){
    //sys_savebuffer(int bufferlen,int bufferToWrite_);
    sys_setData( messg, datlen);
    sys_savebuffer(datlen,4);
  }

int set_messageZone(char messg[], int datlen, int zonex){
    //sys_savebuffer(int bufferlen,int bufferToWrite_);
    int freebuffer = getAvailableBuffer(zonex);
    ledflags[freebuffer][3]=zonex;
    sys_setData( messg, datlen);
    sys_savebuffer(datlen,freebuffer);
  }

int sys_setData(char messg[],int datlen){   
    dataBufferLength=datlen;  
    for (int i=0; i<dataBufferLength; i=i+1){
               sys_dataBuffer[i]=messg[i];
     } 
}

void alarmsignal(int set){
    switch(set){
      
        case  0  :    digitalWrite(alarm_pin, deactivate);
                      break;
        case  1  :    digitalWrite(alarm_pin, activate); 
                      break;
        default  :    error(0x04);
                      break;
      }  
  }
  
int alarm(int toggle_alarm){

    char messg0[]="                                  ";
    sprintf(messg0,"do zone function state %d zone %d ",toggle_alarm,zone);
    if(debug==1) Serial.println(messg0);
    
    if(toggle_alarm==1){
                 alarm_flag=1;
                 alarmflags[zone]=1;
                 button_pressed=0;
                 button_pressedx=0;
                 alarmsignal(1);
                 sys_messages(0);
                 EEPROM.write(0, (char)0x0A);
      }
    else if (toggle_alarm==0){
                 alarm_flag=0;
                 button_pressed=0;
                 button_pressedx=0;
                 alarmflags[zone]=0;
                 alarmsignal(0);
                 sys_messages(1);
                 EEPROM.write(0, (char)0x00);      
      }
    return 0;
  }


int alarmZ(){
    char messg0[]="                                              ";
    int thisIsAnAlarm = 0;
    if(isAnAlarm) thisIsAnAlarm = 1;
    sprintf(messg0,"do zone function state %d zone %d ",thisIsAnAlarm,zoneToExecute);
    if(debug==1) Serial.println(messg0);
    if(isAnAlarm){
                 alarmflags[zoneToExecute]=1;
      }
    else if (!isAnAlarm){
                 alarmflags[zoneToExecute]=0;  
      }
    return 0;
  }

int sendActivatePacket(){
    send_packet(0xA7,0xA7, 0 , 0);
                 for (int i=0; i<3;i++){
                     if(!wait_answer()){
                          send_packet(0xA7,0xA7, 0 , 0);
                      }                  
                  }
    return 0;
}

int check_state(){
      if(button_pressed==1){// && alarm_flag==0) {
                 alarm(1);
                 char messg0[]="                                       ";
                  sprintf(messg0,"rst:function state %d zone %d ",1,zone);
                  if(debug==1) Serial.println(messg0); 
                  sendActivatePacket();
                  button_pressed==0;
                  button_pressedx==0;
                 
        }
      else if(button_pressedx==1){// && alarm_flag==0) {
                 alarm(1);
                 char messg0[]="                                        ";
                  sprintf(messg0,"int:function state %d zone %d ",1,zone);
                  if(debug==1) Serial.print("Activado"); 
                  sendActivatePacket();
                  button_pressed==0;
                  button_pressedx==0;
                 
        }  
      else if (digitalRead(button_pin)==HIGH) {
                alarm(1);
                char messg0[]="                                      ";
                  sprintf(messg0,"btn:function state %d zone %d ",1,zone);
                  if(debug==1) Serial.println(messg0); 
                  sendActivatePacket();
                  button_pressed==0;
                  button_pressed==0;
                  button_pressedx==0;
        }

        attachInterrupt(digitalPinToInterrupt(interruptPin), irq_button, RISING);
        if(alarm_flag) alarm(1);
        return 0;
  }


int sys_savebuffer(int bufferlen,int bufferToWrite_){
  
  if(bufferlen<50){
      for(int i=0;i<50 ;i++){
        ledbuffer[bufferToWrite_][i]= sys_dataBuffer[i];       
      }
      ledflags[bufferToWrite_][0]=5;
      //if(bufferToWrite_==4)  ledflags[bufferToWrite_][0]=1;
      ledflags[bufferToWrite_][1]=bufferlen;
      ledflags[bufferToWrite_][2]=calcLines(bufferlen);
      for(int i=0;i<ledflags[bufferToWrite_][2] ;i++){
        for(int j=0;j<max_per_line+5 ;j++){
          if(j<max_per_line)ledSubBuffer[bufferToWrite_][i][j]= ledbuffer[bufferToWrite_][(i*max_per_line)+j]; 
          else  ledSubBuffer[bufferToWrite_][i][j]= 0x20;      
        }      
      }
  }
 /* Serial.write("< write buffer: ");
  Serial.write(bufferToWrite_+0x30);
  Serial.write(" : ");
  Serial.write(ledflags[bufferToWrite_][0]+0x30);
  Serial.write(" >");*/
  clearDataBuffer();
  return 0;
}

int savebuffer(int bufferlen,int bufferToWrite_){
  
  if(bufferlen<50){
      for(int i=0;i<50 ;i++){
        ledbuffer[bufferToWrite_][i]= dataBuffer[2+i];       
      }
      ledflags[bufferToWrite_][0]=dataBuffer[0];
      if(bufferToWrite_==4)  ledflags[bufferToWrite_][0]=1;
      ledflags[bufferToWrite_][1]=bufferlen;
      ledflags[bufferToWrite_][2]=calcLines(bufferlen);
      for(int i=0;i<ledflags[bufferToWrite_][2] ;i++){
        for(int j=0;j<20 ;j++){
          if(j<max_per_line)ledSubBuffer[bufferToWrite_][i][j]= ledbuffer[bufferToWrite_][(i*max_per_line)+j]; 
          else  ledSubBuffer[bufferToWrite_][i][j]= 0x20;      
        }      
      }
  }
  //Serial3.write(0xFA);
  //Serial3.write(bufferToWrite_);
  //Serial3.write(ledflags[bufferToWrite_][0]);
  //Serial3.write(0xFB);
  clearDataBuffer();
  return 0;
}

int deleteBuffer(int buffernum){
  for(int i=0;i<50;i++){
    ledbuffer[buffernum][i]= 0;
  }
      ledflags[buffernum][0]=0;
      ledflags[buffernum][1]=0;
      ledflags[buffernum][2]=0;
      return 0;
}

int deleteAllBuffers(){

  for(int j=0;j<5;j++){
     for(int i=0;i<50;i++){
      ledbuffer[j][i]= 0;
    }
        ledflags[j][0]=0;
        ledflags[j][1]=0;
        ledflags[j][2]=0;
  }
  return 0;
  
}

int clearDataBuffer(){

  for(int j=0;j<255;j++){
      dataBuffer[j]= 0x20;    
  }
  return 0;
}

int error(int code){
  /*
    Error codes:
    0x00: buffer incorrecto
    0x01: largo incorrecto de mensaje
    0x02: codigos de error 0x00 y 0x01 juntos
    0x03: comando incorrecto/ error de sistema 
  */
  //send_packet(0x0E, code, 0xFF, 0x00);
  if(debug==1) Serial.print("::: err :::");
  return 0;
}
/////////////============= PANEL LED ============////////////

int topPriority(){
  int topprior=0;
  for(int i=0;i<5;i++){
      if (topprior<ledflags[i][0]) topprior=ledflags[i][0];
  }
  return topprior;
}

int zoneTurnOff(int zonex){
  for(int i=0;i<5;i++){
      if (ledflags[i][3]==zonex) ledflags[i][0]=0;
  }
  return 0;
 }

 int getAvailableBuffer(int zonex){
    for(int i=0;i<4;i++){
      if (ledflags[i][3]==zonex) return i;
    }
    for(int i=0;i<4;i++){
      if (ledflags[i][0]==0) return i;
    }
    return 0;
  }

int calcLines(int sizeOfBuffer){
  for(int i=1;i<=4 && sizeOfBuffer>0 ;i++){
    if (sizeOfBuffer<=(max_per_line*i)) return i;  
  }
  return 0;
}

long getDelta(){
    line_time_current=millis();
    return line_time_current-line_time_start;
  }

void checkProtocol(){
    RS485RECEIVE(false);
    if(getDeltaProtocol()>line_time){
          clear_serial();
          startDeltaProtocol();
      }
  }  

long getDeltaProtocol(){
    protocol_current=millis();
    return line_time_current-protocol_start;
  }

int startDeltaProtocol(){
    protocol_current=millis();
    protocol_start=millis();
    return 0;
  }

int startDelta(){
    line_time_current=millis();
    line_time_start=millis();
    return 0;
  }
  
int writeMessages(){
  //Serial.write(0xAA);
  int topprior = topPriority();
  long time_delta=getDelta();
  if(ledflags[currentBuffer][0]==topprior && time_delta>line_time){

      dmd.drawString(0, 0, ledSubBuffer[currentBuffer][currentLine]);
      startDelta();  
      currentLine++; 
      if (currentLine >= ledflags[currentBuffer][2] | currentLine >3) {
          currentLine=0;
          currentBuffer++;
          if(currentBuffer>4) currentBuffer=0;
        }  
      
   } else if (ledflags[currentBuffer][0]!=topprior) {

      currentBuffer++;
      currentLine=0;
      if(currentBuffer>4) currentBuffer=0;
   }
   return 0;
}

int checkAlarms(){
    int localflag=0;
    for(int i=1; i<101;i++){
        if(alarmflags[i]>0) localflag=1;
      }  
    alarmflags[0]=localflag;
    alarmsignal(localflag);
    return localflag;
}

int writeThisZone(){
  //Serial.write(0xCC);
  long time_delta=getDelta();
  if(time_delta>line_time){
      dmd.drawString(0, 0, ledSubBuffer[4][currentLine]);
      startDelta();  
      currentLine++; 
      if (currentLine >= ledflags[4][2] | currentLine >3 ) {
          currentLine=0;
          currentBuffer++;
        }     
   } 
   return 0;
}
int writeZones(){  //alarmflags[100]
  long time_delta=getDelta();
  checkAlarms();    
  if (alarmflags[0]==0) { 
      //Serial.write(0xAB);
      currentBuffer=0;
      writeThisZone();
  }
  else if (currentBuffer==100) writeThisZone();
  //else if (currentBuffer==zone) currentBuffer++;
  
   
  else if(currentBuffer>0 && alarmflags[currentBuffer]>0 && time_delta>line_time && currentBuffer<100){
      //Serial.write(0xDD);
      char messg0[]="    ";
      sprintf(messg0," %02d `",currentBuffer);
      dmd.drawString(0, 0, messg0);
      startDelta();  
      currentBuffer++;
            
   } else if (time_delta>line_time){

      currentBuffer++;
      if(currentBuffer>100) {
         char messg1[]="    ";
         if (alarmflags[0]>0 && currentLine==0) sprintf(messg1,"{   ");
         if (alarmflags[0]>0 && currentLine==1) sprintf(messg1,"ZONA");
         currentLine++;
         if(currentLine>1) {currentBuffer=0; currentLine=0;}
         dmd.drawString(0, 0, messg1);
         startDelta();  
      }
   }
   return 0;
}

/////////////============= PANEL LED ============////////////

int systemMessage(char towrite[]){
  //dmd.drawString(0, 0, F(towrite));
  char *next = towrite;
  box.print(*next);
  return 0;
}

/////////////=============  COMM  ==============/////////////



int wait_answer(){
    unsigned long current_t=0, delta_t=0;
    start_t=millis();

    while(delta_t<answer_time){
      current_t = millis();
      delta_t = current_t-start_t;
      packet_check();
      writeMessages();
      wdt_enable(WDTO_1S);
      if(packet_end==1)  {  return 1; }
    }
    return 0;
  }

void getMessage(){
    if(packet_size>=2)getData(packet_size);
}
  
void getData(int ldata){   
    dataBufferLength=ldata;  
    for (int i=0; i<ldata; i=i+1){
               dataBuffer[i]=packet[6+i];
     } 
}

void clear_serial(){
  
  for(int i=0; i<10; i=i+1){
      packet[i]=0;
    }
    packet_end=0;
    packet_ini=0; 
    packet_counter=0;
    packet_size=0;
}  
  
//////////=============  PROTOCOLO  ==============//////////
  

int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera      :   0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de unidad maestra
    3 address       :   direccion de este dispositivo
    4 zone          :   zona de ubicacion de este dispositivo
    5 largodatos    :   largo de datos ( 1 comando + 1 codigo + largo)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
_____________________________________________________________________________________________________________________________

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================*/
  
    while (Serial3.available() && packet_end!=1) {
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      startDeltaProtocol();
      packet[packet_counter] = Serial3.read();
      if(debug>0) Serial.write(packet[packet_counter]);
      if(packet_counter==6 && packet_ini>0 && packet_size>0 )  {command=packet[packet_counter];}//systemMessage("    x");}
      else if(packet_counter==6 && packet_size==0){clear_serial();return 0;}
      else if(packet_counter==7 && packet_ini>0 && packet_size>1)  {code=packet[packet_counter];}//systemMessage("    x");}
      else if(packet_counter==8 && packet_ini>0 && packet_size>2)  {code2=packet[packet_counter];}//systemMessage("    x");}
      
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {r_app_id=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {r_master_ad=packet[packet_counter]; packet_counter= packet_counter+1;
      } //if (r_master_ad!=address && r_master_ad!=0) {Serial.write(0xBB);Serial.write(r_ad);packet_counter=0; packet_size=0; packet_ini=0;}}//systemMessage(" x");}
      else if(packet_counter==3 && packet_ini>0)  {r_ad=packet[packet_counter]; packet_counter= packet_counter+1;  }//systemMessage(" x");}
      else if(packet_counter==4 && packet_ini>0)  {r_zone=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==5 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}     
      else if(packet_counter<6+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==6+packet_size && packet_ini>0){r_chck=packet[packet_counter];packet_counter= packet_counter+1;}//systemMessage("       x!");}
      else if(packet_counter==7+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;if(debug==1) Serial.print("!\n");}//systemMessage("       x!");}
      else if(packet_counter>8+packet_size && packet_end!=1){ clear_serial();}
    } 
    while  (Serial3.available() && packet_end==1){
        byte dummy=Serial3.read();
    }
    return 0;
  }  
  

  int checksum(){
       byte checksm = packet[1];
       for (int i=2; i<packet_size+6; i=i+1){
           checksm = packet[i] ^ checksm;
        }  
       if(checksm==r_chck) {
          if(debug==1) Serial.print("\nchecksum ok\n");
          return 0;
        } 
       else {
          char messg0[]="                                  ";
          sprintf(messg0,"chksm %d %d ",checksm,r_chck);
          if(debug==1) Serial.print(messg0);
          if(debug==1) Serial.print("\nchecksum fail\n");
          return 1;
        } 
    }
       

  void send_packet(int commando, int codigo, int s_address, int ldata){
    
    
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de este dispositivo
    3 address       :   direccion de unidad maestra
    4 zone          :   zona de ubicacion de este dispositivo
    5 largo         :   largo de data ( 1 comando + 1 codigo + datos)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
=============================================================================================================================*/

         int chcksm=0;
         packet_o[0] = 0xFE; 
         packet_o[1] = app_id;
         chcksm = chcksm ^ packet_o[1];
         packet_o[2] = master_ad;
         chcksm = chcksm ^ packet_o[2];
         packet_o[3] = address;
         chcksm = chcksm ^ packet_o[3];
         packet_o[4] = zone;
         chcksm = chcksm ^ packet_o[4];
         packet_o[5] = ldata+2;
         chcksm = chcksm ^ packet_o[5];
         packet_o[6] = commando;
         chcksm = chcksm ^ packet_o[6];
         packet_o[7] = codigo;
         chcksm = chcksm ^ packet_o[7];
         for (int i=0; i<ldata; i=i+1){
               packet_o[8+i] = data[i];
               chcksm = chcksm ^ packet_o[8+i];
        } 
        packet_o[8+ldata] =  chcksm; 
        packet_o[9+ldata] =  0xFD;
        RS485SEND();
        delay(5);
        int lengthpacket=10+ldata;
        //if(debug==1) Serial.println("\nSending length"+lengthpacket+" with data "+ldata);
        for (int i=0; i<10+ldata;i=i+1){
          Serial3.write(packet_o[i]);
       }
       delay(1);
       RS485RECEIVE(true);
    }



/*===========================================================
/////////////////////////  RECICLAJE  ///////////////////////
===========================================================*/
 /*
 
   int send_packet(int commando, int codigo, int s_address, int ldata){
       int chcksm=0;
       packet_o[0] = 0xFE; 
       packet_o[1] = app_id; //0x02 = app de mensaj
       packet_o[1] = ldata+1; 
       chcksm = chcksm ^ packet_o[1];
       packet_o[2] = address; 
       chcksm = chcksm ^ packet_o[2];
       packet_o[3] = master_ad; 
       chcksm = chcksm ^ packet_o[3];
       packet_o[4] = commando; 
       chcksm = chcksm ^ packet_o[4];
       packet_o[5] = codigo; 
       chcksm = chcksm ^ packet_o[5];
       for (int i=0; i<ldata; i=i+1){
               packet_o[6+i] = data[i];
               chcksm = chcksm ^ packet_o[5+i];
        } 
       packet_o[6+ldata] =  chcksm; 
       packet_o[7+ldata] =  0xFD;
       for (int i=0; i<8+ldata;i=i+1){
          Serial.write(packet_o[i]);
       }
    }



int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 Tamaño:   Bytes de data 0xDD;
    2 Direccion:0xXX; (0x10)
    3 Comando:  0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    4 Data :    D1: codigo (buffer a escribir/borrar 0->4), D1: prioridad (ver explicacion de prioridades y buffers)
    5 Chksum:   Cabecera (Xor) Direccion (Xor) Commando (Xor) Data ;
    
=============================================================================================================================*/
/*============================================================================================================================

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================/
  
    if (Serial.available()) {
      
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      packet[packet_counter] = Serial.read();
      //Serial.write(packet_counter);
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {packet_counter= packet_counter+1;}//systemMessage("  x");}
      else if(packet_counter==3 && packet_ini>0 && packet[packet_counter]!=address && packet[packet_counter]!=0) {packet_counter= 0; packet_ini=0;}
      else if(packet_counter==3 && packet_ini>0 && (packet[packet_counter]==address || packet[packet_counter]==0)) {packet_counter= packet_counter+1;}//systemMessage("   x");}
      else if(packet_counter==4 && packet_ini>0){ command=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage("    x");}
      else if(packet_counter<5+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==5+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;}//systemMessage("       x!");}
    }
    
  }

 */
