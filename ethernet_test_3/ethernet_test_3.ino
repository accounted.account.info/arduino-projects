#include <SPI.h>
#include <Controllino.h>


/*=================================================================================
//////////////////  SOFTWARE FIREWATCHER V3 SIGMA TELECOM  ////////////////////////
===================================================================================

INGENIERO: FELIPE EDUARDO BRAVO SILVA
NOMBRE P.: SISTEMA DE ALERTA DE INCENDIOS (VERSION CONTROLLINO)
ESTADO   : EN DESARROLLO
FECHA ACT: 26-09-16
FECHA COM: 1-09-16
PROTOCOLO: CUSTOM FBS PROTOCOL (PROPIETARIO DE FELIPE BRAVO BAJO LICENCIA COPYLEFT)


=================================================================================*/
/*
OE -> 9 Pin    

A -> 6 Pin / Controllino: 10

B -> 7 Pin / Controllino: 12

CKL -> 13 Pin 

SKL -> 8 Pin 

R -> 11


  conector DMD en panel
               ______
 pin9 <-  nOE  | 1 2 | A    -> pin6
  x   <-  GND  | 3 4 | B    -> pin7
  x   <-  GND _| 5 6 | C    ->  x
  x   <-  GND |  7 8 | CKL  -> pin13
  x   <-  GND |  9-10| SCKL -> pin8
  x   <-  GND  | 1 2 | R    -> pin11
  x   <-  GND  | 3 4 | G    ->  x
  G   <-  GND  | 5 6 | D    ->  x
               -------
               
conector DMD en panel (version CONTROLLINO)
 Vs Controllino X2 Header
               ______
 pin9 <-  nOE  | 1 2 | A    -> pin10
  G   <-  GND  | 3 4 | B    -> pin12
  x   <-  GND _| 5 6 | C    ->  x
  x   <-  GND |  7 8 | CKL  -> pin13
  x   <-  GND |  9-10| SCKL -> pin8
  x   <-  GND  | 1 2 | R    -> pin11
  x   <-  GND  | 3 4 | G    ->  x
  x   <-  GND  | 5 6 | D    ->  x
               -------           

      
  Controllino X2 Header  Vs Panel

                     ______
  -     x   <-  5V*  | 1 2 | GND  ->  G     3
  -     x   <-  A6   | 3 4 | A7   ->  x     -
  -     x   <-  A8   | 5 6 | A9   ->  x     -
  -     x   <-  A10  | 7 8 | A11  ->  x     -
  -     x   <-  A12  | 9-10| A13  ->  x     -
  -     x   <-  RST  | 1 2 | 8    ->  SCKL  10
  1    nOE  <-  9    | 3 4 | 10   ->  A     2
  12    R   <-  11   | 5 6 | 12   ->  B     4
  8    CKL  <-  13   | 7 8 | 42   ->  x     -
  -     x   <-  43   | 9-20| 44   ->  x     -
  -     x   <-  45   | 1 2 | 16   ->  x     -
  -     x   <-  17   | 3 4 | 3v3  ->  x     -
  -     x   <-  GND  | 5 6 | AREF ->  x     -
                    -------           
               
*/

 
#include <DMD2.h>
#include <EEPROM.h>
#include <Sigma9_fw.h>
#include <avr/wdt.h>
#include <Ethernet.h>
#include <avr/pgmspace.h> 
#include <HTTPserver.h>

#define DISPLAYS_WIDE 1
#define DISPLAYS_HIGH 1 

//CONTROLLINO DMD2 PINS

#define PIN_NOE 9
#define PIN_A   10
#define PIN_B   12
#define PIN_SCK 8
#define PIN_CLK 13
#define PIN_R   11


/*===========================================================
/////////////////////// + ADDRESS   /////////////////////////
===========================================================*/

/*/-- address--/*/
int address= 15                        ; //direccion de este equipo
int master_ad= 0xFF;
int zone=address;

/*===========================================================
///////////////////////  SYS VARS   /////////////////////////
===========================================================*/

int debug=0;

/*===========================================================
/////////////////////////  EEPROM VARS  /////////////////////
===========================================================*/

const int frameSize = 21;  //bytes = 10 bytes + state byte
const int maxIndex=48;  // max memory > maxIndex * frameSize Bytes
byte Eframe[20];
byte EfState;




/*===========================================================
/////////////////////////  EEPROM FRAMES  ///////////////////
===========================================================*/
/**  
 *   
 *   FRAME   [state][DATA]
 *   
 *   state (1 byte) : state of this frame where 1 is "written" and anything else is "empty"
 *   DATA: bytes of data;
 *   
 *   
 *   PAGINATION
 *   
 *   FRAME INDEX = 0 > System States
 *   [state][alarm][zone][device id]
 *      0      1     2        3
 *   
 *   FRAME INDEX = 1 > ip <bytes>
 *   FRAME INDEX = 2 > mac <bytes>
 *   FRAME INDEX = 3 > ips <bytes>
 *   FRAME INDEX = 4 > ipsp <chars>
 *   FRAME INDEX = 5 > dns <bytes>
 *   FRAME INDEX = 6 > gw <bytes>
 *   FRAME INDEX = 7 > mask <bytes>
 *   FRAME INDEX = 8 > appsp <chars>
 *   FRAME INDEX > 8 UNUSED
 *   
*/

const int ialarm=0;
const int izone=1;
const int iaddress=2;

const int iip=1;
const int imac=2;
const int iips=3;
const int iipsp=4;
const int idns=5;
const int igw=6;
const int imask=7;
const int iappsp=8;

void writeFrame(int index){
  if(index>maxIndex) return;
  EEPROM.write((index * frameSize),EfState);
  EfState = EEPROM.read(index * frameSize);
  if(debug>1){
    Serial.print((index * frameSize), HEX);
    Serial.println(F(" index Write state ")); 
    Serial.print(EfState, HEX);
    Serial.println(F(" <<<"));     
    for(int i=1;i<frameSize;i++){
      Serial.print(Eframe[i-1],DEC);
      Serial.print(" - ");
    } 
  }
  for(int i=1;i<frameSize;i++){
    EEPROM.write(((index * frameSize)+i),Eframe[i-1]);
  }
}

void readFrame(int index){
  if(index>maxIndex) return;
  EfState = EEPROM.read(index * frameSize);
  for(int i=1;i<frameSize;i++){
    Eframe[i-1] = EEPROM.read((index * frameSize)+i);
  } 
  if(debug>1){ 
    Serial.print((index * frameSize), HEX);
    Serial.println(F(" index Read state ")); 
    Serial.print(EfState, HEX);
    Serial.println(F(" <<<"));  
    for(int i=1;i<frameSize;i++){
      Serial.print(Eframe[i-1],DEC);
      Serial.print(" - ");
    } 
    Serial.println(); 
  }
}


char* byteToChar(){
  
  const char * cBuffer  = reinterpret_cast<const char*>(Eframe);  
  byte efsize = sizeof(Eframe);  
  if(debug>1){
    Serial.println();
    Serial.println("byte* to char*");
    Serial.println(cBuffer);
    Serial.print(efsize, HEX);
    Serial.println(" size");  
  }
  return cBuffer;
}

void chartoByte(char bytes[]){ 
  byte charsize = strlen(bytes);
  byte efsize = sizeof(Eframe);
  for (int cnt = 0;  cnt < efsize; cnt++)
  {
    // convert ascii to its byte representation 
    if (cnt < charsize) Eframe[cnt]= (byte)bytes[cnt];
    else Eframe[cnt]= (byte)0;
    if( debug>1 )Serial.print(Eframe[cnt], HEX);
  }
  if(debug>1){
    Serial.println();
    Serial.println(F("char* to byte*")); 
    Serial.print(efsize, HEX);
    Serial.println(F(" ef size")); 
    Serial.print(charsize, HEX);
    Serial.println(F(" chr size")); 
    Serial.println(bytes); 
  }
}

void readCharsFromE(int index, char* original){ 
    //Serial.println("original"); 
    //Serial.println(original); 
    readFrame(index); 
    if( EfState == 1 ) return byteToChar();
    else  {
        EfState = (byte)1;
        chartoByte(original);
        writeFrame(index);
        readFrame(index); 
        original = byteToChar(); 
    }    
}

/*===========================================================
///////////////////////  ETH VARS   /////////////////////////
===========================================================*/

// MAC address 
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xBB, 0x00 };

// Device's IP address  
byte ip[] = {192, 168, 60, 82}; 

// IP server address 
byte ipServer[] = {192, 168, 60, 223}; 

//Server port
int ipServerPort = 5050;

//dns server 
byte dnServer[] = { 9, 9, 9, 9};  

//gateway 
byte gateway[] = {192, 168, 60, 1};  

//mask
byte subnet[] = {255, 255, 255, 0};   

//app server port
int appServerPort = 5050;

//web server
EthernetServer webServer(80);  // create a server at port 80

//app server
EthernetServer appServer(appServerPort);
 
// derive an instance of the HTTPserver class with custom handlers
class htmlServerClass : public HTTPserver
  {
  virtual void processPostType        (const char * key, const byte flags);
  virtual void processPostArgument    (const char * key, const char * value, const byte flags);
  };  // end of htmlServerClass

htmlServerClass htmlServer;


/*===========================================================
//////////////////////  HTML VARS   /////////////////////////
===========================================================*/
 
bool logIn=false;
char pass[]="sigmati0n";
char logout[]= "CLOSE";
char aON[]= "ALARM=ON";
char aOFF[]= "ALARM=OFF";
char debugL[]= "DEBUGL=";
char qDevId[]="?devid=";
char qZone[]="&zone=";
char qIp[]="&ip=";
//char qIp[]="&ip=";

char dBuffer[20];
byte sBuffer[6];
bool saveSetupFlag = false;



// -----------------------------------------------
//  User handlers
// -----------------------------------------------

void htmlServerClass::processPostType (const char * key, const byte flags)
  {
//     loadWebUI(); 
  } // end of processPostType

void resetEframe(){
  memset(Eframe,0,20);
}

void saveToEP(byte *thisArray, int frame,int dSize){
  resetEframe();
  EfState = (byte) 1;
  memcpy(Eframe, thisArray, dSize); 
  writeFrame(frame);
}

void readFromEP(byte *thisArray, int frame, int dSize){
  resetEframe();
  readFrame(frame);
  if(EfState==1) memcpy(thisArray, Eframe, dSize); 
  else saveToEP(thisArray, frame, dSize);
}

void saveToEPChar(int thisInt, int frame){
  resetEframe();
  EfState = (byte) 1; 
  sprintf (Eframe, "%d", thisInt); 
  writeFrame(frame);
}

void readFromEPChar(int *thisInt, int frame){
  resetEframe();
  readFrame(frame);
  if(EfState==1) thisInt = atoi(Eframe);
  else saveToEPChar(thisInt, frame);
}

void saveIp(){   saveToEP(ip,iip,4); }

void readIp(){   readFromEP(ip,iip,4); }


void saveMac(){   saveToEP(mac,imac,6); }

void readMac(){   readFromEP(mac,imac,6); }
 
void saveIps(){   saveToEP(ipServer,iips,4); }

void readIps(){   readFromEP(ipServer,iips,4); }

void saveIpsp(){   saveToEPChar(ipServerPort,iipsp); }

void readIpsp(){   readFromEPChar(&ipServerPort,iipsp); }

int hex8(char *in)
{
   uint8_t c, h;

   c = in[0];

   if (c <= '9' && c >= '0') {  c -= '0'; }
   else if (c <= 'f' && c >= 'a') { c -= ('a' - 0x0a); }
   else if (c <= 'F' && c >= 'A') { c -= ('A' - 0x0a); }
   else return(-1);

   h = c;

   c = in[1];

   if (c <= '9' && c >= '0') {  c -= '0'; }
   else if (c <= 'f' && c >= 'a') { c -= ('a' - 0x0a); }
   else if (c <= 'F' && c >= 'A') { c -= ('A' - 0x0a); }
   else return(-1);

   return ( h<<4 | c);
}

void toByteFormat (char input_string[], char separator[], int segment_number, bool hex) 
{
  char * token = input_string;
  
  byte thisByte=0; 
  
   // loop through the string to extract all other tokens
   for(  int i=0  ;  token != NULL && i< segment_number   ;   i++  ) {
      if(i==0) token = strtok(input_string,separator);
      else token = strtok(NULL,separator);
      if(token != NULL){
        Serial.print("token ");
        Serial.println(token);
        if (hex) thisByte = (byte)  hex8(token);
        else thisByte = (byte) atoi(token);
        sBuffer[i]=thisByte;
      }
   } 
}


void htmlServerClass::processPostArgument (const char * key, const char * value, const byte flags)
  {
    Serial.print("KEY ");
    Serial.println(key);
    Serial.print("value ");
    Serial.println(value); 
    Serial.println();
   if (strcmp(key,"pass")==0 && strcmp(value,pass)==0)
    {
    logIn=true; 
  } 

  if(logIn) {
      if (memcmp (key, "DEBUGL", 6) == 0 && isdigit (value [0]) )
        {
        debug = atoi (value); 
      } else if (strcmp(key,"ALARM")==0 && logIn )
        {
          Serial.print(" set alarm ");
        if(strcmp(value,"OFF")){
            Serial.print(" on ");
            alarm(1);  
        } else {
            Serial.print(" off ");
            alarm(0);
        }
        saveSetupFlag=true;
      } else if (strcmp(key,logout)==0 && strcmp(value,"TRUE")==0)
        {
        logIn=false; 
      } else if (strcmp(key,"devid")==0){
          address = atoi(value);     
          saveSetupFlag=true;
      } else if (strcmp(key,"zone")==0){
          zone = atoi(value);
          saveSetupFlag=true;
      } else if (strcmp(key,"ip")==0)
      {
        toByteFormat(value,".",4,false);
        memcpy(ip, sBuffer, sizeof(ip)); 
        saveIp();           
      } else if (strcmp(key,"mac")==0)
        {
        toByteFormat(value,":",6,true);
        memcpy(mac, sBuffer, sizeof(mac)); 
        saveMac();
      } else if (strcmp(key,"ips")==0)
        {
        toByteFormat(value,".",4,false);
        memcpy(ipServer, sBuffer, sizeof(ipServer)); 
        saveIps();
      } else if (strcmp(key,"ipsp")==0)
        {
        ipServerPort = atoi(value); 
        
      } else if (strcmp(key,"dns")==0)
        {
        toByteFormat(value,".",4,false);
        memcpy(dnServer, sBuffer, sizeof(dnServer)); 
        saveIpsp();
      } else if (strcmp(key,"gw")==0)
        {
        toByteFormat(value,".",4,false);
        memcpy(gateway, sBuffer, sizeof(gateway));  
        
      } else if (strcmp(key,"mask")==0)
        {
        toByteFormat(value,".",4,false);
        memcpy(subnet, sBuffer, sizeof(subnet)); 
        
      } else if (strcmp(key,"appsp")==0)
        {
        appServerPort = atoi(value); 
      }  
  }
  
}
 

// -----------------------------------------------
//  End of user handlers
// -----------------------------------------------



/*===========================================================
///////////////////////  SYS VARS   /////////////////////////
===========================================================*/

SoftDMD dmd(DISPLAYS_WIDE,DISPLAYS_HIGH,PIN_NOE,PIN_A,PIN_B,PIN_SCK,PIN_CLK,PIN_R);
//SoftDMD dmd(DISPLAYS_WIDE,DISPLAYS_HIGH);
DMD_TextBox box(dmd, 0, 0, 32, 16);

/*/-- System --/*/
int alarmflags[101];
int doCommand=0;
int bufferToWrite=0;
int app_id=0x01;
int currentBuffer=4;
int currentLine=0;
const int max_per_line=4;
  
/*/-- system time handle --/*/
  unsigned long start_t=0 ;  //SERIAL
  unsigned long work_last=0;
  unsigned long buzzer_start=0 ;  //SERIAL
  unsigned long buzzer_current=0;  
  unsigned long work_current=0;
  const unsigned long work_frame=600;
  const unsigned long answer_time=2000;
  const unsigned long line_time=1500;
  unsigned long line_time_start=0;
  unsigned long line_time_current=0;
  unsigned long protocol_start=0;
  unsigned long protocol_current=0;
  
/*/-- LED system buffers and flags --/*/
  char ledbuffer[5][50];  // 4 buffers de 50 caracteresz
  char ledSubBuffer[5][4][20];  // 4 buffers con 4 subbuffers de 14 caracteresz
  int ledflags[5][4];     // 4 flags: 0-set 1-largo 2-lineas calculadas 3-not used 
  const char *MESSAGE = "       SIGMA TELECOM";

/*/-- system PINOUT --/*/  
const byte alarm_pin = CONTROLLINO_R6;
const byte interruptPin = CONTROLLINO_IN0;
const byte button_pin = CONTROLLINO_IN0;

/*/-- Button Handling --/*/
volatile byte state = LOW;

const byte activate = LOW;
const byte deactivate = HIGH;

unsigned long last_interrupt_time = 0;
unsigned long interrupt_time = millis();
 
/*/-- System flags --/*/ 
int button_pressed=0;
int button_pressedx=0;
int alarm_flag=0;

/*========================================================================
    
    flags:
        0:set => 0 sin mensajes, 1 con mensaje, 2+ con mensaje emergencia 
               si setea mensaje emergencia, toma prioridad sobre otros 
               mensajes de emergencia(set+1 del mensaje de emergencia 
               con mayor prioridad), si existe almenos 1 mensaje de 
               emergencia, los mensajes normales son ignorados.
               Si solo existe un mensaje de emergencia, tomara el valor 2.
        1:largo => indica el largo del mensaje
        
========================================================================*/

/*========================================================================

chars especiales:
$ : escape
& : vehiculo
@ : exclamacion en triangulo
^ : peligro electrico
{ : fuego
} : derrumbe
~ : eñe
0x7F : FULL

========================================================================*/

/*===========================================================
/////////////////////////  SETUPS   /////////////////////////
===========================================================*/


void setup() {
  alarm(0);
  dmd.setBrightness(255);
  dmd.selectFont(Sigma9);
  dmd.begin();
  Serial.begin(2000000);
  Serial3.begin(9600); 
  attachInterrupt(digitalPinToInterrupt(interruptPin), irq_button, RISING); 
  testroutine();
  delay(500);
  clearDataBuffer();
  startDelta();
  pinMode(alarm_pin,OUTPUT);
  digitalWrite(alarm_pin,deactivate);
  delay(1); 
  loadSettings();
  Controllino_RS485Init(); 
  RS485RECEIVE(false);
  Ethernet.begin(mac, ip, dnServer, gateway, subnet);
  webServer.begin();           // start to listen for clients  
  appServer.begin();           // start to listen for clients
  delay(2000);
  serialMessage();  
}  

/*===========================================================
//////////////////    Utility functions  ////////////////////
===========================================================*/

void serialMessage(){
  ip2Char(ip);
  Serial.println(F("///////////////////////////////////////////////"));
  Serial.println();
  Serial.println(F("SIGMATION FIREWATCHER HTML MicroServer")); 
  Serial.println(F("started at "));
  Serial.print(dBuffer);
  Serial.println(F("  port 80 "));
  ip2Char(gateway);
  Serial.print(dBuffer);
  Serial.println(F("  gateway "));
  Serial.println();
  Serial.print(F("/////////////// SIGMA TELECOM ////////////////////"));
}

void loadSettings(){
    //TODO: LOAD SYSTEM SETTINGS! 
    Serial.println(F("read 0 ")); 
    readFrame(0);
    if(EfState==1){ 
      button_pressed=Eframe[ialarm]; 
      zone=Eframe[izone]; 
      address=Eframe[iaddress];  
    } else {
      saveSetup();  
    } 
    readIp(); 
    readMac(); 
    readIps(); 
    readIpsp();
}

void toggleValue(byte State, int index){
   readFrame(0);
   byte readE = Eframe[index]; 
   if (readE != State ) {
       Eframe[index]=State;
       saveSetup();
    } 
 
 }

void toggleAlarmState(byte State){
   toggleValue(State, ialarm);
}

void setZoneE(byte zone){ 
   toggleValue(zone, izone);
}

void saveSetup(){
    Serial.println(F("saving setup")); 
    EfState = (byte) 1;
    Eframe[0]=(byte) alarm_flag;
    Eframe[1]=(byte) zone;
    Eframe[2]=(byte) address;
    writeFrame(0);
    saveSetupFlag = false;
}

/*===========================================================
////////  CONTROLLINO RS485 CONTROL FUNCTIONS    ////////////
===========================================================*/

void RS485SEND(){ 
      if(debug>0) Serial.print(F("\nSalida de datos\n"));
      Controllino_SwitchRS485RE(1);
      Controllino_SwitchRS485DE(1);
  }

void RS485RECEIVE(boolean debugx){ 
      if(debug==1 && debugx) Serial.print(F("\nEntrada de datos\n"));
      Controllino_SwitchRS485RE(0);
      Controllino_SwitchRS485DE(0);
  }

/*===========================================================
//////////////////    packet analyzer    ////////////////////
===========================================================*/
  const byte NoAlarm= 0x00; 
  const byte Alarm= 0x01; 
  const byte setThisZone= 0x02; 
  const byte zoneNoAlarm= 0xB0; 
  const byte zoneAlarm= 0xC1; 
  const byte serverPing= 0x03; 
  
/*/-- packet data --/*/
  int command=0, data[200];
  int code=0;
  int code2=0;
  int r_zone;
  int r_master_ad=0;
  int r_ad=0;
  int r_app_id=0;
  int r_chck=0;
/*/-- packet bytes --/*/
  int o_address = 0x00;
  int packet[255];
  int packet_o[255];
  int packet_counter=0;
  int packet_ini=0;
  int packet_end=0;
  int packet_size=0;
  int dataBuffer[255];
  int sys_dataBuffer[255];
  int dataBufferLength=0;
/*/-- packet logic --/*/
  boolean zoneExecution=false;
  boolean execution=false;
  boolean setZone=false;
  boolean isPing=false;
  boolean isAnAlarm=false;
  boolean isaTask=false;
  int zoneToExecute=0;
  

boolean packetAnalizer(){
  packet_check();
  e_packet_check();
  isAnAlarm=false;
  execution=false;
  setZone=false;
  zoneExecution=false;
  isPing=false;
  isaTask=false;
  zoneToExecute=0;
  if(packet_end!=1) return false;
  //if(checksum()!=0) return false;
  char messg0[] ="                           ";
  sprintf(messg0,"command:function state %d ",command);
  if(debug>0) Serial.println(messg0);
  
  switch(command){

      case NoAlarm    :   if(r_master_ad==address){
                              isAnAlarm=false;
                              execution=true;
                              setZone=false;
                              zoneExecution=false;
                              isPing=false;
                          } else {
                                clear_serial();
                                return false;
                            }                           
                          break;
      case Alarm      :   if(r_master_ad==address){
                            isAnAlarm=true;
                            execution=true;
                            setZone=false;
                            zoneExecution=false;
                            isPing=false; 
                          } else {
                                clear_serial();
                                return false;
                            } 
                          break;
      case setThisZone:   if(r_master_ad==address){
                            isAnAlarm=false;
                            execution=false;
                            setZone=true;
                            zoneExecution=false;
                            isPing=false;
                            zoneToExecute=code2;
                          } else {
                                clear_serial();
                                return false;
                            }  
                          break;
      case zoneNoAlarm:   isAnAlarm=false;
                          execution=false;
                          setZone=false;
                          zoneExecution=true;
                          isPing=false;
                          zoneToExecute=code;
                          break;
      case zoneAlarm  :   isAnAlarm=true;
                          execution=false;
                          setZone=false;
                          zoneExecution=true;
                          isPing=false;
                          zoneToExecute=code;
                          break;
      case serverPing :   isAnAlarm=false;
                          execution=false;
                          setZone=false;
                          zoneExecution=false;
                          isPing=true;
                          zoneToExecute=code;
                          break;
      default         :   isAnAlarm=false;
                          execution=false;
                          setZone=false;
                          zoneExecution=false;
                          isPing=false;
                          isaTask=false;
                          clear_serial();
                          return false; 
  }
  
  if(packet_size>5 && (execution || setZone)){
    int startpoint=packet_size-4;
    for(int i=0;i<4;i++){
            data[i]=packet[6+i+startpoint];
    }
    isaTask=true;
  }
  return true;
  //Serial.println("done");
}

/*===========================================================
///////////////////////    LOOP    //////////////////////////
===========================================================*/

void loop() {
  wdt_enable(WDTO_8S);
  if(packetAnalizer()){
      if(debug>0) Serial.print(F("\ncommand mode\n"));
      commandMode();
      clear_serial();
  }      
  writeZones();// writeMessages();
  check_state();
  checkProtocol();
  htmlServerInstance();
  //delay(10);
}

/*===========================================================
/////////////////////////  FUNCIONES  ///////////////////////
===========================================================*/
/////////////=============  DEMO  ===============////////////

int testroutine(){
  //dmd.drawFilledBox(0,0,32,16);
  //delay(3000);
  const char *next = MESSAGE; 
  while(*next) { 
    box.print(*next);
    delay(200);
    next++;
  }
  return 0;
}

/////////////=============   IRQ  ===============////////////

void irq_button(){
     interrupt_time = millis();
     // If interrupts come faster than 200ms, assume it's a bounce and ignore
     unsigned long delta_time =  interrupt_time - last_interrupt_time;
     delta_time = abs(delta_time);
     if (delta_time > 200) 
     {
        button_pressedx=1;
        detachInterrupt(digitalPinToInterrupt(interruptPin));
     }
     last_interrupt_time = interrupt_time;      
  }

/////////////============= SYSTEM ===============////////////
int commandMode(){
  if(execution){
    if(!isAnAlarm){
        alarm(0);
        code= 0x00;
        if(debug>0) Serial.print(F("\nAlarm deactivated from server\n"));
    }else if(isAnAlarm){
        alarm(1);
        code= 0x01;
        if(debug>0) Serial.print(F("\nAlarm activated from server\n"));
    }
  } else if (zoneExecution){
      doZoneFunction();  
  } else if(setZone){
      setCurrentZone(1);
      code= alarm_flag;
      if(debug>0) Serial.print(F("\nZone has been changed \n"));
  } else if(isPing){
      code= alarm_flag;
      if(zone!=zoneToExecute) setCurrentZone(0);
      if(debug>0) Serial.print(F("\nServer ping\n"));
  } 

  if(execution || setZone || isPing){
      if(debug>0) Serial.print(F("\sending answer ...\n"));
      if(isaTask) send_packet(code,code, 0 , 4); 
      else send_packet(code,code, 0 , 0);  
  }
}

void setCurrentZone(int fromcode){
  if (zoneToExecute>0 && zoneToExecute<101){
        char messg0[]="                                  ";
        sprintf(messg0,"setzone %d %d ",zoneToExecute,fromcode);
        if(debug>0) Serial.println(messg0);
        if (zoneToExecute!=0 && zone!=zoneToExecute){
//          zone=zoneToExecute; 
           setZoneE(zone); 
        } 
        updatezone();
    }
}


void updatezone(){
    
    switch(alarm_flag){ //alarm_flag
      case  0  :    sys_messages(1);
                    break;
                  
      case  1  :    sys_messages(0);
                    break;
                    
      default  :    error(0x03);
                    break;
      
      }
  }
  
int doZoneFunction(){
  if (zoneToExecute>0 && zoneToExecute<101) alarmZ(); 
}


int sys_messages(int messagenum){
       char messg0[]="{   }       ";
       char messg1[]="ZONA    `";
       //messg0[12]=127;
      sprintf(messg0,"{   }    %02d ",zone);
      sprintf(messg1,"ZONA %02d `",zone);
       
       switch(messagenum){
      
        case  0  :    set_message(messg0,12);
                      break;
        case  1  :    set_message(messg1,9);
                      break;
        default  :    error(0x03);
                      break;
      }
  }

int sys_messagesZone(int messagenum, int zonex){
       char messg0[]="{   }       ";
       char messg1[]="ZONA    `";
       messg0[12]=127;
       sprintf(messg0,"{   ZONA %02d ",zonex);
       sprintf(messg1,"ZONA %02d `",zonex);
      
       switch(messagenum){
      
        case  0  :    set_messageZone(messg0,12,zonex);
                      break;
        case  1  :    zoneTurnOff(zonex);
                      break;
        default  :    error(0x03);
                      break;
      }
  }
  
int set_message(char messg[], int datlen){
    //sys_savebuffer(int bufferlen,int bufferToWrite_);
    sys_setData( messg, datlen);
    sys_savebuffer(datlen,4);
  }

int set_messageZone(char messg[], int datlen, int zonex){
    //sys_savebuffer(int bufferlen,int bufferToWrite_);
    int freebuffer = getAvailableBuffer(zonex);
    ledflags[freebuffer][3]=zonex;
    sys_setData( messg, datlen);
    sys_savebuffer(datlen,freebuffer);
  }

int sys_setData(char messg[],int datlen){   
    dataBufferLength=datlen;  
    for (int i=0; i<dataBufferLength; i=i+1){
               sys_dataBuffer[i]=messg[i];
     } 
}

void alarmsignal(int set){
    switch(set){
      
        case  0  :    digitalWrite(alarm_pin, deactivate);
                      break;
        case  1  :    digitalWrite(alarm_pin, activate); 
                      break;
        default  :    error(0x04);
                      break;
      }  
  }
  
int alarm(int toggle_alarm){

    char messg0[]="                                  ";
    sprintf(messg0,"do zone function state %d zone %d ",toggle_alarm,zone);
    //if(debug>0) Serial.println(messg0);
    
    if(toggle_alarm==1){
                 alarm_flag=1;
                 alarmflags[zone]=1;
                 button_pressed=0;
                 button_pressedx=0;
                 alarmsignal(1);
                 sys_messages(0);                 
      }
    else if (toggle_alarm==0){
                 alarm_flag=0;
                 button_pressed=0;
                 button_pressedx=0;
                 alarmflags[zone]=0;
                 alarmsignal(0);
                 sys_messages(1);     
      }
    toggleAlarmState((byte)toggle_alarm);
    return 0;
  }


int alarmZ(){
    char messg0[]="                                              ";
    int thisIsAnAlarm = 0;
    if(isAnAlarm) thisIsAnAlarm = 1;
    sprintf(messg0,"Z_ do zone function state %d zone %d ",thisIsAnAlarm,zoneToExecute);
    if(debug>0) Serial.println(messg0);
    if(isAnAlarm){
                 alarmflags[zoneToExecute]=1;
      }
    else if (!isAnAlarm){
                 alarmflags[zoneToExecute]=0;  
      }
    return 0;
  }

int sendActivatePacket(){
    send_packet(0xA7,0xA7, 0 , 0);
                 for (int i=0; i<3;i++){
                     if(!wait_answer()){
                          send_packet(0xA7,0xA7, 0 , 0);
                      }                  
                  }
    return 0;
}

int check_state(){
      if(button_pressed==1){// && alarm_flag==0) {
                 alarm(1);
                 char messg0[]="                                       ";
                  sprintf(messg0,"rst:function state %d zone %d ",1,zone);
                  if(debug>0) Serial.println(messg0); 
                  sendActivatePacket();
                  button_pressed==0;
                  button_pressedx==0;
                 
        }
      else if(button_pressedx==1){// && alarm_flag==0) {
                 alarm(1);
                 char messg0[]="                                        ";
                  sprintf(messg0,"int:function state %d zone %d ",1,zone);
                  if(debug>0) Serial.print("Activado"); 
                  sendActivatePacket();
                  button_pressed==0;
                  button_pressedx==0;
                 
        }  
      else if (digitalRead(button_pin)==HIGH) {
                alarm(1);
                char messg0[]="                                      ";
                  sprintf(messg0,"btn:function state %d zone %d ",1,zone);
                  if(debug>0) Serial.println(messg0); 
                  sendActivatePacket();
                  button_pressed==0;
                  button_pressed==0;
                  button_pressedx==0;
        }

        attachInterrupt(digitalPinToInterrupt(interruptPin), irq_button, RISING);
        if(alarm_flag) alarm(1);
        return 0;
  }


int sys_savebuffer(int bufferlen,int bufferToWrite_){
  
  if(bufferlen<50){
      for(int i=0;i<50 ;i++){
        ledbuffer[bufferToWrite_][i]= sys_dataBuffer[i];       
      }
      ledflags[bufferToWrite_][0]=5;
      //if(bufferToWrite_==4)  ledflags[bufferToWrite_][0]=1;
      ledflags[bufferToWrite_][1]=bufferlen;
      ledflags[bufferToWrite_][2]=calcLines(bufferlen);
      for(int i=0;i<ledflags[bufferToWrite_][2] ;i++){
        for(int j=0;j<max_per_line+5 ;j++){
          if(j<max_per_line)ledSubBuffer[bufferToWrite_][i][j]= ledbuffer[bufferToWrite_][(i*max_per_line)+j]; 
          else  ledSubBuffer[bufferToWrite_][i][j]= 0x20;      
        }      
      }
  }
 /* Serial.write("< write buffer: ");
  Serial.write(bufferToWrite_+0x30);
  Serial.write(" : ");
  Serial.write(ledflags[bufferToWrite_][0]+0x30);
  Serial.write(" >");*/
  clearDataBuffer();
  return 0;
}

int savebuffer(int bufferlen,int bufferToWrite_){
  
  if(bufferlen<50){
      for(int i=0;i<50 ;i++){
        ledbuffer[bufferToWrite_][i]= dataBuffer[2+i];       
      }
      ledflags[bufferToWrite_][0]=dataBuffer[0];
      if(bufferToWrite_==4)  ledflags[bufferToWrite_][0]=1;
      ledflags[bufferToWrite_][1]=bufferlen;
      ledflags[bufferToWrite_][2]=calcLines(bufferlen);
      for(int i=0;i<ledflags[bufferToWrite_][2] ;i++){
        for(int j=0;j<20 ;j++){
          if(j<max_per_line)ledSubBuffer[bufferToWrite_][i][j]= ledbuffer[bufferToWrite_][(i*max_per_line)+j]; 
          else  ledSubBuffer[bufferToWrite_][i][j]= 0x20;      
        }      
      }
  }
  //Serial3.write(0xFA);
  //Serial3.write(bufferToWrite_);
  //Serial3.write(ledflags[bufferToWrite_][0]);
  //Serial3.write(0xFB);
  clearDataBuffer();
  return 0;
}

int deleteBuffer(int buffernum){
  for(int i=0;i<50;i++){
    ledbuffer[buffernum][i]= 0;
  }
      ledflags[buffernum][0]=0;
      ledflags[buffernum][1]=0;
      ledflags[buffernum][2]=0;
      return 0;
}

int deleteAllBuffers(){

  for(int j=0;j<5;j++){
     for(int i=0;i<50;i++){
      ledbuffer[j][i]= 0;
    }
        ledflags[j][0]=0;
        ledflags[j][1]=0;
        ledflags[j][2]=0;
  }
  return 0;
  
}

int clearDataBuffer(){

  for(int j=0;j<255;j++){
      dataBuffer[j]= 0x20;    
  }
  return 0;
}

int error(int code){
  /*
    Error codes:
    0x00: buffer incorrecto
    0x01: largo incorrecto de mensaje
    0x02: codigos de error 0x00 y 0x01 juntos
    0x03: comando incorrecto/ error de sistema 
  */
  //send_packet(0x0E, code, 0xFF, 0x00);
  if(debug>0) Serial.print("::: err :::");
  return 0;
}
/////////////============= PANEL LED ============////////////

int topPriority(){
  int topprior=0;
  for(int i=0;i<5;i++){
      if (topprior<ledflags[i][0]) topprior=ledflags[i][0];
  }
  return topprior;
}

int zoneTurnOff(int zonex){
  for(int i=0;i<5;i++){
      if (ledflags[i][3]==zonex) ledflags[i][0]=0;
  }
  return 0;
 }

 int getAvailableBuffer(int zonex){
    for(int i=0;i<4;i++){
      if (ledflags[i][3]==zonex) return i;
    }
    for(int i=0;i<4;i++){
      if (ledflags[i][0]==0) return i;
    }
    return 0;
  }

int calcLines(int sizeOfBuffer){
  for(int i=1;i<=4 && sizeOfBuffer>0 ;i++){
    if (sizeOfBuffer<=(max_per_line*i)) return i;  
  }
  return 0;
}

long getDelta(){
    line_time_current=millis();
    return line_time_current-line_time_start;
  }

void checkProtocol(){
    RS485RECEIVE(false);
    if(getDeltaProtocol()>line_time){
          clear_serial();
          startDeltaProtocol();
      }
  }  

bool isProtocolActive(){ 
    if(getDeltaProtocol()>line_time){
          return false;
      }
      return true;
  }  

long getDeltaProtocol(){
    protocol_current=millis();
    return line_time_current-protocol_start;
  }

int startDeltaProtocol(){
    protocol_current=millis();
    protocol_start=millis();
    return 0;
  }

int startDelta(){
    line_time_current=millis();
    line_time_start=millis();
    return 0;
  }
  
int writeMessages(){
  //Serial.write(0xAA);
  int topprior = topPriority();
  long time_delta=getDelta();
  if(ledflags[currentBuffer][0]==topprior && time_delta>line_time){

      dmd.drawString(0, 0, ledSubBuffer[currentBuffer][currentLine]);
      startDelta();  
      currentLine++; 
      if (currentLine >= ledflags[currentBuffer][2] | currentLine >3) {
          currentLine=0;
          currentBuffer++;
          if(currentBuffer>4) currentBuffer=0;
        }  
      
   } else if (ledflags[currentBuffer][0]!=topprior) {

      currentBuffer++;
      currentLine=0;
      if(currentBuffer>4) currentBuffer=0;
   }
   return 0;
}

int checkAlarms(){
    int localflag=0;
    for(int i=1; i<101;i++){
        if(alarmflags[i]>0) localflag=1;
      }  
    alarmflags[0]=localflag;
    alarmsignal(localflag);
    return localflag;
}

int writeThisZone(){
  //Serial.write(0xCC);
  long time_delta=getDelta();
  if(time_delta>line_time){
      dmd.drawString(0, 0, ledSubBuffer[4][currentLine]);
      startDelta();  
      currentLine++; 
      if (currentLine >= ledflags[4][2] | currentLine >3 ) {
          currentLine=0;
          currentBuffer++;
        }     
   } 
   return 0;
}
int writeZones(){  //alarmflags[100]
  long time_delta=getDelta();
  checkAlarms();    
  if (alarmflags[0]==0) { 
      //Serial.write(0xAB);
      currentBuffer=0;
      writeThisZone();
  }
  else if (currentBuffer==100) writeThisZone();
  //else if (currentBuffer==zone) currentBuffer++;
  
   
  else if(currentBuffer>0 && alarmflags[currentBuffer]>0 && time_delta>line_time && currentBuffer<100){
      //Serial.write(0xDD);
      char messg0[]="    ";
      sprintf(messg0," %02d `",currentBuffer);
      dmd.drawString(0, 0, messg0);
      startDelta();  
      currentBuffer++;
            
   } else if (time_delta>line_time){

      currentBuffer++;
      if(currentBuffer>100) {
         char messg1[]="    ";
         if (alarmflags[0]>0 && currentLine==0) sprintf(messg1,"{   ");
         if (alarmflags[0]>0 && currentLine==1) sprintf(messg1,"ZONA");
         currentLine++;
         if(currentLine>1) {currentBuffer=0; currentLine=0;}
         dmd.drawString(0, 0, messg1);
         startDelta();  
      }
   }
   return 0;
}

/////////////============= PANEL LED ============////////////

int systemMessage(char towrite[]){
  //dmd.drawString(0, 0, F((towrite));
  char *next = towrite;
  box.print(*next);
  return 0;
}

/////////////=============  COMM  ==============/////////////



int wait_answer(){
    unsigned long current_t=0, delta_t=0;
    start_t=millis();

    while(delta_t<answer_time){
      current_t = millis();
      delta_t = current_t-start_t;
      packet_check();
      writeMessages();
      wdt_enable(WDTO_1S);
      if(packet_end==1)  {  return 1; }
    }
    return 0;
  }

void getMessage(){
    if(packet_size>=2)getData(packet_size);
}
  
void getData(int ldata){   
    dataBufferLength=ldata;  
    for (int i=0; i<ldata; i=i+1){
               dataBuffer[i]=packet[6+i];
     } 
}

void clear_serial(){
  
  for(int i=0; i<10; i=i+1){
      packet[i]=0;
    }
    packet_end=0;
    packet_ini=0; 
    packet_counter=0;
    packet_size=0;    
}  
  
//////////=============  PROTOCOLO  ==============//////////
  

int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera      :   0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de unidad maestra
    3 address       :   direccion de este dispositivo
    4 zone          :   zona de ubicacion de este dispositivo
    5 largodatos    :   largo de datos ( 1 comando + 1 codigo + largo)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
_____________________________________________________________________________________________________________________________

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================*/
  
    while (Serial3.available() && packet_end!=1 ) {
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      startDeltaProtocol();
      packet[packet_counter] = Serial3.read();
      if(debug>0) Serial.write(packet[packet_counter]);
      if(packet_counter==6 && packet_ini>0 && packet_size>0 )  {command=packet[packet_counter];}//systemMessage("    x");}
      else if(packet_counter==6 && packet_size==0){clear_serial();return 0;}
      else if(packet_counter==7 && packet_ini>0 && packet_size>1)  {code=packet[packet_counter];}//systemMessage("    x");}
      else if(packet_counter==8 && packet_ini>0 && packet_size>2)  {code2=packet[packet_counter];}//systemMessage("    x");}
      
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {r_app_id=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {r_master_ad=packet[packet_counter]; packet_counter= packet_counter+1;
      } 
      //if (r_master_ad!=address && r_master_ad!=0) {Serial.write(0xBB);Serial.write(r_ad);packet_counter=0; packet_size=0; packet_ini=0;}}//systemMessage(" x");}
      else if(packet_counter==3 && packet_ini>0)  {r_ad=packet[packet_counter]; packet_counter= packet_counter+1;  }//systemMessage(" x");}
      else if(packet_counter==4 && packet_ini>0)  {r_zone=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==5 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}     
      else if(packet_counter<6+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==6+packet_size && packet_ini>0){r_chck=packet[packet_counter];packet_counter= packet_counter+1;}//systemMessage("       x!");}
      else if(packet_counter==7+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;if(debug>0) Serial.print("!\n");}//systemMessage("       x!");}
      else if(packet_counter>8+packet_size && packet_end!=1){ clear_serial();}
    } 
    while  (Serial3.available() && packet_end==1){
        byte dummy=Serial3.read();
    }
    return 0;
  }  
  

int e_packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera      :   0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de unidad maestra
    3 address       :   direccion de este dispositivo
    4 zone          :   zona de ubicacion de este dispositivo
    5 largodatos    :   largo de datos ( 1 comando + 1 codigo + largo)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
_____________________________________________________________________________________________________________________________

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================*/
    EthernetClient client = appServer.available();
    boolean started = false;
    if (client) { 
      startDeltaProtocol();
      if(debug==1 && client.connected() && client.available() ) {
        Serial.println();
        Serial.println(F("Got Ethernet packet"));
        started=true;
        delay(10);
      }
      packet_counter=0;
      while (client.connected() && client.available() && packet_end!=1 ) {
        if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}        
        packet[packet_counter] = client.read();
        if(debug>0) Serial.print(packet[packet_counter],HEX);
        if(packet_counter==6 && packet_ini>0 && packet_size>0 )  {command=packet[packet_counter];}//systemMessage("    x");}
        else if(packet_counter==6 && packet_size==0){clear_serial();return 0;}
        else if(packet_counter==7 && packet_ini>0 && packet_size>1)  {code=packet[packet_counter];}//systemMessage("    x");}
        else if(packet_counter==8 && packet_ini>0 && packet_size>2)  {code2=packet[packet_counter];}//systemMessage("    x");}
        
        if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
        else if(packet_counter==1 && packet_ini>0)  {r_app_id=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
        else if(packet_counter==2 && packet_ini>0)  {r_master_ad=packet[packet_counter]; packet_counter= packet_counter+1;
        } //if (r_master_ad!=address && r_master_ad!=0) {Serial.write(0xBB);Serial.write(r_ad);packet_counter=0; packet_size=0; packet_ini=0;}}//systemMessage(" x");}
        else if(packet_counter==3 && packet_ini>0)  {r_ad=packet[packet_counter]; packet_counter= packet_counter+1;  }//systemMessage(" x");}
        else if(packet_counter==4 && packet_ini>0)  {r_zone=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
        else if(packet_counter==5 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}     
        else if(packet_counter<6+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
        else if(packet_counter==6+packet_size && packet_ini>0){r_chck=packet[packet_counter];packet_counter= packet_counter+1;}//systemMessage("       x!");}
        else if(packet_counter==7+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;if(debug>0) Serial.print("!\n");}//systemMessage("       x!");}
        else if(packet_counter>8+packet_size && packet_end!=1){ clear_serial();}
      } 
      while  (client.connected() && client.available() && packet_end==1){
          byte dummy=client.read();
      }
    }
    client.stop();
    if(started) if(debug>0) Serial.println();
    if(started) if(debug>0) Serial.println(F("Disconnected"));
    return 0;
  }  

  int checksum(){
       byte checksm = packet[1];
       for (int i=2; i<packet_size+6; i=i+1){
           checksm = packet[i] ^ checksm;
        }  
       if(checksm==r_chck) {
          if(debug>0) Serial.print(F("\nchecksum ok\n"));
          return 0;
        } 
       else {
          char messg0[]="                                  ";
          sprintf(messg0,"chksm %d %d ",checksm,r_chck);
          if(debug>0) Serial.print(messg0);
          if(debug>0) Serial.print(F("\nchecksum fail\n"));
          return 1;
        } 
    }
       

  void send_packet(int commando, int codigo, int s_address, int ldata){
    
    
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de este dispositivo
    3 address       :   direccion de unidad maestra
    4 zone          :   zona de ubicacion de este dispositivo
    5 largo         :   largo de data ( 1 comando + 1 codigo + datos)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
=============================================================================================================================*/

         int chcksm=0;
         packet_o[0] = 0xFE; 
         packet_o[1] = app_id;
         chcksm = chcksm ^ packet_o[1];
         packet_o[2] = master_ad;
         chcksm = chcksm ^ packet_o[2];
         packet_o[3] = address;
         chcksm = chcksm ^ packet_o[3];
         packet_o[4] = zone;
         chcksm = chcksm ^ packet_o[4];
         packet_o[5] = ldata+2;
         chcksm = chcksm ^ packet_o[5];
         packet_o[6] = commando;
         chcksm = chcksm ^ packet_o[6];
         packet_o[7] = codigo;
         chcksm = chcksm ^ packet_o[7];
         for (int i=0; i<ldata; i=i+1){
               packet_o[8+i] = data[i];
               chcksm = chcksm ^ packet_o[8+i];
        } 
        packet_o[8+ldata] =  chcksm; 
        packet_o[9+ldata] =  0xFD;
        RS485SEND();
        delay(5);
        int lengthpacket=10+ldata;
        //if(debug>0) Serial.println("\nSending length"+lengthpacket+" with data "+ldata);
        for (int i=0; i<10+ldata;i=i+1){
          Serial3.write(packet_o[i]);
       }
       delay(1);
       RS485RECEIVE(true);
       EthernetClient sClient;
       sClient.connect(ipServer,ipServerPort);
       for (int i=0; i<10+ldata;i=i+1){
           sClient.write(packet_o[i]);
       }      
       sClient.stop();
    }



/*===========================================================
/////////////////////////  HTTP FUNCTIONS  //////////////////
===========================================================*/



void htmlServerInstance(){
    EthernetClient client = webServer.available();  // try to get client 
    if (client) {  // got client?
        htmlServer.begin (&client);
        boolean currentLineIsBlank = true;
        boolean loadWeb = false;
        int cnt=0;
        
        if (client.connected()) {
          
            while (client.available() && !htmlServer.done) {   // client data available to read
                 htmlServer.processIncomingByte (client.read ());
            }  
        } 
        /** 
         *  If web app is enabled, it's time to send it to the client
        */
        if(saveSetupFlag) saveSetup();
        loadWebUI();
        /*
         * Close the connection and clear buffer
        */
        client.stop(); // close the connection 
    }  
}
  

void sendToServer(){
      EthernetClient sClient;
      sClient.connect(ipServer,5050);
      sClient.println("test");
      sClient.stop();  
}



void loadWebUI(){ 
      if(!logIn) loadLogIn();
      else loadWebApp();
}


void loadLogIn(){ 
      loadHead();
      loadLBody();
      //loadFooter();
      htmlEnd();
}


void loadWebApp(){ 
      loadHead();
      loadBody();
      loadFooter();
      htmlEnd();
}

void loadHead(){
      htmlServer.println(F("HTTP/1.1 200 OK"));
      htmlServer.println(F("Content-Type: text/html"));
      htmlServer.println(F("Connection: close"));
      htmlServer.println();
      // send web page
      htmlServer.println(F("<!DOCTYPE html>"));
      htmlServer.println(F("<html>"));
      htmlServer.println(F("<head>"));
      htmlServer.println(F("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"));
      loadStyle();
      htmlServer.println(F("<title>FIREWATCHER REMOTE UNIT CONFIGURATION</title>")); 
      htmlServer.println(F("</head>"));
}

void loadBody(){
      htmlServer.println(F("<body onload = \"ar(10000);\" "));
      if (alarm_flag) htmlServer.println(F(" class='emergency' "));
      htmlServer.println(F(">"));
      printTitle();
      htmlServer.println(F("<br>"));
      loadAlarmForm();
      loadSettingsForm(); 
      loadDebugForm();
      loadLogOutForm();
      loadScript();
      htmlServer.println(F("</body>")); 
  }

void printTitle(){
  
    htmlServer.println(F("<h1 class='loginBody'>FIREWATCHER UNIT CONFIGURATION</h1>"));
  }
  
void loadLBody(){
      htmlServer.println(F("<body "));
      htmlServer.println(F(" class='loginBody' "));
      htmlServer.println(F(">"));
      printTitle();
      htmlServer.println(F("<p>Sigmation</p>")); 
      htmlServer.println(F("<br>")); 
      loadLogInForm();
      htmlServer.println(F("</body>")); 
}


void loadLogInForm(){
  loadForm("Log In");
  loadPassInput();
  endForm();
}

void labeledInput(__FlashStringHelper label[],__FlashStringHelper dType[], __FlashStringHelper dClass[], __FlashStringHelper dName[], __FlashStringHelper dValue[], boolean submit){
  htmlServer.print(F("<label for='"));
  htmlServer.print(dName);
  htmlServer.print(F("'>"));
  htmlServer.print(label);
  htmlServer.print(F("</label>"));   
  printInput(dType, dClass, dName, dValue, submit);
}

void labeledInputV( __FlashStringHelper label[],__FlashStringHelper dType[], __FlashStringHelper dClass[], __FlashStringHelper dName[], char* dValue, boolean submit){
  htmlServer.print(F("<label for='"));
  htmlServer.print(dName);
  htmlServer.print(F("'>"));
  htmlServer.print(label);
  htmlServer.print(F("</label>"));   
  printInputV(dType, dClass, dName, dValue, submit);
}

void loadPassInput(){  
  labeledInput(F("Password"),F("password"),F("pass rounded"),F("pass"),F(" "),false); 
  printInput(F("button"),F("abutton emergency"),F(" "),F("Log In"),true);
}


void loadAlarmForm(){
    loadForm("Alarm Control");
    loadAlarmControl();
    endForm();
}

void loadDebugForm(){
    loadForm("Serial Debug Options");
    loadDebugOptions();
    htmlServer.println(F("<p>Serial interface Bauds: 2000000</p>"));
    endForm();
} 

void loadSettingsForm(){
    loadForm("Device Settings");
    loadSystemSettings(); 
    endForm();
}

void loadLogOutForm(){
    loadForm("Log Out");
    loadLogOut(); 
    endForm();
}

void loadForm(char formName[]){
    htmlServer.println(F("<div><form method=\"POST\"><fieldset>"));
    htmlServer.print(F("<legend>"));
    htmlServer.print(formName);
    htmlServer.println(F("</legend>"));         
}

void endForm(){
    htmlServer.println(F("</fieldset></form></div>"));  
}

void loadScript(){
  htmlServer.println(F("<script>")); 
  htmlServer.println(F("function ar(t){"));
 // htmlServer.println(F(" setTimeout(\"window.location.href='/' \", t);"));
  htmlServer.println(F("}"));
  htmlServer.println(F("</script>"));
}

void loadStyle(){
  htmlServer.println(F(" \
    <style> \
      html{padding: 0px;}\
      body{padding: 0px; background:#e0e0e0;}\ 
      .loginBody{color:#CCCCCC;background:#003366;padding:0px;}\
      input,label { display:block;}\
      * {text-align: center !important;margin: auto !important;padding: 10px;font-family: 'Roboto', sans-serif;}\
      .rounded{border-radius:5px;}\
    </style>"));
  htmlServer.println(F(" <style> \
      .abutton {color: #fff !important;\ 
                  background: #003366;\
                  padding: 20px;\
                  border-radius: 5px;\
                  display: inline-block;\
                  border: none;\
                  transition: all 0.4s ease 0s;\
                  cursor: pointer;\
                  margin-top:10px !important;\
                  }\ 
    </style>"));
   htmlServer.println(F(" <style> \
       .abutton:hover { background: #0745E1; transition: all 0.4s ease 0s; }\
       .emergency { background: #ed3330; color: white! important;}\
       .smallPad{ padding: 2px !important;}\
       .sfooter{ background: #202020; color: #E8E8E8; position: absolute;left: 0;right: 0;z-index: 150;padding: 3px !important;}\
    </style>"));
    
}
 

//devid=15&zone=15&ip=192.168.60.82&mac=de%3Aad%3Abe%3Aef%3Abb%3A00&ips=192.168.60.223&ipsp=5050&dns=9.9.9.9&gw=192.168.60.1&mask=255.255.255.0&appsp=5050

void loadSystemSettings()
{ 
    labeledInputV(F("Device ID"),F("number"),F("smallPad rounded"),F("devid"),int2Char(address),false);  
    labeledInputV(F("Zone"),F("number"),F("smallPad rounded"),F("zone"),int2Char(zone),false);  
    labeledInputV(F("IP"),F("text"),F("smallPad rounded"),F("ip"),ip2Char(ip),false); 
    labeledInputV(F("Mac"),F("text"),F("smallPad rounded"),F("mac"),mac2Char(),false); 
    labeledInputV(F("IP Server"),F("text"),F("smallPad rounded"),F("ips"),ip2Char(ipServer),false); 
    labeledInputV(F("S. Port"),F("number"),F("smallPad rounded"),F("ipsp"),int2Char(ipServerPort),false); 
    labeledInputV(F("DNS"),F("text"),F("smallPad rounded"),F("dns"),ip2Char(dnServer),false); 
    labeledInputV(F("GW"),F("text"),F("smallPad rounded"),F("gw"),ip2Char(gateway),false); 
    labeledInputV(F("Mask"),F("text"),F("smallPad rounded"),F("mask"),ip2Char(subnet),false); 
    labeledInputV(F("App Port"),F("number"),F("smallPad rounded"),F("appsp"),int2Char(appServerPort),false); 
    printInput(F("button"),F("abutton"),F(" "),F("Send"),true);
}

void loadDebugOptions(){
    if(debug==0) {
      labeledInputV(F("No Debug"),F("radio"),F("smallPad' checked '"),F("DEBUGL"),int2Char(0),false); 
      labeledInputV(F("Level 1"),F("radio"),F("smallPad "),F("DEBUGL"),int2Char(1),false);
      labeledInputV(F("Level 2"),F("radio"),F("smallPad "),F("DEBUGL"),int2Char(2),false);
    } else if(debug==1) {
      labeledInputV(F("No Debug"),F("radio"),F("smallPad "),F("DEBUGL"),int2Char(0),false); 
      labeledInputV(F("Level 1"),F("radio"),F("smallPad' checked '"),F("DEBUGL"),int2Char(1),false);
      labeledInputV(F("Level 2"),F("radio"),F("smallPad "),F("DEBUGL"),int2Char(2),false);
    } else if(debug==2) {
      labeledInputV(F("No Debug"),F("radio"),F("smallPad "),F("DEBUGL"),int2Char(0),false); 
      labeledInputV(F("Level 1"),F("radio"),F("smallPad "),F("DEBUGL"),int2Char(1),false);
      labeledInputV(F("Level 2"),F("radio"),F("smallPad' checked '"),F("DEBUGL"),int2Char(2),false);
    } else {
      labeledInputV(F("No Debug"),F("radio"),F("smallPad "),F("DEBUGL"),int2Char(0),false); 
      labeledInputV(F("Level 1"),F("radio"),F("smallPad "),F("DEBUGL"),int2Char(1),false);
      labeledInputV(F("Level 2"),F("radio"),F("smallPad "),F("DEBUGL"),int2Char(2),false);
    } 
    printInput(F("button"),F("abutton"),F(" "),F("Send"),true);
  }

char* int2Char(int finalval){ 
  sprintf (dBuffer, "%i", finalval);
  if(debug>1){
    Serial.println(F("int 2 char"));
    Serial.println(dBuffer);
  }
  return *dBuffer;
}

char* ip2Char(byte ipVal[]){ 
  sprintf (dBuffer, "%d.%d.%d.%d", ipVal[0], ipVal[1], ipVal[2], ipVal[3]);
  if(debug>1){
    Serial.println(F("ip 2 char"));
    Serial.println(dBuffer);
  }
  
  return *dBuffer;
}

char* mac2Char(){ 
  sprintf (dBuffer, "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3],mac[4],mac[5]);
  if(debug>1){
    Serial.println(F("mac 2 char"));
    Serial.println(dBuffer);
  }
  return *dBuffer;
}

 

void loadLogOut()
{ 
    printInput(F("hidden"),F(" "),F("CLOSE"),F("TRUE"),false);   
    printInput(F("button"),F("abutton"),F(" "),F("Log Out"),true);
}


void loadAlarmControl()
{
    char* checked1 = " ";    
    char* checked2 = " ";

    htmlServer.print(" <h2> "); 
    if(alarm_flag) htmlServer.print(F(" ALARM IS ON ")); 
    else htmlServer.print(F(" ALARM IS OFF ")); 
    htmlServer.print(" </h2> "); 

    if (alarm_flag) {    // switch on
         
        printInput(F("hidden"),F(" "),F("ALARM"),F("OFF"),false); 
        printInput(F("button"),F("abutton"),F(" "),F("DEACTIVATE"),true);  
    }
    else {              // switch off
        
        printInput(F("hidden"),F(" "),F("ALARM"),F("ON"),false); 
        printInput(F("button"),F("abutton emergency"),F(" "),F("ACTIVATE"),true);
    }  
}

void printInput( __FlashStringHelper dType[], __FlashStringHelper dClass[], __FlashStringHelper dName[], __FlashStringHelper dValue[], boolean submit){
     htmlServer.print(F("<input "));
     htmlServer.print(F("type='"));
     htmlServer.print(dType);
     htmlServer.print(F("' "));
     htmlServer.print(F(" class=' "));
     htmlServer.print(dClass);
     htmlServer.print(F("' "));
     htmlServer.print(F(" name='"));
     htmlServer.print(dName);
     htmlServer.print(F("' "));
     htmlServer.print(F(" value='"));
     htmlServer.print(dValue);
     htmlServer.print(F("' "));
     if(submit)htmlServer.print(F("onclick='submit();' >"));
     else htmlServer.print(F(" >"));
}

void printInputV( __FlashStringHelper dType[], __FlashStringHelper dClass[], __FlashStringHelper dName[], char* dValue, boolean submit){
     htmlServer.print(F("<input "));
     htmlServer.print(F("type='"));
     htmlServer.print(dType);
     htmlServer.print(F("' "));
     htmlServer.print(F(" class=' "));
     htmlServer.print(dClass);
     htmlServer.print(F("' "));
     htmlServer.print(F(" name='"));
     htmlServer.print(dName);
     htmlServer.print(F("' "));
     htmlServer.print(F(" value='"));
     htmlServer.print(dBuffer);
     htmlServer.print(F("' "));
     if(submit)htmlServer.print(F("onclick='submit();' >"));
     else htmlServer.print(F(" >"));
     if(debug>1){
       Serial.print("input ");
       Serial.println(dName);
       Serial.println(dBuffer);
     }
}
 

void loadFooter(){ 
      htmlServer.println("<footer class='sfooter'>\
            <p>Sigmation: Firewatcher, Sigma Telecom | <a href='mailto:info@sigma-telecom.com'>\
            info@sigma-telecom.com</a> <a href='http://www.sigma-telecom.com'>Website</a> \
          </footer>");
}


void htmlEnd(){
      htmlServer.println("</html>"); 
}

 
