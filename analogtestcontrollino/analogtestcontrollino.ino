#include <Controllino.h>  /* Usage of CONTROLLINO library allows you to use CONTROLLINO_xx aliases in your sketch. */

/*
  CONTROLLINO - Analog Out Analog In, Version 01.00

  Simple menu which alows you to test special CONTROLLINO MAXI Automation features 0-10V analog inputs and 0-10V / 0-20mA analog outputs.
  Please use Tools-Serial Monitor (9600 baud)

  IMPORTANT INFORMATION!
  Please, select proper target board in Tools->Board->CONTROLLINO MAXI Automation before Upload to your CONTROLLINO.
  (Please, refer to https://github.com/CONTROLLINO-PLC/CONTROLLINO_Library if you do not see the CONTROLLINOs in the Arduino IDE menu Tools->Board.)

  This example sketch requires external 24V power supply for your CONTROLLINO. 
 
  Created 2 Feb 2017
  by Lukas

  https://controllino.biz/

  (Check https://github.com/CONTROLLINO-PLC/CONTROLLINO_Library for the latest CONTROLLINO related software stuff.)
*/

int incomingByte = 0;   // for incoming serial data
int analogOutPercent0 = 0;   // initial value 0-100 percent
int analogOutPercent1 = 0;   // initial value 0-100 percent
int showMenu = 1;
int measureAI0 = 0;
int measureAI1 = 0;


void setup() {
  // initialize serial communications at 9600 bps
  Serial.begin(9600);

  pinMode(CONTROLLINO_A0, OUTPUT); 
  
}


void loop() {
  analogWrite(CONTROLLINO_A0, 0);
  delay(3000);
  analogWrite(CONTROLLINO_A0, 255);
  delay(3000);
  
}


/* End of the example. Visit us at https://controllino.biz/ or contact us at info@controllino.biz if you have any questions or troubles. */

/* 2017-02-02: The sketch was successfully tested with Arduino 1.8.1, Controllino Library 1.1.0 and CONTROLLINO MAXI Automation */
