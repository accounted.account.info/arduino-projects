#include <Wire.h>
#include <SFE_MicroOLED.h>  // Include the SFE_MicroOLED library
#include <WEMOS_DHT12.h>
#include <ESP8266WiFi.h>

#define PIN_RESET 255  //
#define DC_JUMPER 0  // I2C Addres: 0 - 0x3C, 1 - 0x3D
MicroOLED oled(PIN_RESET, DC_JUMPER);  // I2C Example

DHT12 dht12;

const int goaltemp= 22;
const int histeresis=1;

void setup() {

  Serial.begin(115200);
  pinMode(D3, OUTPUT);
  digitalWrite(D3, LOW);
  delay(2000);
  oled.begin();     // Initialize the OLED
  oled.clear(PAGE); // Clear the display's internal memory
  oled.clear(ALL);  // Clear the library's display buffer
  oled.display(); // display the memory buffer drawn

}

void loop() {
  
  // Clear the buffer.


  if(dht12.get()==0){
    oled.clear(PAGE);
    oled.setFontType(0); // set font type 0, please see declaration in SFE_MicroOLED.cpp
    
    oled.setCursor(3, 12);
    oled.print("H: ");
    oled.print(dht12.humidity);

    oled.setCursor(3, 0); 
    oled.print("T: ");
    oled.print(dht12.cTemp);
    if (dht12.cTemp>goaltemp+histeresis) digitalWrite(D3, HIGH); 
    else if (dht12.cTemp<=goaltemp) digitalWrite(D3, LOW);

    oled.setCursor(3, 24); 
    oled.print("I/O:");
    oled.print(goaltemp+histeresis);
    oled.print("|");
    oled.print(goaltemp);

    int state =  digitalRead(D3);
    oled.setCursor(3, 36); 
    oled.print("Relay: ");
    oled.print((state) ? "ON":"OFF");
  }
  else
  {
    oled.print("error! ");
  }
  oled.display();
  delay(1000);

}
