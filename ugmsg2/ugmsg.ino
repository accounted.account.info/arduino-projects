/*
  Use the Marquee function to make an LED-sign type display

  This marquee function scrolls in all directions, random distances for each
  direction. If you edit the code in the "switch" statement below then you can
  make a simpler marquee that only scrolls in one direction.
*/
#include <SPI.h>
#include <DMD2.h>
#include <fonts/Sigma9.h>

/* For "Hello World" as your message, leave the width at 4 even if you only have one display connected */
#define DISPLAYS_WIDE 4
#define DISPLAYS_HIGH 1

SoftDMD dmd(DISPLAYS_WIDE,DISPLAYS_HIGH);
DMD_TextBox box(dmd, 0, 0, 32, 16);

/*
chars especiales:
$ : escape
& : vehiculo
@ : exclamacion en triangulo
^ : peligro electrico
{ : fuego
} : derrumbe
~ : eñe
0x7F : FULL
*/

// the setup routine runs once when you press reset:
void setup() {
  dmd.setBrightness(255);
  dmd.selectFont(Sigma9);
  dmd.begin();
  int counter =32;
  /* TIP: If you want a longer string here than fits on your display, just define the display DISPLAYS_WIDE value to be wider than the
    number of displays you actually have.
   */
}

int phase = 0; // 0-3, 'phase' value determines direction

// the loop routine runs over and over again forever:
void loop() {
  
  dmd.drawString(0, 0, F("} TRONADURA !    "));
  delay(1500);
  dmd.drawString(0, 0, F("EN SECTOR 7          "));
  delay(1500);
  dmd.drawString(0, 0, F("@ PELIGRO          "));
  delay(1500);
  dmd.drawString(0, 0, F("} DERRUMBE          "));
  delay(1500);
  dmd.drawString(0, 0, F("& Vehiculo    "));
  delay(1500);
  dmd.drawString(0, 0, F("Aproximandose          "));
  delay(1500);
  dmd.drawString(0, 0, F("{ ASADO!!          "));
  delay(1500);
  dmd.drawString(0, 0, F("En Sector 8        "));
  delay(1500);
  dmd.drawString(0, 0, F("$ Evacuacion      "));
  delay(1500);
  dmd.drawString(0, 0, F("Sigma-Telecom      "));
  delay(1500);
  dmd.drawString(0, 0, F("UGMsg System      "));
  delay(1500);
  dmd.drawString(0, 0, F("Feliz Cumplea~os"));
  delay(1500);
  dmd.drawString(0, 0, F("\\"));
}
