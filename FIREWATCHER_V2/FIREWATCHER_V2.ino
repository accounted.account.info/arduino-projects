/*=================================================================================
////////////////////////  SOFTWARE UGMSG SIGMA TELECOM  ///////////////////////////
===================================================================================

INGENIERO: FELIPE EDUARDO BRAVO SILVA
NOMBRE P.: SISTEMA DE MENSAJES EN PANELES LED UGMSG
ESTADO   : EN DESARROLLO
FECHA ACT: 26-09-16
FECHA COM: 1-09-16
PROTOCOLO: CUSTOM FBS PROTOCOL (PROPIETARIO DE FELIPE BRAVO BAJO LICENCIA COPYLEFT)


=================================================================================*/
/*
OE -> 9 Pin 

A -> 6 Pin 

B -> 7 Pin 

CKL -> 13 Pin 

SKL -> 8 Pin 

R -> 11


  conector DMD en panel
               ______
 pin9 <-  nOE  | - - | A    -> pin6
  x   <-  GND  | - - | B    -> pin7
  x   <-  GND _| - - | C    ->  x
  x   <-  GND |  - - | CKL  -> pin13
  x   <-  GND |  - - | SCKL -> pin8
  x   <-  GND  | - - | R    -> pin11
  x   <-  GND  | - - | G    ->  x
  G   <-  GND  | - - | D    ->  x
               -------
               
  conector DMD en panel (version CONTROLLINO)
               ______
 pin9 <-  nOE  | - - | A    -> pin10
  x   <-  GND  | - - | B    -> pin12
  x   <-  GND _| - - | C    ->  x
  x   <-  GND |  - - | CKL  -> pin13
  x   <-  GND |  - - | SCKL -> pin8
  x   <-  GND  | - - | R    -> pin11
  x   <-  GND  | - - | G    ->  x
  G   <-  GND  | - - | D    ->  x
               -------           
               
*/


#include <SPI.h>
#include <DMD2.h>
#include <EEPROM.h>
#include <Sigma9_fw.h>


#define DISPLAYS_WIDE 1
#define DISPLAYS_HIGH 1 

//CONTROLLINO DMD2 PINS

#define PIN_NOE 9
#define PIN_A   10
#define PIN_B   12
#define PIN_SCK 8
#define PIN_CLK 13
#define PIN_R   11

/*===========================================================
/////////////////////// + ADDRESS   /////////////////////////
===========================================================*/

/*/-- address--/*/
int address= 2; //direccion de este equipo
int master_ad= 0xFF;
int zone=address;

/*===========================================================
///////////////////////  SYS VARS   /////////////////////////
===========================================================*/

SoftDMD dmd(DISPLAYS_WIDE,DISPLAYS_HIGH,PIN_NOE,PIN_A,PIN_B,PIN_SCK,PIN_CLK,PIN_R);
//SoftDMD dmd(DISPLAYS_WIDE,DISPLAYS_HIGH);
DMD_TextBox box(dmd, 0, 0, 32, 16);

/*/-- System --/*/
int alarmflags[101];
int doCommand=0;
int bufferToWrite=0;
int app_id=0x01;
int currentBuffer=4;
int currentLine=0;
const int max_per_line=4;
int firstcom=1;

/*/-- packet data --/*/
int command=0, data[200];
int code=0;
int r_zone;
int r_master_ad=0;
int r_ad=0;
int r_app_id=0;
int r_chck=0;

/*/-- packet bytes --/*/
  int o_address = 0x00;
  int packet[255];
  int packet_o[255];
  int packet_counter=0;
  int packet_ini=0;
  int packet_end=0;
  int packet_size=0;
  int dataBuffer[255];
  int sys_dataBuffer[255];
  int dataBufferLength=0;
  
/*/-- system time handle --/*/
  unsigned long start_t=0 ;  //SERIAL
  unsigned long work_last=0;
  unsigned long buzzer_start=0 ;  //SERIAL
  unsigned long buzzer_current=0;  
  unsigned long work_current=0;
  const unsigned long work_frame=600;
  const unsigned long answer_time=600;
  const unsigned long line_time=2000;
  unsigned long line_time_start=0;
  unsigned long line_time_current=0;
  
/*/-- LED system buffers and flags --/*/
  char ledbuffer[5][50];  // 4 buffers de 50 caracteresz
  char ledSubBuffer[5][4][20];  // 4 buffers con 4 subbuffers de 14 caracteresz
  int ledflags[5][4];     // 4 flags: 0-set 1-largo 2-lineas calculadas 3-not used 


/*/-- system PINOUT --/*/
int button_pin = 3;
int alarm_pin = 25;
 
/*/-- System flags --/*/ 
int button_pressed=0;
int alarm_flag=0;

/*========================================================================
    
    flags:
        0:set => 0 sin mensajes, 1 con mensaje, 2+ con mensaje emergencia 
               si setea mensaje emergencia, toma prioridad sobre otros 
               mensajes de emergencia(set+1 del mensaje de emergencia 
               con mayor prioridad), si existe almenos 1 mensaje de 
               emergencia, los mensajes normales son ignorados.
               Si solo existe un mensaje de emergencia, tomara el valor 2.
        1:largo => indica el largo del mensaje
        
========================================================================*/

/*========================================================================

chars especiales:
$ : escape
& : vehiculo
@ : exclamacion en triangulo
^ : peligro electrico
{ : fuego
} : derrumbe
~ : eñe
0x7F : FULL

========================================================================*/

/*===========================================================
/////////////////////////  SETUPS   /////////////////////////
===========================================================*/


void setup() {
  dmd.setBrightness(255);
  dmd.selectFont(Sigma9);
  dmd.begin();
  Serial.begin(2400);
  Serial1.begin(2400);
  pinMode(button_pin,INPUT_PULLUP);
  //attachInterrupt(1, irq_button, RISING);
  alarm(0);
//  sampleText();
  clearDataBuffer();
  startDelta();
  pinMode(alarm_pin,OUTPUT);
  digitalWrite(alarm_pin,LOW);
  delay(1);
  if(EEPROM.read(0)==0x0A) button_pressed=1;
  int lastzone = EEPROM.read(1);
  if(lastzone>0 && lastzone<100) zone = lastzone;
  delay(2000);
}


/*===========================================================
/////////////////////////  LOOP    //////////////////////////
===========================================================*/

void loop() {
  packet_check(); //
  if(packet_end==1){
    if(r_master_ad==address) doFunction(command);
    else if (r_master_ad==0  && code!=zone) doZoneFunction(command);
    else if (r_master_ad==0  && code==zone) doFunction(command);
    doCommand=0;
    clear_serial();
  }
  writeZones();// writeMessages();
  check_state();
  delay(1);
}

/*===========================================================
/////////////////////////  FUNCIONES  ///////////////////////
===========================================================*/
/////////////=============  DEMO  ===============////////////

int sampleText(){
    char sample[]="|   }    01 ";
    int ix=0;
    for(int i=0; i<4; i++)
    {
      ledbuffer[4][i] =sample[i];
      ledSubBuffer[4][0][i]=sample[i];
    } 
    for(int i=4; i<8; i++)
    {
      ledbuffer[4][i] =sample[i];
      ledSubBuffer[4][1][i-4]=sample[i];
    }     
    for(int i=8; i<12; i++)
    {
      ledbuffer[4][i] =sample[i];
      ledSubBuffer[4][2][i-8]=sample[i];
    }       
    ledflags[4][0]=1;
    ledflags[4][1]=12;
    ledflags[4][2]=3; 
}

/////////////=============   IRQ  ===============////////////

void irq_button(){
    button_pressed=1;
    
  }

/////////////============= SYSTEM ===============////////////
int doFunction(int doCommand_){ 
  int code = 0;
  switch(doCommand_){
  
    case  0  :    alarm(0);
                  code= 0x00;
                  //Serial.write((byte)doCommand_);
                  break;
                  
    case  1  :    alarm(1);
                  code= 0x01;
                  //Serial.write((byte)doCommand_);
                  break;

    case  2:      setZone(1);
                  code= alarm_flag;
                  //Serial.write((byte)doCommand_);
                  break;
                                
    default  :    error(0x03);
                  code= 0x03;
                  //Serial.write((byte)doCommand_);
                  break;
  } 
  if(r_master_ad==address) send_packet(code,code, 0 , 0);
  
}


void setZone(int fromcode){
    char messg0[]="                                  ";
    sprintf(messg0,"setzone %d %d ",fromcode);
    Serial.println(messg0);
    if (fromcode==1 && code!=0) zone=code;
    else if (r_zone!=0) zone==r_zone;
    if (zone!=0) EEPROM.write(1,zone); 
    updatezone();
  }


void updatezone(){
    
    switch(alarm_flag){ //alarm_flag
      case  0  :    sys_messages(1);
                    break;
                  
      case  1  :    sys_messages(0);
                    break;
                    
      default  :    error(0x03);
                    break;
      
      }
  }
  
int doZoneFunction(int doCommand_){
  //if (code!=0 && doCommand_>2) alarmtest(doCommand_);
  if (code!=0 && code<101) alarmZ(doCommand_,code); 
}

int alarmtest(int doCommand_){
    doCommand_=doCommand_-3;
    /*Serial.write(0xBB);
    Serial.write(doCommand_);
    Serial.write(code);
    Serial.write(alarmflags[0]);*/
    alarmZ(doCommand_,code);     
    send_packet(code,code, 0 , 0);
  }

int sys_messages(int messagenum){
       char messg0[]="{   }       ";
       char messg1[]="ZONA    `";
       //messg0[12]=127;
      sprintf(messg0,"{   }    %02d ",zone);
      sprintf(messg1,"ZONA %02d `",zone);
       
       switch(messagenum){
      
        case  0  :    set_message(messg0,12);
                      break;
        case  1  :    set_message(messg1,9);
                      break;
        default  :    error(0x03);
                      break;
      }
  }

int sys_messagesZone(int messagenum, int zonex){
       char messg0[]="{   }       ";
       char messg1[]="ZONA    `";
       messg0[12]=127;
       sprintf(messg0,"{   ZONA %02d ",zonex);
       sprintf(messg1,"ZONA %02d `",zonex);
      
       switch(messagenum){
      
        case  0  :    set_messageZone(messg0,12,zonex);
                      break;
        case  1  :    zoneTurnOff(zonex);
                      break;
        default  :    error(0x03);
                      break;
      }
  }
  
//getAvailableBuffer()

int set_message(char messg[], int datlen){
    //sys_savebuffer(int bufferlen,int bufferToWrite_);
    sys_setData( messg, datlen);
    sys_savebuffer(datlen,4);
  }

int set_messageZone(char messg[], int datlen, int zonex){
    //sys_savebuffer(int bufferlen,int bufferToWrite_);
    int freebuffer = getAvailableBuffer(zonex);
    ledflags[freebuffer][3]=zonex;
    sys_setData( messg, datlen);
    sys_savebuffer(datlen,freebuffer);
  }

int sys_setData(char messg[],int datlen){   
    dataBufferLength=datlen;  
    for (int i=0; i<dataBufferLength; i=i+1){
               sys_dataBuffer[i]=messg[i];
     } 
}

void alarmsignal(int set){
    switch(set){
      
        case  0  :    digitalWrite(alarm_pin, LOW);
                      break;
        case  1  :    digitalWrite(alarm_pin, HIGH); 
                      break;
        default  :    error(0x04);
                      break;
      }
  
  }
  
int alarm(int toggle_alarm){

    char messg0[]="                                  ";
    sprintf(messg0,"do zone function state %d zone %d ",toggle_alarm,zone);
    Serial.println(messg0);
    
    if(toggle_alarm==1){
                 alarm_flag=1;
                 alarmflags[zone]=1;
                 button_pressed=0;
                 alarmsignal(1);
                 sys_messages(0);
                 //EEPROM.write(0, 0x0A);
      }
    else if (toggle_alarm==0){
                 alarm_flag=0;
                 button_pressed=0;
                 alarmflags[zone]=0;
                 alarmsignal(0);
                 sys_messages(1);
                 //EEPROM.write(0, 0x00);      
      }
  
  }


int alarmZ(int toggle_alarm, int zonex){
    char messg0[]="                                  ";
    sprintf(messg0,"do zone function state %d zone %d ",toggle_alarm,zonex);
    Serial.println(messg0);
    if(toggle_alarm==1){
                 alarmflags[zonex]=1;
                 //sys_messagesZone(0, zonex);
      }
    else if (toggle_alarm==0){
                 alarmflags[zonex]=0;
                 //sys_messagesZone(1, zonex);    
      }
  
  }

int check_state(){
      if(button_pressed==1){// && alarm_flag==0) {
                 alarm(1);
                 char messg0[]="                                  ";
                  sprintf(messg0," (activado por softwr) function state %d zone %d ",1,zone);
                  Serial.println(messg0);
                 alarmflags[zone]=1;
                 send_packet(0xA7,0xA7, 0 , 0);
                 for (int i=0; i<3;i++){
                     if(!wait_answer()){
                          send_packet(0xA7,0xA7, 0 , 0);
                      }                  
                  }
                  button_pressed==0;
                 
        }
      else if (digitalRead(button_pin)==LOW) {
                alarm(1);
                char messg0[]="                                  ";
                  sprintf(messg0," (activado por boton) function state %d zone %d ",1,zone);
                  Serial.println(messg0);
                 alarmflags[zone]=1;
                send_packet(0xA7,0xA7, 0 , 0);
                 for (int i=0; i<3;i++){
                     while(!wait_answer()){
                      }  
                      send_packet(0xA7,0xA7, 0 , 0);                
                  }
                  button_pressed==0;
        }
        //else alarm(0);
  }


int sys_savebuffer(int bufferlen,int bufferToWrite_){
  
  if(bufferlen<50){
      for(int i=0;i<50 ;i++){
        ledbuffer[bufferToWrite_][i]= sys_dataBuffer[i];       
      }
      ledflags[bufferToWrite_][0]=5;
      //if(bufferToWrite_==4)  ledflags[bufferToWrite_][0]=1;
      ledflags[bufferToWrite_][1]=bufferlen;
      ledflags[bufferToWrite_][2]=calcLines(bufferlen);
      for(int i=0;i<ledflags[bufferToWrite_][2] ;i++){
        for(int j=0;j<max_per_line+5 ;j++){
          if(j<max_per_line)ledSubBuffer[bufferToWrite_][i][j]= ledbuffer[bufferToWrite_][(i*max_per_line)+j]; 
          else  ledSubBuffer[bufferToWrite_][i][j]= 0x20;      
        }      
      }
  }
 /* Serial.write("< write buffer: ");
  Serial.write(bufferToWrite_+0x30);
  Serial.write(" : ");
  Serial.write(ledflags[bufferToWrite_][0]+0x30);
  Serial.write(" >");*/
  clearDataBuffer();
}

int savebuffer(int bufferlen,int bufferToWrite_){
  
  if(bufferlen<50){
      for(int i=0;i<50 ;i++){
        ledbuffer[bufferToWrite_][i]= dataBuffer[2+i];       
      }
      ledflags[bufferToWrite_][0]=dataBuffer[0];
      if(bufferToWrite_==4)  ledflags[bufferToWrite_][0]=1;
      ledflags[bufferToWrite_][1]=bufferlen;
      ledflags[bufferToWrite_][2]=calcLines(bufferlen);
      for(int i=0;i<ledflags[bufferToWrite_][2] ;i++){
        for(int j=0;j<20 ;j++){
          if(j<max_per_line)ledSubBuffer[bufferToWrite_][i][j]= ledbuffer[bufferToWrite_][(i*max_per_line)+j]; 
          else  ledSubBuffer[bufferToWrite_][i][j]= 0x20;      
        }      
      }
  }
  //Serial1.write(0xFA);
  //Serial1.write(bufferToWrite_);
  //Serial1.write(ledflags[bufferToWrite_][0]);
  //Serial1.write(0xFB);
  clearDataBuffer();
}

int deleteBuffer(int buffernum){
  for(int i=0;i<50;i++){
    ledbuffer[buffernum][i]= 0;
  }
      ledflags[buffernum][0]=0;
      ledflags[buffernum][1]=0;
      ledflags[buffernum][2]=0;
}

int deleteAllBuffers(){

  for(int j=0;j<5;j++){
     for(int i=0;i<50;i++){
      ledbuffer[j][i]= 0;
    }
        ledflags[j][0]=0;
        ledflags[j][1]=0;
        ledflags[j][2]=0;
  }
  
}

int clearDataBuffer(){

  for(int j=0;j<255;j++){
      dataBuffer[j]= 0x20;    
  }
  
}

int error(int code){
  /*
    Error codes:
    0x00: buffer incorrecto
    0x01: largo incorrecto de mensaje
    0x02: codigos de error 0x00 y 0x01 juntos
    0x03: comando incorrecto/ error de sistema 
  */
  send_packet(0x0E, code, 0xFF, 0x00);
  Serial.print("::: err :::");
}
/////////////============= PANEL LED ============////////////

int topPriority(){
  int topprior=0;
  for(int i=0;i<5;i++){
      if (topprior<ledflags[i][0]) topprior=ledflags[i][0];
  }
  return topprior;
}

int zoneTurnOff(int zonex){
  for(int i=0;i<5;i++){
      if (ledflags[i][3]==zonex) ledflags[i][0]=0;
  }
  
 }

 int getAvailableBuffer(int zonex){
    for(int i=0;i<4;i++){
      if (ledflags[i][3]==zonex) return i;
    }
    for(int i=0;i<4;i++){
      if (ledflags[i][0]==0) return i;
    }
    return 0;
  }

int calcLines(int sizeOfBuffer){
  for(int i=1;i<=4 && sizeOfBuffer>0 ;i++){
    if (sizeOfBuffer<=(max_per_line*i)) return i;  
  }
  return 0;
}

long getDelta(){
    line_time_current=millis();
    return line_time_current-line_time_start;
  }

int startDelta(){
    line_time_current=millis();
    line_time_start=millis();
  }
  
int writeMessages(){
  //Serial.write(0xAA);
  int topprior = topPriority();
  long time_delta=getDelta();
  if(ledflags[currentBuffer][0]==topprior && time_delta>line_time){

      dmd.drawString(0, 0, ledSubBuffer[currentBuffer][currentLine]);
      startDelta();  
      currentLine++; 
      if (currentLine >= ledflags[currentBuffer][2] | currentLine >3) {
          currentLine=0;
          currentBuffer++;
          if(currentBuffer>4) currentBuffer=0;
        }  
      
   } else if (ledflags[currentBuffer][0]!=topprior) {

      currentBuffer++;
      currentLine=0;
      if(currentBuffer>4) currentBuffer=0;
   }
}

int checkAlarms(){
    int localflag=0;
    for(int i=1; i<101;i++){
        if(alarmflags[i]>0) localflag=1;
      }  
    alarmflags[0]=localflag;
    alarmsignal(localflag);
}

int writeThisZone(){
  //Serial.write(0xCC);
  long time_delta=getDelta();
  if(time_delta>line_time){
      dmd.drawString(0, 0, ledSubBuffer[4][currentLine]);
      startDelta();  
      currentLine++; 
      if (currentLine >= ledflags[4][2] | currentLine >3 ) {
          currentLine=0;
          currentBuffer++;
        }     
   } 
}
int writeZones(){  //alarmflags[100]
  long time_delta=getDelta();
  checkAlarms();    
  if (alarmflags[0]==0) { 
      //Serial.write(0xAB);
      currentBuffer=0;
      writeThisZone();
  }
  else if (currentBuffer==100) writeThisZone();
  //else if (currentBuffer==zone) currentBuffer++;
  
   
  else if(currentBuffer>0 && alarmflags[currentBuffer]>0 && time_delta>line_time && currentBuffer<100){
      //Serial.write(0xDD);
      char messg0[]="    ";
      sprintf(messg0," %02d `",currentBuffer);
      dmd.drawString(0, 0, messg0);
      startDelta();  
      currentBuffer++;
            
   } else if (time_delta>line_time){

      currentBuffer++;
      if(currentBuffer>100) {
         char messg1[]="    ";
         if (alarmflags[0]>0 && currentLine==0) sprintf(messg1,"{   ");
         if (alarmflags[0]>0 && currentLine==1) sprintf(messg1,"ZONA");
         currentLine++;
         if(currentLine>1) {currentBuffer=0; currentLine=0;}
         dmd.drawString(0, 0, messg1);
         startDelta();  
      }
   }
}

/////////////============= PANEL LED ============////////////

int systemMessage(char towrite[]){
  //dmd.drawString(0, 0, F(towrite));
  char *next = towrite;
  box.print(*next);
}

/////////////=============  COMM  ==============/////////////

int checkPacket(){
  int result=0;
  if(packet_end==1 && command>0 ){
      doCommand=command;
      getMessage();
      if(code<5 && dataBufferLength<52 ) {
          bufferToWrite= code;             // buffer=code
          send_packet(bufferToWrite, dataBufferLength, 0xFF, 0x00);  // recibido
          result=1;
      }
      else {
          if(code>4 && dataBufferLength>51)  error(0x02); //buffer incorrecto y tamaño muy grande
          else if(code>4) error(0x00); //buffer incorrecto
          else error(0x01); //tamaño incorrecto (mayor a 50 caracteres)
          doCommand=0;  //borrar comando
          result=0;
      }
      clear_serial();
  }
  else if(packet_end==1 && command==0){
      doCommand=command;
      if(code<5) {
          bufferToWrite= code;
          send_packet(0x01, 0x00, 0xFF, 0x00);
          result=1;
                            }
      else {
          error(0x00); //buffer incorrecto
          doCommand=0xFF;  //borrar comando
          result=0;
      }
      clear_serial();
  }
  return result;
}

int wait_answer(){
    unsigned long current_t=0, delta_t=0;
    start_t=millis();

    while(delta_t<answer_time){
      current_t = millis();
      delta_t = current_t-start_t;
      packet_check();
      writeMessages();
      if(packet_end==1)  {  return 1; }
    }
    return 0;
  }

int getMessage(){
    if(packet_size>=2)getData(packet_size);
}
  
int getData(int ldata){   
    dataBufferLength=ldata;  
    for (int i=0; i<ldata; i=i+1){
               dataBuffer[i]=packet[6+i];
     } 
}

int clear_serial(){
  
  for(int i=0; i<10; i=i+1){
      packet[i]=0;
    }
    packet_end=0;
    packet_ini=0; 
    packet_counter=0;
    packet_size=0;
}  
  
//////////=============  PROTOCOLO  ==============//////////
  

int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera      :   0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de unidad maestra
    3 address       :   direccion de este dispositivo
    4 zone          :   zona de ubicacion de este dispositivo
    5 largodatos    :   largo de datos ( 1 comando + 1 codigo + largo)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
_____________________________________________________________________________________________________________________________

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================*/
  
    if (Serial1.available()) {
      firstcom=1;
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      packet[packet_counter] = Serial1.read();
      //Serial.write(packet[packet_counter]);
      if(packet_counter==6 && packet_ini>0 && packet_size>0 )  {command=packet[packet_counter];}//systemMessage("    x");}
      else if(packet_counter==7 && packet_ini>0 && packet_size>1)  {code=packet[packet_counter];}//systemMessage("    x");}
      
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {r_app_id=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {r_master_ad=packet[packet_counter]; packet_counter= packet_counter+1;
      } //if (r_master_ad!=address && r_master_ad!=0) {Serial.write(0xBB);Serial.write(r_ad);packet_counter=0; packet_size=0; packet_ini=0;}}//systemMessage(" x");}
      else if(packet_counter==3 && packet_ini>0)  {r_ad=packet[packet_counter]; packet_counter= packet_counter+1;  }//systemMessage(" x");}
      else if(packet_counter==4 && packet_ini>0)  {r_zone=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==5 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}     
      else if(packet_counter<6+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==6+packet_size && packet_ini>0){r_chck=packet[packet_counter];packet_counter= packet_counter+1;}//systemMessage("       x!");}
      else if(packet_counter==7+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;}//systemMessage("       x!");}
    }
    
  }  
  

  int checksum(){
       int checksm = packet[1];
       for (int i=2; i<packet_size+8; i=i+1){
           checksm = packet[i] ^ checksm;
        }  
       if(checksm==packet[packet_size+8]) return 0;
       else {               
             systemMessage("Error Checksum");
             //Serial.print("::: chk: ");
             //Serial.print(checksm);
             //Serial.print(" pchk: ");
             //Serial.print(packet[packet_size+5]);
             //Serial.print(" ::: ");
             return 1;
       }
       
    }
       

  int send_packet(int commando, int codigo, int s_address, int ldata){
    
    
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 app id        :   id de aplicacion (2 en este caso)
    2 master address:   direccion de este dispositivo
    3 address       :   direccion de unidad maestra
    4 zone          :   zona de ubicacion de este dispositivo
    5 largo         :   largo de data ( 1 comando + 1 codigo + datos)
    6 Comando       :   0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    7 codigo        :   buffer a escribir/borrar 0->4
    7+largo Data    :   D0: prioridad (ver explicacion de prioridades y buffers), D1 en adelante: caracteres
    8+largo Chksum  :   Cabecera (Xor) Direccion (Xor) Commando (Xor) Codigo (Xor) Data ;
    9+largo end     :   0xFD;
    
=============================================================================================================================*/

         int chcksm=0;
         packet_o[0] = 0xFE; 
         packet_o[1] = app_id;
         chcksm = chcksm ^ packet_o[1];
         packet_o[2] = master_ad;
         chcksm = chcksm ^ packet_o[2];
         packet_o[3] = address;
         chcksm = chcksm ^ packet_o[3];
         packet_o[4] = zone;
         chcksm = chcksm ^ packet_o[4];
         packet_o[5] = ldata+2;
         chcksm = chcksm ^ packet_o[5];
         packet_o[6] = commando;
         chcksm = chcksm ^ packet_o[6];
         packet_o[7] = codigo;
         chcksm = chcksm ^ packet_o[7];
         for (int i=0; i<ldata; i=i+1){
               packet_o[8+i] = data[i];
               chcksm = chcksm ^ packet_o[8+i];
        } 
        packet_o[8+ldata] =  chcksm; 
        packet_o[9+ldata] =  0xFD;
        for (int i=0; i<4;i++){
          if(firstcom==1) Serial1.write(0);
        }
        for (int i=0; i<10+ldata;i=i+1){
          if(firstcom==1) Serial1.write(packet_o[i]);
       }
    }



/*===========================================================
/////////////////////////  RECICLAJE  ///////////////////////
===========================================================*/
 /*
 
   int send_packet(int commando, int codigo, int s_address, int ldata){
       int chcksm=0;
       packet_o[0] = 0xFE; 
       packet_o[1] = app_id; //0x02 = app de mensaj
       packet_o[1] = ldata+1; 
       chcksm = chcksm ^ packet_o[1];
       packet_o[2] = address; 
       chcksm = chcksm ^ packet_o[2];
       packet_o[3] = master_ad; 
       chcksm = chcksm ^ packet_o[3];
       packet_o[4] = commando; 
       chcksm = chcksm ^ packet_o[4];
       packet_o[5] = codigo; 
       chcksm = chcksm ^ packet_o[5];
       for (int i=0; i<ldata; i=i+1){
               packet_o[6+i] = data[i];
               chcksm = chcksm ^ packet_o[5+i];
        } 
       packet_o[6+ldata] =  chcksm; 
       packet_o[7+ldata] =  0xFD;
       for (int i=0; i<8+ldata;i=i+1){
          Serial.write(packet_o[i]);
       }
    }



int packet_check(){
  
/*===========================================================================================================================

  Estructura:
    0 Cabecera: 0xFE;
    1 Tamaño:   Bytes de data 0xDD;
    2 Direccion:0xXX; (0x10)
    3 Comando:  0x00 (guardar mensaje) , 0x01 (borrar mensaje);
    4 Data :    D1: codigo (buffer a escribir/borrar 0->4), D1: prioridad (ver explicacion de prioridades y buffers)
    5 Chksum:   Cabecera (Xor) Direccion (Xor) Commando (Xor) Data ;
    
=============================================================================================================================*/
/*============================================================================================================================

  Prioridad de mensajes:
  
    El o los mensajes con la prioridad mas alta seran mostrados, los demas son
    ignorados, hasta que el o los mensajes sean borrados o su prioridad
    sea alterada. Por logica no tiene sentido usar una prioridad mas alta
    que 5, que es la cantidad de buffers del sistema para el display, mas una prioridad de mensaje idle(1).
  
  Buffers
    buffer 0 esta destinado a mensajes broadcast, tendra siempre prioridad 4.
    buffer 1-3 estan destinados a mensajes locales. prioridad 1-5
    buffer 4 esta destinado a un mensaje idle, prioridad siempre sera 1
    
==============================================================================================================================/
  
    if (Serial.available()) {
      
      if(packet_counter>0 && packet_ini==0){packet_counter=0; packet_size=0;}
      packet[packet_counter] = Serial.read();
      //Serial.write(packet_counter);
      if(packet[packet_counter]==0xFE && packet_ini==0) {packet[0]==0xFE;packet_ini=1; packet_counter= packet_counter+1;}//systemMessage("x");}//
      else if(packet_counter==1 && packet_ini>0)  {packet_size=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage(" x");}
      else if(packet_counter==2 && packet_ini>0)  {packet_counter= packet_counter+1;}//systemMessage("  x");}
      else if(packet_counter==3 && packet_ini>0 && packet[packet_counter]!=address && packet[packet_counter]!=0) {packet_counter= 0; packet_ini=0;}
      else if(packet_counter==3 && packet_ini>0 && (packet[packet_counter]==address || packet[packet_counter]==0)) {packet_counter= packet_counter+1;}//systemMessage("   x");}
      else if(packet_counter==4 && packet_ini>0){ command=packet[packet_counter]; packet_counter= packet_counter+1;}//systemMessage("    x");}
      else if(packet_counter<5+packet_size  && packet_ini>0 ){packet_counter= packet_counter+1;}//systemMessage("     x");systemdebug(packet_counter);}
      else if(packet_counter==5+packet_size && packet_ini>0){packet_end=1; packet_counter= packet_counter+1;}//systemMessage("       x!");}
    }
    
  }

 */
